//
//  FirstViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import "CategoriesViewController.h"
#import "CustomCategoryCell.h"
#import "UIColorHelper.h"
#import "RequestBuilder.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "BACategory.h"
#import "BAConfiguration.h"
#import "CombosViewController.h"
#import "ProductsTableViewController.h"
#import "Product.h"
#import "Schedule.h"
#import "ShoppingCartViewController.h"
#import "ConfirmedOrderViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "BBBadgeBarButtonItem.h"
#import "MSDynamicsDrawerViewController.h"
#import <AudioToolbox/AudioServices.h>
#import <Google/Analytics.h>
#import "CustomLoaderView.h"
#import "AuthDashboardViewController.h"
#import "Constants.h"
#import "WSCoachMarksView.h"
#import "BAUser.h"
#import "BrandBanner+CoreDataClass.h"
#import "OrderRatingViewController.h"
#import "OrderListViewController.h"
#import "SearchItemsViewController.h"


@interface CategoriesViewController () <NSFetchedResultsControllerDelegate, UIScrollViewDelegate, UISearchBarDelegate, UISearchResultsUpdating>{
    CustomLoaderView *activityLoader;
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) CombosViewController *comboViewController;
@property (nonatomic) CGFloat lastContentOffset;
@property (strong, nonatomic) BBBadgeBarButtonItem *shoppingCartBarButtonItem;
@property (strong, nonatomic) NSMutableArray *sectionChanges;
@property (strong, nonatomic) NSMutableArray *itemChanges;
@property (strong, nonatomic) SearchItemsViewController *searchItemsController;
@property (strong, nonatomic) UISearchController * searchController;

@end

@implementation CategoriesViewController

@synthesize categoriesCollectionView = _categoriesCollectionView;
@synthesize combosScrollPageControl = _combosScrollPageControl;
@synthesize companyId = _companyId;
@synthesize deliveryAddressContainer = _deliveryAddressContainer;
@synthesize deliveryAddressLabel = _deliveryAddressLabel;
@synthesize shoppingCartButton = _shoppingCartButton;
@synthesize shoppingCartZeroButton = _shoppingCartZeroButton;
@synthesize activityIndicatorContainerView = _activityIndicatorContainerView;

- (void)initFetchController {
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deletedObj = 0"];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"BACategory"];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"orderKey" ascending:YES]]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsController.fetchRequest setPredicate:predicate];
    [self.fetchedResultsController setDelegate:self];
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

- (void)setupComboViewController {
    self.comboViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"comboViewController"];
    self.comboViewController.companyId = _companyId;
    [self addChildViewController:self.comboViewController];
    [self.comboControllerContainerView addSubview:self.comboViewController.view];
    self.comboViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 208);
    
    self.comboControllerContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.comboControllerContainerView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.comboControllerContainerView.layer.shadowRadius = 4.0;
    self.comboControllerContainerView.layer.shadowOpacity = 0.7;
}

-(void)setupDeliveryAddressContainer{
    UIColor *endColor = [UIColorHelper practicalColorWithRed:218.0 green:48.0 blue:57.0 alpha:0.9];
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:130.0 green:61.0 blue:115.0 alpha:0.9];
    CGRect frame = _deliveryAddressContainer.bounds;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
//    UIImage *background = [UIColorHelper imageFromLayer:ca];
    [_deliveryAddressContainer.layer insertSublayer:ca atIndex:0];
}

-(void) showMarkerLoader{
    _activityIndicatorContainerView.hidden = false;
//    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando"];
//    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

-(void)selectNewLocation:(id)sender{
    if([self presentingViewController]){
        [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"selectLocationViewControllerId"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ivc];
        [UIView transitionWithView:[UIApplication sharedApplication].keyWindow
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{ [UIApplication sharedApplication].keyWindow.rootViewController = navigationController; }
                    completion:nil];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"order_created"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"shopping_cart_modified"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:[[AppDelegate sharedAppDelegate].dynamicsDrawerViewController drawerViewControllerForDirection:MSDynamicsDrawerDirectionLeft]
                                                    name:@"user_state_changed"
                                                  object:nil];
}

#pragma mark NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    _sectionChanges = [[NSMutableArray alloc] init];
    _itemChanges = [[NSMutableArray alloc] init];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [_categoriesCollectionView performBatchUpdates:^{
        for (NSDictionary *change in _sectionChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [_categoriesCollectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [_categoriesCollectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                }
            }];
        }
        for (NSDictionary *change in _itemChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [_categoriesCollectionView insertItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [_categoriesCollectionView deleteItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeUpdate:
                        [_categoriesCollectionView reloadItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeMove:
                        [_categoriesCollectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                        break;
                }
            }];
        }
    } completion:^(BOOL finished) {
        _sectionChanges = nil;
        _itemChanges = nil;
        _collectionViewHeightConstraint.constant =_categoriesCollectionView.collectionViewLayout.collectionViewContentSize.height;
    }];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    change[@(type)] = @(sectionIndex);
    [_sectionChanges addObject:change];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    switch(type) {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            change[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [_itemChanges addObject:change];
}

#pragma mark UIViewController lifecycle

- (void)setupNavigation {
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    UIImage *image = [UIImage imageNamed:@"nav-logo"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
}

-(void)viewWillAppear:(BOOL)animated{
    [self setupNavigation];
    [self trackPageWithGA];
    if ([AppDelegate sharedAppDelegate].internetConnection) {
        [BrandBanner deleteAllBrandBanners:self.managedObjectContext];
        [self requestCategories];
    }else{
        [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
    }
    NSDictionary *retrievedDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"new_order_rating"];
    if (retrievedDictionary!=nil) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"new_order_rating"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self showRatingView:retrievedDictionary];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"order_rating_arrived"
                                                  object:nil];
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Página de Categorías"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _companyId = [AppDelegate sharedAppDelegate].closestCompany;
    _deliveryAddressLabel.text = [AppDelegate sharedAppDelegate].selectedLocationAddress;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showConfirmedOrderWithName:) name:@"order_created" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateShoppingCartBadge) name:@"shopping_cart_modified" object:nil];
    [self initFetchController];
    [self setupDeliveryAddressContainer];
    [self setupRightBarButtonItem];
    [self setupComboViewController];
    if (![AppDelegate sharedAppDelegate].reachableCompany) {
        [self showUnreachableAlert];
    }
}

-(void)showCoachMarks{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    CGFloat screenWidth = screenRect.size.width;
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{6.0f,17.0f},{50.0f,50.0f}}],
                                @"caption": @"Inicia sesión aquí",
                                @"shape": @"square"
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{screenWidth - 56.0f,17.0f},{50.0f,50.0f}}],
                                @"caption": @"Chequea tus productos en el carrito de compras",
                                @"shape": @"square"
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{12.0f,(screenHeight) - 45.0f},{screenWidth - 24.0f,40.0f}}],
                                @"caption": @"Puedes cambiar tu ubicación aquí",
                                @"shape": @"square"
                                }
                            ];
    WSCoachMarksView *coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    coachMarksView.animationDuration = 0.5f;
    coachMarksView.enableSkipButton = NO;
    [self.navigationController.view addSubview:coachMarksView];
    [coachMarksView start];
}


-(void)updateTotalNumber{
    long combos = [[AppDelegate sharedShoppingCart] amountOfCombosByQty];
    long products = [[AppDelegate sharedShoppingCart] amountOfProductsByQty];
    _shoppingCartBarButtonItem.badgeValue = [NSString stringWithFormat:@"%ld",combos + products];
}

-(void)updateShoppingCartBadge{
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    [self updateTotalNumber];
}

-(void)setupRightBarButtonItem{
    BBBadgeBarButtonItem *barButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:_shoppingCartButton zeroUIButton:_shoppingCartZeroButton];
    _shoppingCartBarButtonItem = barButton;
    barButton.badgeOriginX = 5.0;
    barButton.badgeOriginY = -2.0;
    barButton.badgeMinSize = 10.0;
    barButton.badgeBGColor = [UIColor clearColor];
    [self updateTotalNumber];
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(showSearchView:)];
    
    self.navigationItem.rightBarButtonItems = @[barButton, searchButton];
}

-(void)showSearchView:(id)sender{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.searchItemsController = (SearchItemsViewController*)[storyboard instantiateViewControllerWithIdentifier:@"searchItemsViewControllerId"];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchItemsController];
    self.searchController.hidesNavigationBarDuringPresentation = false;
    self.searchController.searchBar.keyboardType = UIKeyboardTypeAlphabet;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchBar.placeholder = @"Buscar productos";
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTitle:@"Cancelar"];
    [self presentViewController:self.searchController animated:true completion:nil];
    
    
}
#pragma mark UI Search Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(fetchResults) object:nil];
    [self performSelector:@selector(fetchResults) withObject:nil afterDelay:2.0];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(reload) object:nil];
    [self fetchResults];
}

- (void)fetchResults {
    NSString* text = self.searchController.searchBar.text;
    if (![text isEqual:@""]) {
        [self.searchItemsController handleSearchInput:text];
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    [self fetchResults];
}

#pragma mark HTTP Requests

-(void)requestSchedules{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_schedules_by_company:_companyId parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                NSDictionary *jsonObj = (NSDictionary *) responseObject;
                NSArray *schedulesArray = jsonObj[@"schedules"];
                NSString *scheduleText = jsonObj[@"schedule_text"][@"current_text"];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    [_activityIndicatorContainerView setHidden:true];
                    NSError *error = nil;
                    
                    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"BAConfiguration"];
                    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BAConfiguration" inManagedObjectContext:self.managedObjectContext];
                    NSArray *configurations = [_managedObjectContext executeFetchRequest:request error:&error];
                    if (error) {
                        [NSException raise:@"no schedule found" format:@"%@", [error localizedDescription]];
                    }
                    BAConfiguration *baconfigurationRecord = nil;
                    if (configurations.count > 0) {
                        // there is a truck with same id exsist. Use update method
                        baconfigurationRecord = [configurations lastObject];
                    }else {
                        baconfigurationRecord = [[BAConfiguration alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
                    }
                    baconfigurationRecord.scheduleText = scheduleText;
                    
                    if ([self.managedObjectContext save:&error]) {
                        
                    } else {
                        if (error) {
                            NSLog(@"Unable to save record.");
                            NSLog(@"%@, %@", error, error.localizedDescription);
                        }
                    }
                    
                    for (int i=0; i<schedulesArray.count; i++) {
                        NSString *scheduleId = [NSString stringWithFormat:@"%@",[[schedulesArray objectAtIndex:i] valueForKey:@"id"]] ;
                        error = nil;
                        request = [[NSFetchRequest alloc] initWithEntityName:@"Schedule"];
                        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"scheduleId==%@",scheduleId];
                        request.predicate=predicate;
                        entity = [NSEntityDescription entityForName:@"Schedule" inManagedObjectContext:self.managedObjectContext];
                        NSArray *schedules = [_managedObjectContext executeFetchRequest:request error:&error];
                        if (error) {
                            [NSException raise:@"no schedule found" format:@"%@", [error localizedDescription]];
                        }
                        Schedule *scheduleRecord = nil;
                        if (schedules.count > 0) {
                            // there is a truck with same id exsist. Use update method
                            scheduleRecord = [schedules lastObject];
                        }else {
                            scheduleRecord = [[Schedule alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
                        }
                        scheduleRecord.scheduleId = scheduleId;
                        scheduleRecord.dayOfWeek = [[schedulesArray objectAtIndex:i] valueForKey:@"day_of_week"];
                        scheduleRecord.initialHour = [[schedulesArray objectAtIndex:i] valueForKey:@"initial_hour"];
                        scheduleRecord.initialMnutes = [[schedulesArray objectAtIndex:i] valueForKey:@"initial_minutes"];
                        scheduleRecord.endHour = [[schedulesArray objectAtIndex:i] valueForKey:@"end_hour"];
                        scheduleRecord.endMinutes = [[schedulesArray objectAtIndex:i] valueForKey:@"end_minutes"];
                        
                        if ([self.managedObjectContext save:&error]) {
                            
                        } else {
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                [_activityIndicatorContainerView setHidden:true];
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

- (void)requestCategories{
    [self showMarkerLoader];
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_categories_with_parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                NSArray *jsonArray = (NSArray *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    //[BACategory deleteAllCategories:self.managedObjectContext];
                    for (int i=0; i<jsonArray.count; i++) {
                        NSNumber *catId = [[jsonArray objectAtIndex:i] valueForKey:@"id"];
                        NSError *error = nil;
                        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"BACategory"];
                        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"categoryId==%@",catId];
                        request.predicate=predicate;
                        NSEntityDescription *entity = [NSEntityDescription entityForName:@"BACategory" inManagedObjectContext:self.managedObjectContext];
                        NSArray *categories = [_managedObjectContext executeFetchRequest:request error:&error];
                        if (error) {
                            [NSException raise:@"no category find" format:@"%@", [error localizedDescription]];
                        }
                        BACategory *record = nil;
                        if (categories.count > 0) {
                            // there is a truck with same id exist. Use update method
                            record = [categories lastObject];
                        }else {
                            record = [[BACategory alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
                        }
                        record.name = [[jsonArray objectAtIndex:i] valueForKey:@"name"];
                        record.categoryId = catId;
                        record.orderKey = [[jsonArray objectAtIndex:i] valueForKey:@"order_key"];
                        id val = [[jsonArray objectAtIndex:i] valueForKey:@"description"];
                        val = (val == [NSNull null] ? nil : val);
                        record.categoryDescription = val;
                        record.categoryImgCode = [[jsonArray objectAtIndex:i] valueForKey:@"img_code"];
                        record.deletedObj = [[jsonArray objectAtIndex:i] valueForKey:@"deleted"];
                        record.isAlcoholic = [[jsonArray objectAtIndex:i] valueForKey:@"alcoholic"];
                        if ([self.managedObjectContext save:&error]) {
                            NSArray *brandsArray = (NSArray *) [[jsonArray objectAtIndex:i] valueForKey:@"visual_brands"];
                            for (int j = 0; j<brandsArray.count; j++) {
                                NSEntityDescription *entity = [NSEntityDescription entityForName:@"BrandBanner" inManagedObjectContext:self.managedObjectContext];
                                BrandBanner* brandBanner = nil;
                                brandBanner = [[BrandBanner alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
                                NSError *error = nil;
                                brandBanner.name = [[brandsArray objectAtIndex:j] valueForKey:@"name"];
                                brandBanner.categoryId = [[brandsArray objectAtIndex:j] valueForKey:@"category_id"];
                                brandBanner.bannerId = [[brandsArray objectAtIndex:j] valueForKey:@"id"];
                                brandBanner.imgUrl = [[brandsArray objectAtIndex:j] valueForKey:@"brand_banner_img_url"];
                                if ([self.managedObjectContext save:&error]) {
                                } else {
                                    if (error) {
                                        NSLog(@"Unable to save record.");
                                        NSLog(@"%@, %@", error, error.localizedDescription);
                                    }
                                }
                            }
                        } else {
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                    [self requestSchedules];
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
         
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
             @autoreleasepool {
                [_activityIndicatorContainerView setHidden:true];
                 NSLog(@"FAIL: %@", responseObject);
             }
         });
         
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return [[self.fetchedResultsController sections] count];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    double imageRatio = 0.77;
    double cellWidthProportion = 0.43;
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    return CGSizeMake(screenWidth*cellWidthProportion, screenWidth*cellWidthProportion*imageRatio);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CustomCategoryCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"customCategoryCell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark UICollectionViewDelegate

- (void)configureCell:(CustomCategoryCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    BACategory *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Update Cell
    [cell.categoryNameLabel setText:record.name.capitalizedString];
    [cell.categoryImageView setImage:([UIImage imageNamed: [NSString stringWithFormat:@"%@_collection_view_image",record.categoryImgCode]])];
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section:(nonnull NSIndexPath *)indexPath{
    return CGSizeMake(60.0f, 30.0f);// width is ignored
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGRect frame = _deliveryAddressContainer.frame;
    CGFloat size = frame.size.height;
    
    [_deliveryAddressContainer setFrame:frame];
    
    if (self.lastContentOffset > scrollView.contentOffset.y){
        //Scrolling up
    }else if (self.lastContentOffset < scrollView.contentOffset.y){
        //Scrolling down
    }
    self.lastContentOffset = scrollView.contentOffset.y;

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController* destination = [segue destinationViewController];
    if (destination.class == ProductsTableViewController.class) {
        if ([AppDelegate sharedAppDelegate].internetConnection) {
            ((ProductsTableViewController*)destination).categoryId =  ((BACategory *)([self.fetchedResultsController objectAtIndexPath:[_categoriesCollectionView indexPathForCell:sender]])).categoryId;
            ((ProductsTableViewController*)destination).companyId = _companyId;
            ((ProductsTableViewController*)destination).productImgCode =  ((BACategory *)([self.fetchedResultsController objectAtIndexPath:[_categoriesCollectionView indexPathForCell:sender]])).categoryImgCode;
            ((ProductsTableViewController*)destination).categoryName =  ((BACategory *)([self.fetchedResultsController objectAtIndexPath:[_categoriesCollectionView indexPathForCell:sender]])).name;

        }else{
            [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
        }
    }else if (destination.class == ShoppingCartViewController.class){
        ((ShoppingCartViewController*)destination).parentVC = self;
    }
}

-(void)showConfirmedOrderWithName:(NSNotification*)notification{
    NSDictionary* userInfo = notification.userInfo;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ConfirmedOrderViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"confirmedOrderViewControllerId"];
    vc.address = (NSString*)userInfo[@"order"][@"delivery_address"];
    vc.orderPoints = (NSNumber*)userInfo[@"order"][@"fidelity_points"];
    vc.contactName = (NSString*)userInfo[@"order"][@"contact_name"];
    vc.phoneNumber = (NSString*)userInfo[@"order"][@"contact_phone"];
    vc.total = (NSNumber *)(userInfo[@"order"][@"total"]);
    vc.addressReference = (NSString *)(userInfo[@"order"][@"address_reference"]);
    vc.paymentMethod = (NSString *)(userInfo[@"order"][@"payment_method"]);
    vc.cashAmount = (NSNumber *)(userInfo[@"order"][@"cash_amount"]);
    vc.parentVC = self;
    if ([vc.paymentMethod isEqualToString:@"online"]) {
        vc.cardSelected = (NSString*)(userInfo[@"order"][@"card_id_selected"]);
    }else{
        vc.cardSelected = @"";
    }
    
    NSError *error = nil;
    BAUser* user = [BAUser getLoggedInUser];
    user.fidelityPoints = [NSNumber numberWithFloat:([user.fidelityPoints floatValue] + [vc.orderPoints floatValue])];
    if ([self.managedObjectContext save:&error]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"user_state_changed" object:nil userInfo:nil];
    } else {
        if (error) {
            NSLog(@"Unable to save record.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
    }
    vc.deliveryFee = [AppDelegate sharedAppDelegate].deliveryFee;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)goBackToSelectLocation:(id)sender {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Información"
                                 message:@"Se borrarán los productos agregados a tu carro de compras"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self selectNewLocation:sender];
                                }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                actionWithTitle:@"Cancelar"
                                style:UIAlertActionStyleCancel
                                handler:^(UIAlertAction * action)
                                {
                                }];
    
    [alert addAction:yesButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void)showUnreachableAlert{
    UIAlertController * alert= [UIAlertController
                                alertControllerWithTitle:@"Oops"
                                message:@"¡Aún no la hacemos a tu zona! Pero te dejamos nuestro catálogo por si juergueas en otro lado 😎"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void)showOrdersList{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OrderListViewController *cvc = (OrderListViewController *)[storyboard instantiateViewControllerWithIdentifier:@"orderListViewControllerId"];
    cvc.showingPreviousOrders = false;
    UINavigationController *paneNavigationViewController = [[UINavigationController alloc] initWithRootViewController:cvc];
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneViewController:paneNavigationViewController animated:NO completion:nil];
    [AppDelegate sharedAppDelegate].menuViewController.paneViewControllerType = @"ORDERS";
    
}

-(void)showFirstTimeSignUpAlert{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Información"
                                 message:@"¡Regístrate ahora y obtén descuentos exclusivos!"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self goToAuthDashboard];
                                }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancelar"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                   }];
    
    [alert addAction:yesButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void)goToAuthDashboard{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AuthDashboardViewController *cvc = (AuthDashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"authDashboardControllerId"];
    cvc.originVc = auth_dashboard_origin_slider;
    
    UINavigationController *paneNavigationViewController = [[UINavigationController alloc] initWithRootViewController:cvc];
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneViewController:paneNavigationViewController animated:NO completion:nil];
}

- (IBAction)openSlider:(id)sender {
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:YES completion:^{
    }];
}

- (void)showRatingView:(NSDictionary*)userInfo {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"orderRatingViewControllerId"];
    ((OrderRatingViewController*)ivc).orderId = (NSNumber*)userInfo[@"order_id"];
    ivc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self.navigationController presentViewController:ivc animated:true completion:nil];
}

@end
