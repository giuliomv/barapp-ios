//
//  BAUser+CoreDataProperties.h
//  
//
//  Created by Giulio Mondoñedo on 1/12/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BAUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface BAUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *fullName;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *phoneNumber;
@property (nullable, nonatomic, retain) NSDate *birthday;
@property (nullable, nonatomic, retain) NSString *avatarUrl;
@property (nullable, nonatomic, retain) NSNumber *fidelityPoints;
@property (nullable, nonatomic, retain) NSString *userEmail;

@end

NS_ASSUME_NONNULL_END
