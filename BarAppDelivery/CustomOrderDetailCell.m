//
//  CustomOrderDetailCell.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 29/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "CustomOrderDetailCell.h"

@implementation CustomOrderDetailCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
