//
//  OrderRatingViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 31/05/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import "OrderRatingViewController.h"
#import "RequestBuilder.h"
#import "CustomLoaderView.h"

@interface OrderRatingViewController ()<UITextViewDelegate>{
    CustomLoaderView *activityLoader;
}
@property (nonatomic, strong) UITextView *activeField;
- (void)keyboardWillBeHidden:(NSNotification *)sender;
- (void)keyboardWasShown:(NSNotification *)sender;

@end

@implementation OrderRatingViewController

@synthesize commentsTextView = _commentsTextView;
@synthesize orderId = _orderId;
@synthesize ratingStarsView = _ratingStarsView;
@synthesize popUpView = _popUpView;
@synthesize thanksContainerView = _thanksContainerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [_commentsTextView setTextContainerInset:UIEdgeInsetsZero];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeEditing)];
    [_popUpView addGestureRecognizer:gestureRecognizer];
}

-(void)removeEditing{
    if (_commentsTextView.isFirstResponder) {
        [_commentsTextView endEditing:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ratingKeyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ratingKeyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)ratingKeyboardWasShown:(NSNotification *)aNotification {
        [self setViewMovedUp:YES];
    
}
#define kOFFSET_FOR_KEYBOARD 80.0

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = _popUpView.frame;
    if (movedUp){
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
    }
    else{
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
    }
    _popUpView.frame = rect;
    
    [UIView commitAnimations];
}

- (void)ratingKeyboardWillBeHidden:(NSNotification *)notification {
    [self setViewMovedUp:NO];
}

-(void) showMarkerLoader{
    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Enviando opinión"];
    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

-(void)textViewDidChange:(UITextView *)textView{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goBack {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitRating:(id)sender {
    [self showMarkerLoader];
    RequestBuilder *r = [[RequestBuilder alloc] init];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"nps": @{@"numeric_score":[NSNumber numberWithDouble: _ratingStarsView.value],@"order_id": _orderId, @"comment":_commentsTextView.text}}];
    [r submit_order_rating:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                NSObject *jsonObject = (NSObject *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if ([jsonObject valueForKey:@"success"]) {
                        [activityLoader stopAndRemoveLoader];
                        [self showThanksText];
                        [self performSelector:@selector(goBack) withObject:NULL afterDelay:2.0];
                    }
                });
            }
        });
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                [activityLoader stopAndRemoveLoader];
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

-(void)showThanksText{
    [_popUpView setHidden:YES];
    [_thanksContainerView setHidden:NO];
}

@end
