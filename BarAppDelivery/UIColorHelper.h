//
//  UIColorHelper.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 4/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColorHelper : UIColor

+(NSDictionary*)getConstDictionary;
+(UIColor*)practicalColorWithRed:(CGFloat) r
                           green:(CGFloat) g
                            blue:(CGFloat) b
                           alpha:(CGFloat) a;
+(UIColor*)getColorByCategoryCode:(NSString*) code;
+(CAGradientLayer*)gradientColorWithColors:(NSArray*)colors
                                startPoint:(CGPoint)startPoint
                                  endPoint:(CGPoint)endPoint
                                    frame:(CGRect)frame;

+(UIImage *)imageFromLayer:(CALayer *)layer;
+(UIImage *)imageWithColor:(UIColor *)color andBounds:(CGRect)imgBounds;
@end
