//
//  CustomAnimatedBackgroundView.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 17/11/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import "CustomAnimatedBackgroundView.h"

@interface CustomAnimatedBackgroundView()

@end

@implementation CustomAnimatedBackgroundView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.firstColor     = [UIColor redColor];
        self.secondColor       = [UIColor whiteColor];
        self.horizontalGradient   = NO;
        
        [self customInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    [self customInit];
}

- (void)setNeedsLayout {
    [super setNeedsLayout];
    [self setNeedsDisplay];
}


- (void)prepareForInterfaceBuilder {
    
    [self customInit];
}

- (void)customInit {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    
    gradient.colors = [NSArray arrayWithObjects:(id)[self.firstColor CGColor],(id)[self.secondColor CGColor], nil];
    gradient.endPoint = (self.horizontalGradient) ? CGPointMake(1, 0) : CGPointMake(0, 1);
    [self.layer insertSublayer:gradient atIndex:0];
    
}

//- (void)setFirstColor:(UIColor *)firstColor {
//    NSLog(@"COLORR %@", firstColor);
//    if (firstColor != _firstColor) {
//        _firstColor = firstColor;
//        [self customInit];
//    }
//}
//
//- (void)setSecondColor:(UIColor *)secondColor {
//    if (secondColor != _secondColor) {
//        _secondColor = secondColor;
//        [self customInit];
//    }
//}
//
//-(void)setHorizontalGradient:(BOOL)horizontalGradient{
//    _horizontalGradient = horizontalGradient;
//    [self customInit];
//}

@end
