//
//  Combo+CoreDataProperties.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Combo.h"

NS_ASSUME_NONNULL_BEGIN

@interface Combo (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *comboId;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSString *imgUrl;
@property (nullable, nonatomic, retain) NSNumber *qty;
@property (nullable, nonatomic, retain) NSNumber *addedToCart;
@property (nullable, nonatomic, assign) NSNumber *deletedObj;
@property (nullable, nonatomic, assign) NSNumber *flgStock;
@property (nullable, nonatomic, assign) NSNumber *orderKey;
@property (nullable, nonatomic, assign) NSString *shoppingCartImgUrl;
@property (nullable, nonatomic, retain) NSString *comboDescription;
@property (nullable, nonatomic, retain) NSString *bannerType;

@end

NS_ASSUME_NONNULL_END
