//
//  SignUpViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 29/11/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "SignUpViewController.h"
#import "UIColorHelper.h"
#import "AJWValidator.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/Analytics.h>
#import "CustomLoaderView.h"
#import "RequestBuilder.h"
#import "PDKeychainBindings.h"
#import "PDKeychainBindingsController.h"
#import "BAUser.h"
#import "AppDelegate.h"

#define kInputValidator @"kInputValidator"
#define kInputField @"kInputField"
#define kErrorView @"kErrorView"

@interface SignUpViewController ()<UITextFieldDelegate, UITextViewDelegate, AJWValidatorDelegate>{
    CustomLoaderView *activityLoader;
}

@property (nonatomic, strong) UITextField *activeField;
@property (strong, nonatomic) NSArray *validationData;

- (void)keyboardWillBeHidden:(NSNotification *)sender;
- (void)keyboardWasShown:(NSNotification *)sender;

@end

@implementation SignUpViewController

@synthesize userBasicInfo = _userBasicInfo;
@synthesize contentScrollView = _contentScrollView;
@synthesize fullNameTextField = _fullNameTextField;
@synthesize emailTextField = _emailTextField;
@synthesize telephoneTextField = _telephoneTextField;
@synthesize fullNameErrorView = _fullNameErrorView;
@synthesize emailErrorView = _emailErrorView;
@synthesize telephoneErrorView = _telephoneErrorView;
@synthesize confirmSignUpButton = _confirmSignUpButton;
@synthesize checkAdultSwitch = _checkAdultSwitch;
@synthesize birthdayPicker = _birthdayPicker;
@synthesize birthdayTextField = _birthdayTextField;
@synthesize birthdayErrorView = _birthdayErrorView;
@synthesize birthdayToolbar = _birthdayToolbar;
@synthesize parentVC = _parentVC;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardWillBeHidden:)];
    [_contentScrollView addGestureRecognizer:gestureRecognizer];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [self setupNavigationView];
    [self setupValidations];
    [self setupBirthdayPicker];
    [self setupFormData];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Página de registro"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)trackSignUp{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"accion_ui" action:@"click_registrar" label:nil value:nil] build]];
}

-(void)viewDidAppear:(BOOL)animated{
    for (int i=0; i < self.validationData.count; i++) {
        [self setupInputBorderToView:((UIView *)[self.validationData objectAtIndex:i][kErrorView])];
    }
    [self fullNameValidated];
    [self emailValidated];
    [self birthdayValidated];
}

-(void)setupBirthdayPicker{
    [_birthdayTextField setInputView:_birthdayPicker];
    [_birthdayTextField setInputAccessoryView:_birthdayToolbar];
}

-(void)setupNavigationView{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
    
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Iniciar Sesión".uppercaseString attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
}

-(void) fullNameValidated{
    [((AJWValidator*)[self.validationData objectAtIndex:0][kInputValidator]) validate:_fullNameTextField.text];
}

-(void) emailValidated{
    [((AJWValidator*)[self.validationData objectAtIndex:2][kInputValidator]) validate:_emailTextField.text];
}

-(void) birthdayValidated{
    [((AJWValidator*)[self.validationData objectAtIndex:3][kInputValidator]) validate:_birthdayTextField.text];
}

-(void)setupFormData{
    if ([_userBasicInfo valueForKey:@"email"]!=nil) {
        _emailTextField.text = [_userBasicInfo valueForKey:@"email"];
    }
    if ([_userBasicInfo valueForKey:@"picture"]!=nil) {
        [_avatarImageView setImageWithURL:[NSURL URLWithString:[[[_userBasicInfo valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"]]];
    }
    if ([_userBasicInfo valueForKey:@"birthday"]!=nil) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        dateFormat.lenient = true;
        NSDate *dateT = [dateFormat dateFromString:[_userBasicInfo valueForKey:@"birthday"]];
        [_birthdayPicker setDate:dateT];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dateT];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:((long)components.month-1)];
        _birthdayTextField.text = [NSString stringWithFormat:@"%@ %ld, %ld", monthName , (long)components.day, (long)components.year];
    }
    _fullNameTextField.text = [_userBasicInfo valueForKey:@"name"];
    _helloLabel.text = [NSString stringWithFormat:@"HOLA %@",[[_userBasicInfo valueForKey:@"first_name"] uppercaseString]];
}

-(void)setupInputBorderToView:(UIView *)view{
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColorHelper practicalColorWithRed:117.0 green:117.0 blue:117.0 alpha:1.0] CGColor];
    upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(view.frame), 1.0f);
    [view.layer addSublayer:upperBorder];
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + self.navigationController.navigationBar.bounds.size.height + 20, 0.0);
    _contentScrollView.contentInset = contentInsets;
    _contentScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _activeField.frame.origin) ) {
        [_contentScrollView scrollRectToVisible:_activeField.frame animated:YES];
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [_fullNameTextField endEditing:YES];
    [_emailTextField endEditing:YES];
    [_telephoneTextField endEditing:YES];
    [_birthdayTextField endEditing:YES];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    _contentScrollView.contentInset = contentInsets;
    _contentScrollView.scrollIndicatorInsets = contentInsets;
    
}

-(void)setupValidations{
    self.validationData = @[
                            @{
                                kInputValidator: [self nameValidator],
                                kInputField: _fullNameTextField,
                                kErrorView:_fullNameErrorView
                                },
                            @{
                                kInputValidator: [self phoneValidator],
                                kInputField: _telephoneTextField,
                                kErrorView:_telephoneErrorView
                                },
                            @{
                                kInputValidator: [self emailValidator],
                                kInputField:_emailTextField,
                                kErrorView:_emailErrorView
                                },
                            @{
                                kInputValidator: [self birthdayValidator],
                                kInputField:_birthdayTextField,
                                kErrorView:_birthdayErrorView
                                }
                            ];
    for (int i=0; i < self.validationData.count; i++) {
        [self setupValidatorStateChangeHandler:[self.validationData objectAtIndex:i][kInputValidator] withErrorView:[self.validationData objectAtIndex:i][kErrorView] forInputField:[self.validationData objectAtIndex:i][kInputField]];
        [[self.validationData objectAtIndex:i][kInputField] ajw_attachValidator:[self.validationData objectAtIndex:i][kInputValidator]];
        if ([[self.validationData objectAtIndex:i][kInputField] isKindOfClass:[UITextField class]]) {
            ((UITextField *)[self.validationData objectAtIndex:i][kInputField]).delegate = self;
        } else {
            ((UITextView *)[self.validationData objectAtIndex:i][kInputField]).delegate = self;
        }
    }
}

- (AJWValidator *)nameValidator
{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    validator.delegate = self;
    [validator addValidationToEnsurePresenceWithInvalidMessage:@"No olvides dejar tu nombre."];
    return validator;
}

- (AJWValidator *)phoneValidator
{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"^(\\d{9})$" invalidMessage:@"No olvides dejarnos tu celular (9 dígitos)"];
    return validator;
}

- (AJWValidator *)emailValidator
{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    [validator addValidationToEnsureValidEmailWithInvalidMessage:@"No olvides dejarnos tu correo"];
    return validator;
}

- (AJWValidator *)birthdayValidator
{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:@"¡Por favor ingresa tu cumpleaños!"];
    return validator;
}

-(void)setupValidatorStateChangeHandler:(AJWValidator*)validator withErrorView:(UIView*)view forInputField:(UITextField*)inputField{
    validator.validatorStateChangedHandler = ^(AJWValidatorState newState) {
        switch (newState) {
                
            case AJWValidatorValidationStateValid:
                [self markValidField:inputField inErrorView:view];
                break;
                
            case AJWValidatorValidationStateInvalid:
                [self markInvalidField:inputField inErrorView:view forValidation:validator];
                break;
                
            case AJWValidatorValidationStateWaitingForRemote:
                // do loading indicator things
                break;
                
        }
        [self allowSubmit:[self validateInputs]];
    };
}

-(void)allowSubmit:(BOOL) allowing{
    _confirmSignUpButton.enabled = allowing;
    if (allowing) {
        [_confirmSignUpButton setBackgroundColor:[UIColorHelper practicalColorWithRed:60.0 green:89.0 blue:253.0 alpha:1.0]];
    } else {
        [_confirmSignUpButton setBackgroundColor:[UIColorHelper practicalColorWithRed:189.0 green:189.0 blue:189.0 alpha:1.0]];
    }
}

-(BOOL)validateInputs{
    BOOL validated = TRUE;
    for (int i=0; i < self.validationData.count; i++) {
        validated &= ((AJWValidator *)[self.validationData objectAtIndex:i][kInputValidator]).state ==AJWValidatorValidationStateValid;
    }
    validated &= _checkAdultSwitch.isOn;
    return validated;
}

-(void)markValidField:(UITextField *)textField inErrorView:(UIView*)errorView {
    ((CALayer *)[errorView.layer.sublayers lastObject]).backgroundColor = [[UIColorHelper practicalColorWithRed:117.0 green:117.0 blue:117.0 alpha:1.0] CGColor];
    ((UILabel *)[errorView.subviews firstObject]).text = @"";
}


-(void)markInvalidField:(UITextField *)textField inErrorView:(UIView*)errorView forValidation:(AJWValidator*)validator {
    ((CALayer *)[errorView.layer.sublayers lastObject]).backgroundColor = [[UIColorHelper practicalColorWithRed:211.0 green:47.0 blue:47.0 alpha:1.0] CGColor];
    ((UILabel *)[errorView.subviews firstObject]).text = [validator.errorMessages firstObject];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* #pragma mark - UITextField Delegate */

-(void)textFieldTextChanged:(id)sender{
    
}

-(void)textViewDidChange:(UITextView *)textView{
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeField = nil;
}
    


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) showMarkerLoader{
    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando los tragos"];
    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

-(void)closeSignUp{
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)confirmSignUp:(id)sender {
    if ([AppDelegate sharedAppDelegate].internetConnection) {
        if ([self isAdult:_birthdayPicker.date]) {
            [self requestSignUp];
        } else {
            [self showValidationAlert:@"Debes ser mayor de 18 años para poder registrarte en Bar App."];
        }
    }else{
        [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
    }
}

- (void)showValidationAlert:(NSString *) message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Información"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void)requestSignUp{
    [self showMarkerLoader];
    RequestBuilder *r = [[RequestBuilder alloc] init];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"user": @{@"full_name":_fullNameTextField.text,@"email": _emailTextField.text, @"phone_number":_telephoneTextField.text, @"fb_birthday": _birthdayPicker.date}, @"access_token": [FBSDKAccessToken currentAccessToken].tokenString, @"token_expires_at": [FBSDKAccessToken currentAccessToken].expirationDate}];
    [r register_new_user:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                // Create Entity
                NSObject *jsonObject = (NSObject *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if ([jsonObject valueForKey:@"success"]) {
                        NSObject *jsonData = [jsonObject valueForKey:@"data"];
                        if ([jsonData valueForKey:@"new_user"]) {
                            NSObject *userData = [jsonData valueForKey:@"user"];
                            [activityLoader stopAndRemoveLoader];
                            PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
                            [bindings setObject:[userData valueForKey:@"authentication_token"] forKey:@"authToken"];
                            BAUser *record = nil;
                            NSError *error = nil;
                            NSEntityDescription *entity = [NSEntityDescription entityForName:@"BAUser" inManagedObjectContext:self.managedObjectContext];
                            record = [[BAUser alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
                            record.fullName = [userData valueForKey:@"full_name"];
                            record.phoneNumber = [userData valueForKey:@"phone_number"];
                            record.fidelityPoints = [userData valueForKey:@"total_fidelity_points"];
                            NSString *dateString = [userData valueForKey:@"fb_birthday"];
                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss z"];
                            NSDate *dateFromString = [[NSDate alloc] init];
                            dateFromString = [dateFormatter dateFromString:dateString];
                            record.birthday = dateFromString;
                            record.firstName = [_userBasicInfo valueForKey:@"first_name"];
                            record.avatarUrl = [[[_userBasicInfo valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                            record.userEmail = [_userBasicInfo valueForKey:@"email"];
                            if ([self.managedObjectContext save:&error]) {
                                [self trackSignUp];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"user_state_changed" object:nil userInfo:nil];
                                [self closeSignUp];
                                [((AuthDashboardViewController *)[self parentVC]) showNotificationDefaultAlert];
                            } else {
                                if (error) {
                                    NSLog(@"Unable to save record.");
                                    NSLog(@"%@, %@", error, error.localizedDescription);
                                }
                            }
                        }
                    }
                    [activityLoader stopAndRemoveLoader];
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                [activityLoader stopAndRemoveLoader];
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

- (IBAction)cancelSignUp:(id)sender {
    [self closeSignUp];
}

- (BOOL) isAdult:(NSDate *)birthday{
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    return age >= 18;
}

- (IBAction)dateChanged:(id)sender {
    UIDatePicker *picker = (UIDatePicker *)sender;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:picker.date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [[df monthSymbols] objectAtIndex:((long)components.month-1)];
    _birthdayTextField.text = [NSString stringWithFormat:@"%@ %ld, %ld", monthName , (long)components.day, (long)components.year];
    [self birthdayValidated];
}

- (IBAction)closeBirthdayPicker:(id)sender {
    [_birthdayTextField resignFirstResponder];
}

- (IBAction)adultSwitchChanged:(id)sender {
    [self allowSubmit:[self validateInputs]];
}
@end
