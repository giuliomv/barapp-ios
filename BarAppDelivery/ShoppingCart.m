//
//  ShoppingCart.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 12/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "ShoppingCart.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Product.h"
#import "Combo.h"
#import "ShoppingCartItem.h"

@implementation ShoppingCart
@synthesize total = _total;
@synthesize shoppingCartCombosItems = _shoppingCartCombosItems;
@synthesize shoppingCartProductsItems = _shoppingCartProductsItems;

-(id)init{
    self = [super init];
    if (self) {
        _total = 0.0;
        _shoppingCartProductsItems = [self getShoppingCartProducts];
        _shoppingCartCombosItems = [self getShoppingCartCombos];
        
    }
    return self;
}

-(long)amountOfProductsByQty{
    long qtyXProducts = 0;
    for (int i=0; i<_shoppingCartProductsItems.count; i++) {
        qtyXProducts += ((ShoppingCartItem *)[_shoppingCartProductsItems objectAtIndex:i]).qty.longValue;
    }
    return qtyXProducts;
}

-(long)amountOfCombosByQty{
    long qtyXCombos = 0;
    for (int i=0; i<_shoppingCartCombosItems.count; i++) {
        qtyXCombos += ((ShoppingCartItem *)[_shoppingCartCombosItems objectAtIndex:i]).qty.longValue;
    }
    return qtyXCombos;
}

-(NSMutableArray*)getShoppingCartCombos{
    NSManagedObjectContext* context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *comboEntity = [NSEntityDescription entityForName:@"Combo" inManagedObjectContext:context];
    [request setEntity:comboEntity];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"addedToCart == 1"];
    [request setPredicate:predicate];
    NSArray *combos = [context executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no combo find" format:@"%@", [error localizedDescription]];
    }
    NSMutableArray * items = [NSMutableArray new];
    if (combos.count > 0) {
        // there is a truck with same id exsist. Use update method
        for (int i = 0; i<combos.count; i++) {
            Combo *c = [combos objectAtIndex:i];
            ShoppingCartItem *sci = [[ShoppingCartItem alloc]initWithItemId:c.comboId name:c.name price:(c.price) qty:c.qty isCombo:TRUE itemImage:c.shoppingCartImgUrl itemDescription:c.comboDescription isAlcoholic:true];
            _total += ([c.price floatValue]*[c.qty integerValue]);
            [items addObject:sci];
        }
    }
    return items;
}

-(NSMutableArray*)getShoppingCartProducts{
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"addedToCart == 1"];
    request.predicate=predicate;
    NSManagedObjectContext* context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSArray *products = [context executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no product find" format:@"%@", [error localizedDescription]];
    }
    NSMutableArray * items = [NSMutableArray array];
    if (products.count > 0) {
        // there is a truck with same id exsist. Use update method
        for (int i = 0; i<products.count; i++) {
            Product *p = [products objectAtIndex:i];
            bool isAlcoholic = [p isAlcoholicProduct:context];
            ShoppingCartItem *sci = [[ShoppingCartItem alloc]initWithItemId:p.productId name:p.name price:(p.unitPrice) qty:p.qty isCombo:FALSE itemImage:p.imgUrl itemDescription:p.productDescription isAlcoholic:isAlcoholic];
            _total += ([p.unitPrice floatValue]*[p.qty integerValue]);
            [items addObject:sci];
        }
    }
    return items;
}

-(void) addProductToShoppingCart:(Product *) p
                        quantity:(NSNumber*) qty{
    NSLog(@"QTY %@", qty);
    ShoppingCartItem *sci = nil;
    NSManagedObjectContext* context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    if (p.addedToCart.intValue==1) {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.itemId == %d AND SELF.isCombo == NO",p.productId.intValue];
        sci = [[_shoppingCartProductsItems filteredArrayUsingPredicate:bPredicate] firstObject];
        NSUInteger index = [_shoppingCartProductsItems indexOfObject:sci];
        _total -= ([sci.price floatValue]*[sci.qty integerValue]);
        sci.qty = qty;
        _total += ([sci.price floatValue]*[sci.qty integerValue]);
        [_shoppingCartProductsItems replaceObjectAtIndex:index withObject:sci];
    }else{
        bool isAlcoholic = [p isAlcoholicProduct:context];
        sci = [[ShoppingCartItem alloc]initWithItemId:p.productId name:p.name price:(p.unitPrice) qty:qty isCombo:FALSE itemImage:p.imgUrl itemDescription:p.productDescription isAlcoholic:isAlcoholic];
        _total += ([sci.price floatValue]*[sci.qty integerValue]);
        [_shoppingCartProductsItems addObject:sci];
    }
    p.addedToCart = [NSNumber numberWithInt:1];
    p.qty = qty;
    NSError *error;
    if(! [context save:&error])
    {
        NSLog(@"Error,couldn't save:%@", [error localizedDescription]);
    }else{
        NSLog(@"SAVED %@", p.qty);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"shopping_cart_modified" object:nil userInfo:nil];
    
}

-(void) addComboToShoppingCart:(Combo *) c
                      quantity:(NSNumber*) qty{
    ShoppingCartItem *sci = nil;
    if (c.addedToCart.intValue==1) {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.itemId == %d AND SELF.isCombo == YES",c.comboId.intValue];
        sci = [[_shoppingCartCombosItems filteredArrayUsingPredicate:bPredicate] firstObject];
        NSUInteger index = [_shoppingCartCombosItems indexOfObject:sci];
        _total -= ([sci.price floatValue]*[sci.qty integerValue]);
        sci.qty = qty;
        _total += ([sci.price floatValue]*[sci.qty integerValue]);
        [_shoppingCartCombosItems replaceObjectAtIndex:index withObject:sci];
    }else{
        sci = [[ShoppingCartItem alloc]initWithItemId:c.comboId name:c.name price:(c.price) qty:qty isCombo:TRUE itemImage:c.shoppingCartImgUrl itemDescription:c.comboDescription isAlcoholic:true];
        _total += ([sci.price floatValue]*[sci.qty integerValue]);
        [_shoppingCartCombosItems addObject:sci];
    }
    NSManagedObjectContext* context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    c.addedToCart = [NSNumber numberWithInt:1];
    c.qty = qty;
    NSError *error;
    if(! [context save:&error])
    {
        NSLog(@"Error,couldn't save:%@", [error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"shopping_cart_modified" object:nil userInfo:nil];
    
}

-(void) removeComboFromShoppingCart:(Combo *) c{
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.itemId == %d AND SELF.isCombo==1",c.comboId.intValue];
    ShoppingCartItem *sci = [[_shoppingCartCombosItems filteredArrayUsingPredicate:bPredicate] firstObject];
    _total -= ([sci.price floatValue]*[sci.qty integerValue]);
    [_shoppingCartCombosItems removeObject:sci];
    NSManagedObjectContext* context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    c.addedToCart = [NSNumber numberWithInt:0];
    c.qty = [NSNumber numberWithInt:0];
    NSError *error;
    if(! [context save:&error])
    {
        NSLog(@"Error,couldn't save:%@", [error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"shopping_cart_modified" object:nil userInfo:nil];
}

-(void) removeProductFromShoppingCart:(Product *) p{
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.itemId == %d AND SELF.isCombo==0",p.productId.intValue];
    ShoppingCartItem *sci = [[_shoppingCartProductsItems filteredArrayUsingPredicate:bPredicate] firstObject];
    _total -= ([sci.price floatValue]*[sci.qty integerValue]);
    [_shoppingCartProductsItems removeObject:sci];
    NSManagedObjectContext* context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    p.addedToCart = [NSNumber numberWithInt:0];
    p.qty = [NSNumber numberWithInt:0];
    NSError *error;
    if(! [context save:&error])
    {
        NSLog(@"Error,couldn't save:%@", [error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"shopping_cart_modified" object:nil userInfo:nil];
}

@end
