//
//  OrderLocationAnnotation.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "OrderLocationAnnotation.h"

@implementation OrderLocationAnnotation

@synthesize coordinate, title, subtitle;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
