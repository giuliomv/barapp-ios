//
//  BrandBanner+CoreDataProperties.h
//  
//
//  Created by Giulio Mondoñedo on 28/06/17.
//
//

#import "BrandBanner+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BrandBanner (CoreDataProperties)

+ (NSFetchRequest<BrandBanner *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *bannerId;
@property (nullable, nonatomic, copy) NSNumber *categoryId;
@property (nullable, nonatomic, copy) NSString *imgUrl;

@end

NS_ASSUME_NONNULL_END
