//
//  Combo.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "Combo.h"
#import "AppDelegate.h"

@implementation Combo

// Insert code here to add functionality to your managed object subclass

+(Combo*)getComboById:(NSNumber *)comboId
           bannerType:(NSString *)bannerType
              context:(NSManagedObjectContext *)context{
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Combo"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"comboId == %@ AND bannerType == %@", comboId, bannerType];
    request.predicate=predicate;
    NSArray *combos = [context executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no combo found" format:@"%@", [error localizedDescription]];
    }

    return combos.lastObject;
}

+(void)deleteAllCombos:(NSManagedObjectContext *)context{
    NSFetchRequest *allCombos = [[NSFetchRequest alloc] init];
    [allCombos setEntity:[NSEntityDescription entityForName:@"Combo" inManagedObjectContext:context]];
    [allCombos setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *combos = [context executeFetchRequest:allCombos error:&error];
    //error handling goes here
    for (NSManagedObject *combo in combos) {
        [context deleteObject:combo];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}


@end
