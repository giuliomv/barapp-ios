//
//  MyLocationAnnotation.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 29/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "MyLocationAnnotation.h"

@implementation MyLocationAnnotation

@synthesize coordinate = _coordinate;

- (id)initWithLocation:(CLLocationCoordinate2D)coord {
    self = [super init];
    if (self) {
        coordinate = coord;
    }
    return self;
}

@end
