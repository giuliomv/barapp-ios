//
//  LocationPermissionRequestViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 7/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "LocationPermissionRequestViewController.h"
#import "InitialLoaderViewController.h"
#import <Google/Analytics.h>

@interface LocationPermissionRequestViewController ()

@end

@implementation LocationPermissionRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)trackPageWithGA:(NSString*) origin{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:origin];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)goBack {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)acceptAction:(id)sender {
    [self trackPageWithGA:@"Aceptar ver permiso de localización"];
    [self goBack];
    [((InitialLoaderViewController *)[self parentVC]) locationPermissionRequestWasAccepted];
}

- (IBAction)cancelAction:(id)sender {
    [self trackPageWithGA:@"Rechazar ver permiso de localización"];
    [self goBack];
    [((InitialLoaderViewController *)[self parentVC]) locationPermissionRequestWasDenied];
}
@end
