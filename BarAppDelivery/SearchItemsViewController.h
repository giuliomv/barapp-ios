//
//  SearchItemsViewController.h
//  BarAppDelivery
//
//  Created by Home on 6/3/20.
//  Copyright © 2020 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchItemsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

- (void)handleSearchInput:(NSString*) text;

@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;
@property (strong, nonatomic) NSNumber *companyId;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSMutableArray* resultsItems;
- (IBAction)searchMinusSelected:(id)sender;
- (IBAction)searchPlusSelected:(id)sender;

@end

NS_ASSUME_NONNULL_END
