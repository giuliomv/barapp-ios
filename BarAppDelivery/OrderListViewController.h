//
//  OrderListViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 1/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderListViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *previousOrdersButton;
@property (weak, nonatomic) IBOutlet UIButton *activeOrdersButton;
- (IBAction)showSlider:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *orderListCollectionView;
- (IBAction)selectPreviousOrders:(id)sender;
- (IBAction)selectActiveOrders:(id)sender;
@property BOOL showingPreviousOrders;
@property (weak, nonatomic) IBOutlet UIView *emptyOrdersView;
@property (weak, nonatomic) IBOutlet UILabel *emptyOrdersMessageLabel;

@end
