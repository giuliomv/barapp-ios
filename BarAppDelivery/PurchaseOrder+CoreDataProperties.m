//
//  PurchaseOrder+CoreDataProperties.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PurchaseOrder+CoreDataProperties.h"

@implementation PurchaseOrder (CoreDataProperties)

@dynamic purchaseOrderId;
@dynamic contactName;
@dynamic deliveryAddress;
@dynamic addressReference;
@dynamic contactPhone;
@dynamic total;
@dynamic paymentMethod;
@dynamic purchaseDate;
@dynamic cashAmount;

@end
