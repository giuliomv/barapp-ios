//
//  OrderDetailsViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 26/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "UIPlaceHolderTextView.h"
#import "SelectableView.h"

@interface OrderDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (nonatomic, assign) CLLocationCoordinate2D selectedLocationCoordinates;
- (IBAction)confirmOrderButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *confirmOrderButton;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
- (IBAction)textFieldTextChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *addressNumberTextField;
@property (weak, nonatomic) IBOutlet UIView *nameErrorView;
@property (weak, nonatomic) IBOutlet UIView *phoneErrorView;
@property (weak, nonatomic) IBOutlet UIView *addressErrorView;
@property (weak, nonatomic) IBOutlet UIView *referencesErrorView;
@property (weak, nonatomic) IBOutlet UITextField *addressDptoTextField;
@property (weak, nonatomic) IBOutlet UITextField *cellphoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet UIToolbar *cellphoneToolBar;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *referencesTextView;
- (IBAction)resignPhoneNumberResponder:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *addressNumberBottomView;
@property (weak, nonatomic) IBOutlet UIView *addressDptoBottomView;
@property (strong, nonatomic) UIViewController *parentVC;
@property (weak, nonatomic) IBOutlet UITextField *cashAmountTextField;
@property (weak, nonatomic) IBOutlet UIView *cashAmountBottomView;
- (IBAction)goBack:(id)sender;
- (void)cardsListDidClosed;
@property (weak, nonatomic) IBOutlet UIView *paymentMethodContentView;
@property (weak, nonatomic) IBOutlet UIButton *paymentMethodButton;
- (IBAction)showCardsList:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *paymentMethodImageView;
@property (weak, nonatomic) IBOutlet UILabel *paymentMethodNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmButtonTopConstraint;


@end
