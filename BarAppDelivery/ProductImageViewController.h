//
//  ProductImageViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 19/05/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface ProductImageViewController : UIViewController

@property (strong, nonatomic) NSString *productImgUrl;
@property (strong, nonatomic) NSString *productImgName;
@property (strong, nonatomic) Product *product;
@property (weak, nonatomic) IBOutlet UILabel *productUnitCostLabel;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productQtyLabel;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleNavigationItem;
- (IBAction)goBack:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *backgroundOverlayView;
- (IBAction)minusSelected:(UIButton *)sender;
- (IBAction)plusSelected:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end
