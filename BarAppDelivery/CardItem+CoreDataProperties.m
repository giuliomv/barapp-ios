//
//  CardItem+CoreDataProperties.m
//  
//
//  Created by Giulio Mondoñedo on 28/05/18.
//
//

#import "CardItem+CoreDataProperties.h"

@implementation CardItem (CoreDataProperties)

+ (NSFetchRequest<CardItem *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CardItem"];
}

@dynamic itemId;
@dynamic lastFour;
@dynamic cardBrand;

-(id)initWithItemId:(NSNumber *) itemId
           lastFour:(NSString *) lastFour
          cardBrand:(NSString *) cardBrand{
    self = [super init];
    if (self) {
        self.itemId = itemId;
        self.lastFour = lastFour;
        self.cardBrand = cardBrand;
    }
    return self;
}

@end
