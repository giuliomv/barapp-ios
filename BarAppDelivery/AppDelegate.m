//
//  AppDelegate.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "ShoppingCart.h"
#import "RequestBuilder.h"
#import <MapKit/MapKit.h>
#import "MSDynamicsDrawerViewController.h"
#import "SlideDrawerViewController.h"
#import "CategoriesViewController.h"
#import "BAUser.h"
#import <Google/Analytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Reachability.h"
#import "UIColorHelper.h"
#import "ACTReporter.h"


@interface AppDelegate ()

@property (strong, nonatomic) ShoppingCart *shoppingCart;
@property (nonatomic) Reachability *internetReachability;

@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize selectedLocationAddress = _selectedLocationAddress;
@synthesize selectedLocationCoordinates = _selectedLocationCoordinates;
@synthesize closestCompany = _closestCompany;
@synthesize couponApplied = _couponApplied;
@synthesize deliveryFee = _deliveryFee;
@synthesize minimumAmount = _minimumAmount;
@synthesize reachableCompany = _reachableCompany;
@synthesize deliveryMessageOption = _deliveryMessageOption;
@synthesize internetConnection = _internetConnection;

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

-(void)reachabilityUnreachable:(UIViewController *)presenter{
    UIAlertController *reachabilityAlert = [UIAlertController
                          alertControllerWithTitle:@"Información"
                          message:@"Por favor, conecta tu dispositivo a Internet."
                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ir a Settings"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    NSURL*url=[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                    [[UIApplication sharedApplication] openURL:url];
                                }];
    
    [reachabilityAlert addAction:yesButton];
    [presenter presentViewController:reachabilityAlert animated:YES completion:nil];
    [reachabilityAlert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}


#pragma mark UIApplicationDelegate Managing State Transitions

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)setupGoogleAnalytics {
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
}

- (void)regularAppInit {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ivc = NULL;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _closestCompany = (NSNumber *)[defaults objectForKey:@"closestCompany"];
    ivc = [storyboard instantiateViewControllerWithIdentifier:@"initialLoaderViewControllerId"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ivc];
    self.window.tintColor = [UIColor redColor];
    //[[UIBarButtonItem appearance] setTintColor:[UIColor redColor]];
    self.window.rootViewController = navigationController;
    
    [self.window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self initialSetup];
    [self setupReachability];
    [ACTConversionReporter reportWithConversionID:@"926504666" label:@"sBICCNK073AQ2q3luQM" value:@"0.30" isRepeatable:NO];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes: @{NSFontAttributeName:[UIFont fontWithName:@"Gobold" size:14.0]} forState:UIControlStateNormal];

    _shoppingCart = [[ShoppingCart alloc] init];
    
    [self setupGoogleAnalytics];
    [self regularAppInit];
    if (launchOptions) {
        NSDictionary *userInfo = [launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
        if ([userInfo valueForKey:@"nps_push"]) {
            [self postNotificationToPresentPushMessagesVC:userInfo];
        }else if([userInfo valueForKey:@"order_update_push"]){
            [self postNotificationToPresentOrderListVC:userInfo];
        }
    }
    
    return YES;
}

- (void) setupReachability{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    _internetConnection = YES;
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    [self testReachability:netStatus];
}

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    [self testReachability:netStatus];
}

-(void)testReachability:(NetworkStatus)ns{
    if (ns == NotReachable) {
        _internetConnection = NO;
    }else{
        _internetConnection = YES;
    }
}

-(void)initialSetup{
    //Clear keychain on first run in case of reinstallation
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"first_run"]) {
        [BAUser logOutCurrentUser];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"first_run"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void) initCompanyDetails{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _selectedLocationAddress = (NSString *)[defaults objectForKey:@"selectedLocationAddress"];
    double latitude = (double)[defaults doubleForKey:@"selectedLocationCoordinatesLatitude"];
    double longitude = (double)[defaults doubleForKey:@"selectedLocationCoordinatesLongitude"];
    _selectedLocationCoordinates = CLLocationCoordinate2DMake(latitude, longitude);
    _deliveryFee = (NSNumber *)[defaults objectForKey:@"deliveryFee"];
    _minimumAmount = (NSNumber *)[defaults objectForKey:@"minimumAmount"];
}

-(MSDynamicsDrawerViewController *)getDynamicsViewControllerWithPaneView:(UINavigationController *)paneViewController storyboard:(UIStoryboard*) storyboard{
    SlideDrawerViewController *menuViewController = (SlideDrawerViewController *)[storyboard instantiateViewControllerWithIdentifier:@"slideDrawerViewControllerId"];
    menuViewController.paneViewControllerType = @"CATEGORIES";
    MSDynamicsDrawerViewController *dynamicsViewController = (MSDynamicsDrawerViewController *)[storyboard instantiateViewControllerWithIdentifier:@"msDynamicsDrawerViewControllerId"];
    _dynamicsDrawerViewController = dynamicsViewController;
    menuViewController.dynamicsDrawerViewController = dynamicsViewController;
    dynamicsViewController.paneViewController = paneViewController;
    [dynamicsViewController setDrawerViewController:menuViewController forDirection:MSDynamicsDrawerDirectionLeft];
    [dynamicsViewController addStylersFromArray:@[[MSDynamicsDrawerResizeStyler styler],[MSDynamicsDrawerParallaxStyler styler]] forDirection:MSDynamicsDrawerDirectionLeft];
    return dynamicsViewController;
}

#pragma mark UIApplicationDelegate Responding to Notifications and Events

-(void)postNotificationToPresentPushMessagesVC:(NSDictionary*)userInfo {
    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:@"new_order_rating"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)postNotificationToPresentOrderListVC:(NSDictionary*)userInfo {
    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:@"new_order_update"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    if(application.applicationState == UIApplicationStateInactive) {
        
        NSLog(@"Inactive");
        //Show the view with the content of the push
        if ([userInfo valueForKey:@"nps_push"]) {
            [self postNotificationToPresentPushMessagesVC:userInfo];
            completionHandler(UIBackgroundFetchResultNewData);
        }else if([userInfo valueForKey:@"order_update_push"]){
            [self postNotificationToPresentOrderListVC:userInfo];
            completionHandler(UIBackgroundFetchResultNewData);
        }
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
        NSLog(@"Background");
        
        //Refresh the local model
        if ([userInfo valueForKey:@"nps_push"]) {
            [self postNotificationToPresentPushMessagesVC:userInfo];
            completionHandler( UIBackgroundFetchResultNewData );
        }else if([userInfo valueForKey:@"order_update_push"]){
            [self postNotificationToPresentOrderListVC:userInfo];
            completionHandler(UIBackgroundFetchResultNewData);
        }
        
    } else {
        
        NSLog(@"Active");
        if ([userInfo valueForKey:@"nps_push"]) {
            [self postNotificationToPresentPushMessagesVC:userInfo];
            completionHandler( UIBackgroundFetchResultNewData );
        }else if([userInfo valueForKey:@"order_update_push"]){
            [self postNotificationToPresentOrderListVC:userInfo];
            completionHandler(UIBackgroundFetchResultNewData);
        }
        
        //Show an in-app banner
        
    }
    [self regularAppInit];
}

-(void)registerForPushNotifications:(UIApplication *)application {
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    } else{
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
}

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSetting{
    if (notificationSetting!=nil) {
        [application registerForRemoteNotifications];
    }
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    const char *data = [deviceToken bytes];
    NSMutableString *token = [NSMutableString string];
    
    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhX", data[i]];
    }
    NSMutableString *notificationToken = [token copy];
    
    [self sendTokenToServer:token];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"Failed to register token: %@", error.description);
}

-(void)sendTokenToServer:(NSString *)token{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    NSDictionary *params = @{@"device_token": @{@"token_string": token, @"device_platform" : @"ios"}};

    [r send_token_for_push_with_parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"device_token_sent"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

#pragma mark - Shared Accessors

+ (AppDelegate *)sharedAppDelegate
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate;
}

+ (ShoppingCart *)sharedShoppingCart
{
    AppDelegate *appDelegate = [AppDelegate sharedAppDelegate];
    ShoppingCart *sharedShoppingCart = [appDelegate shoppingCart];
    
    return sharedShoppingCart;
}

#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store
 coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return _managedObjectContext;
}


// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BarAppModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}


// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Core_Data.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{ NSMigratePersistentStoresAutomaticallyOption : @(YES),
                               NSInferMappingModelAutomaticallyOption : @(YES) };
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}




-(id)init {
    self = [super init];
    
    if(self) {
    }
    
    return self;
}

@end
