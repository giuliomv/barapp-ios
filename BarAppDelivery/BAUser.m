//
//  Category.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "BAUser.h"
#import "AppDelegate.h"
#import "PDKeychainBindings.h"
#import "PDKeychainBindingsController.h"

@implementation BAUser

+(BOOL)isUserLoggedIn{
    return [[PDKeychainBindings sharedKeychainBindings]objectForKey:@"authToken"] != nil;
}

+(void)logOutCurrentUser{
    [[PDKeychainBindings sharedKeychainBindings]removeObjectForKey:@"authToken"];
}

+(BAUser*)getLoggedInUser{
    NSManagedObjectContext* context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *comboEntity = [NSEntityDescription entityForName:@"BAUser" inManagedObjectContext:context];
    [request setEntity:comboEntity];
    NSArray *users = [context executeFetchRequest:request error:&error];
    return [users lastObject];
}

@end
