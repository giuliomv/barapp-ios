//
//  Expiry.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 23/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#ifndef Expiry_h
#define Expiry_h


#endif /* Expiry_h */

@interface Expiry : NSObject

@property (assign, nonatomic) NSDate* rawValue;
@property (assign, nonatomic) int month;
@property (assign, nonatomic) int year;

-(id)initWithString:(NSString*) str;
-(id) expiryByMonth: (NSString*)month andYear: (NSString*) year;

@end
