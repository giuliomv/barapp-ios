//
//  OrderDetailsViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 26/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "OrderDetailsViewController.h"
#import "CategoriesViewController.h"
#import "CustomOrderDetailCell.h"
#import "ConfirmedOrderViewController.h"
#import "RequestBuilder.h"
#import "AppDelegate.h"
#import "ShoppingCartItem.h"
#import <UIKit/UIKit.h>
#import <Google/Analytics.h>
#import "AJWValidator.h"
#import "UIColorHelper.h"
#import "CustomLoaderView.h"
#import "BAUser.h"
#import "PaymentMethodOrderDetailsCell.h"
#import "CardItem+CoreDataClass.h"
#import "CardsListViewController.h"
#import "Constants.h"
#import "UIColorHelper.h"

#define kInputValidator @"kInputValidator"
#define kInputField @"kInputField"
#define kErrorView @"kErrorView"

@interface OrderDetailsViewController ()<UITextFieldDelegate, UITextViewDelegate, AJWValidatorDelegate>{
    CustomLoaderView *activityLoader;
}

@property (nonatomic, strong) UITextField *activeField;
@property (strong, nonatomic) NSArray *validationData;
@property (strong, nonatomic) NSString *preferred_payment_method;
@property (strong, nonatomic) NSNumber *card_selected_id;

- (void)keyboardWillBeHidden:(NSNotification *)sender;
- (void)keyboardWasShown:(NSNotification *)sender;

@end

@implementation OrderDetailsViewController

@synthesize nameTextField = _nameTextField;
@synthesize addressTextField = _addressTextField;
@synthesize cellphoneTextField = _cellphoneTextField;
@synthesize contentScrollView = _contentScrollView;
@synthesize totalLabel = _totalLabel;
@synthesize activeField = _activeField;
@synthesize referencesTextView = _referencesTextView;
@synthesize addressNumberTextField = _addressNumberTextField;
@synthesize addressDptoTextField = _addressDptoTextField;
@synthesize nameErrorView = _nameErrorView;
@synthesize phoneErrorView = _phoneErrorView;
@synthesize addressErrorView = _addressErrorView;
@synthesize referencesErrorView = _referencesErrorView;
@synthesize confirmOrderButton = _confirmOrderButton;
@synthesize addressNumberBottomView = _addressNumberBottomView;
@synthesize addressDptoBottomView = _addressDptoBottomView;
@synthesize cashAmountTextField = _cashAmountTextField;
@synthesize cashAmountBottomView = _cashAmountBottomView;
@synthesize paymentMethodContentView = _paymentMethodContentView;
@synthesize paymentMethodButton = _paymentMethodButton;
@synthesize paymentMethodImageView = _paymentMethodImageView;
@synthesize paymentMethodNameLabel = _paymentMethodNameLabel;
@synthesize confirmButtonTopConstraint = _confirmButtonTopConstraint;


- (void)viewDidLoad {
    [super viewDidLoad];
    _selectedLocationCoordinates = [AppDelegate sharedAppDelegate].selectedLocationCoordinates;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardWillBeHidden:)];
    [_contentScrollView addGestureRecognizer:gestureRecognizer];
    [self setupUIElements];
    [self setupPaymentMethodButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupValidations];
    [self setupNavigationView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [self trackPageWithGA];
}

-(void)viewDidAppear:(BOOL)animated{
    [self setupInputBorderToView:_addressDptoBottomView];
    [self setupInputBorderToView:_referencesErrorView];
    for (int i=0; i < self.validationData.count; i++) {
        [self setupInputBorderToView:((UIView *)[self.validationData objectAtIndex:i][kErrorView])];
    }
    [self addressValidated];
    [self userInfoValidated];
    [self addressNumberValidated];
    [self cashAmountValidated];
}

-(void)userInfoValidated{
    if ([BAUser isUserLoggedIn]) {
        BAUser *currentUser = [BAUser getLoggedInUser];
        _nameTextField.text = currentUser.fullName;
        _cellphoneTextField.text = currentUser.phoneNumber;
        [((AJWValidator*)[self.validationData objectAtIndex:0][kInputValidator]) validate:_nameTextField.text];
        [((AJWValidator*)[self.validationData objectAtIndex:1][kInputValidator]) validate:_cellphoneTextField.text];
    }
}

-(void)viewDidLayoutSubviews{
    
}

-(void)setupUIElements{
    double total = ([[AppDelegate sharedShoppingCart] total]) ;
    NSDictionary * couponObj = [AppDelegate sharedAppDelegate].couponApplied;
    if (couponObj.count>0) {
        NSNumber * discount = couponObj[@"discount_amount"];
        total = [[AppDelegate sharedShoppingCart] total] * (100 - discount.intValue)*0.01f;
    }
    _totalLabel.text = [NSString stringWithFormat:@"S/. %.02f", total + [AppDelegate sharedAppDelegate].deliveryFee.floatValue];
    [_referencesTextView setTextContainerInset:UIEdgeInsetsZero];
    
    [_cellphoneToolBar sizeToFit];
    _cellphoneTextField.inputAccessoryView = _cellphoneToolBar;
}

-(void)setupPaymentMethodButton{
    _preferred_payment_method = [[NSUserDefaults standardUserDefaults] valueForKey:@"preferred_payment_method"];
    NSString* paymentMethodIconName = @"";
    if ([_preferred_payment_method isEqualToString:@"pos"]) {
        [_paymentMethodNameLabel setText:@"POS"];
        paymentMethodIconName = @"icon_POS";
        [_cashAmountTextField setText:@""];
        [_cashAmountTextField setHidden:true];
        [_cashAmountBottomView setHidden:true];
        _confirmButtonTopConstraint.constant = 140;
    }else if ([_preferred_payment_method isEqualToString:@"cash"]){
        [_cashAmountTextField setHidden:false];
        [_cashAmountBottomView setHidden:false];
        [_paymentMethodNameLabel setText:@"CASH"];
        paymentMethodIconName = @"icon_CASH";
        _confirmButtonTopConstraint.constant = 200;
    }else{
        [_cashAmountTextField setText:@""];
        [_cashAmountTextField setHidden:true];
        [_cashAmountBottomView setHidden:true];
        CardItem* card = [CardItem getCardById:_preferred_payment_method context:[[AppDelegate sharedAppDelegate] managedObjectContext]];
        [_paymentMethodNameLabel setText:[NSString stringWithFormat:@"*%@",card.lastFour]];
        paymentMethodIconName = [NSString stringWithFormat:@"card_%@", [card.cardBrand lowercaseString]];
        _card_selected_id = card.itemId;
        _confirmButtonTopConstraint.constant = 140;
    }
    [_paymentMethodImageView setImage:[UIImage imageNamed:paymentMethodIconName]];
    [_paymentMethodButton setBackgroundImage:[UIColorHelper imageWithColor:[UIColorHelper practicalColorWithRed:75.0 green:75.0 blue:75.0 alpha:0.5] andBounds:_paymentMethodButton.bounds] forState:UIControlStateHighlighted];
    [_paymentMethodButton setBackgroundImage:[UIColorHelper imageWithColor:[UIColor clearColor] andBounds:_paymentMethodButton.bounds] forState:UIControlStateNormal];
}

-(void) showMarkerLoader{
    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando"];
    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

-(void)markValidField:(UITextField *)textField inErrorView:(UIView*)errorView {
    ((CALayer *)[errorView.layer.sublayers lastObject]).backgroundColor = [[UIColorHelper practicalColorWithRed:117.0 green:117.0 blue:117.0 alpha:1.0] CGColor];
    ((UILabel *)[errorView.subviews firstObject]).text = @"";
}


-(void)markInvalidField:(UITextField *)textField inErrorView:(UIView*)errorView forValidation:(AJWValidator*)validator {
    ((CALayer *)[errorView.layer.sublayers lastObject]).backgroundColor = [[UIColorHelper practicalColorWithRed:211.0 green:47.0 blue:47.0 alpha:1.0] CGColor];
    ((UILabel *)[errorView.subviews firstObject]).text = [validator.errorMessages firstObject];
}

-(void)textFieldTextChanged:(id)sender{
    
}

-(void)textViewDidChange:(UITextView *)textView{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_nameTextField resignFirstResponder];
    [_addressTextField resignFirstResponder];
    [_addressNumberTextField resignFirstResponder];
    [_addressDptoTextField resignFirstResponder];
    [_cashAmountTextField resignFirstResponder];
    
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(nonnull NSString *)text{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)setupValidations{
    self.validationData = @[
       @{
           kInputValidator: [self nameValidator],
           kInputField: _nameTextField,
           kErrorView:_nameErrorView
           },
       @{
           kInputValidator: [self phoneValidator],
           kInputField: _cellphoneTextField,
           kErrorView:_phoneErrorView
           },
       @{
           kInputValidator: [self addressValidator],
           kInputField:_addressTextField,
           kErrorView:_addressErrorView
           },
       @{
           kInputValidator: [self addressNumberValidator],
           kInputField:_addressNumberTextField,
           kErrorView:_addressNumberBottomView
           },
       @{
           kInputValidator: [self cashAmountValidator],
           kInputField:_cashAmountTextField,
           kErrorView:_cashAmountBottomView
           }
       ];
    for (int i=0; i < self.validationData.count; i++) {
        [self setupValidatorStateChangeHandler:[self.validationData objectAtIndex:i][kInputValidator] withErrorView:[self.validationData objectAtIndex:i][kErrorView] forInputField:[self.validationData objectAtIndex:i][kInputField]];
        [[self.validationData objectAtIndex:i][kInputField] ajw_attachValidator:[self.validationData objectAtIndex:i][kInputValidator]];
        if ([[self.validationData objectAtIndex:i][kInputField] isKindOfClass:[UITextField class]]) {
            ((UITextField *)[self.validationData objectAtIndex:i][kInputField]).delegate = self;
        } else {
            ((UITextView *)[self.validationData objectAtIndex:i][kInputField]).delegate = self;
        }
    }
}

-(void)setupInputBorderToView:(UIView *)view{
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColorHelper practicalColorWithRed:117.0 green:117.0 blue:117.0 alpha:1.0] CGColor];
    upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(view.frame), 1.0f);
    [view.layer addSublayer:upperBorder];
}

- (AJWValidator *)nameValidator
{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    validator.delegate = self;
    [validator addValidationToEnsurePresenceWithInvalidMessage:@"No olvides dejar tu nombre."];
    return validator;
}

-(void)setupValidatorStateChangeHandler:(AJWValidator*)validator withErrorView:(UIView*)view forInputField:(UITextField*)inputField{
    validator.validatorStateChangedHandler = ^(AJWValidatorState newState) {
        switch (newState) {
                
            case AJWValidatorValidationStateValid:
                [self markValidField:inputField inErrorView:view];
                break;
                
            case AJWValidatorValidationStateInvalid:
                [self markInvalidField:inputField inErrorView:view forValidation:validator];
                break;
                
            case AJWValidatorValidationStateWaitingForRemote:
                // do loading indicator things
                break;
                
        }
        [self allowSubmit:[self validateInputs]];
    };
}

-(void)allowSubmit:(BOOL) allowing{
    _confirmOrderButton.enabled = allowing;
    if (allowing) {
        [_confirmOrderButton setBackgroundColor:[UIColorHelper practicalColorWithRed:65.0 green:18.0 blue:139.0 alpha:1.0]];
    } else {
        [_confirmOrderButton setBackgroundColor:[UIColorHelper practicalColorWithRed:189.0 green:189.0 blue:189.0 alpha:1.0]];
    }
}

- (AJWValidator *)phoneValidator
{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"^(\\d{9})$" invalidMessage:@"Debe tener 9 números"];
    return validator;
}

- (AJWValidator *)addressNumberValidator
{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:@"Coloca tu número de puerta"];
    return validator;
}


- (AJWValidator *)addressValidator{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:@"No olvides dejar tu dirección"];
    return validator;
}

- (AJWValidator *)referenceValidator{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:@"Ayúdanos a llegar más rápido"];
    return validator;
}

- (AJWValidator *)cashAmountValidator{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeNumeric];
    [validator addValidationToEnsureCustomConditionIsSatisfiedWithBlock:^BOOL(NSString *instance) {
        if ([_preferred_payment_method isEqualToString:@"cash"]) {
            if (![instance isEqualToString:@""]) {
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                formatter.numberStyle = NSNumberFormatterDecimalStyle;
                NSNumber *myNumber = [formatter numberFromString:instance];
                
                if (myNumber != nil) {
                    double total = ([[AppDelegate sharedShoppingCart] total]) ;
                    NSDictionary * couponObj = [AppDelegate sharedAppDelegate].couponApplied;
                    if (couponObj.count>0) {
                        NSNumber * discount = couponObj[@"discount_amount"];
                        total = [[AppDelegate sharedShoppingCart] total] * (100 - discount.intValue)*0.01f;
                    }
                    float totalAmount = total + [AppDelegate sharedAppDelegate].deliveryFee.floatValue;
                    if (myNumber.floatValue >= totalAmount) {
                        return true;
                    } else {
                        return false;
                    }
                }else {
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return true;
        }
        return ([instance rangeOfString:@"A"].location == NSNotFound);
    } invalidMessage:@"Por favor, ingrese un monto correcto"];
    return validator;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UIViewController* destination = [segue destinationViewController];
    if (destination.class == CardsListViewController.class) {
        ((CardsListViewController*)destination).origin = card_list_origin_order_details;
        ((CardsListViewController*)destination).parentVC = self;    
    }
}

-(BOOL)validateInputs{
    BOOL validated = TRUE;
    for (int i=0; i < self.validationData.count; i++) {
        validated &= ((AJWValidator *)[self.validationData objectAtIndex:i][kInputValidator]).state ==AJWValidatorValidationStateValid;
    }
    return validated;
}

- (void)showValidationAlert:(NSString *) message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Información"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

- (IBAction)confirmOrderButtonAction:(id)sender {
    if ([self validateInputs]){
        if ([AppDelegate sharedAppDelegate].internetConnection) {
            ((UIButton*)sender).userInteractionEnabled = NO;
            [self showMarkerLoader];
            NSMutableArray *products = [NSMutableArray array];
            for (int i = 0; i < [[AppDelegate sharedShoppingCart] shoppingCartProductsItems].count; i++) {
                ShoppingCartItem *sci = [[[AppDelegate sharedShoppingCart] shoppingCartProductsItems] objectAtIndex:i];
                [products addObject:@{@"product_id": sci.itemId,
                                      @"qty": sci.qty,
                                      @"subtotal": [NSNumber numberWithFloat:sci.qty.intValue * sci.price.floatValue]}];
            }
            NSMutableArray *combos = [NSMutableArray array];
            for (int i = 0; i < [[AppDelegate sharedShoppingCart] shoppingCartCombosItems].count; i++) {
                ShoppingCartItem *sci = [[[AppDelegate sharedShoppingCart] shoppingCartCombosItems] objectAtIndex:i];
                [combos addObject:@{@"combo_id": sci.itemId,
                                    @"qty": sci.qty,
                                    @"subtotal": [NSNumber numberWithFloat:sci.qty.intValue * sci.price.floatValue]}];
            }
            
            RequestBuilder *r = [[RequestBuilder alloc] init];
            NSString * completeAddressString = [NSString stringWithFormat:@"%@, %@", _addressTextField.text, _addressNumberTextField.text];
            NSNumber* cashAmount = [NSNumber numberWithFloat: 0.0];
            NSString* selectedPaymentMethod = @"";
            if ([_preferred_payment_method isEqualToString:@"cash"]) {
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                formatter.numberStyle = NSNumberFormatterDecimalStyle;
                cashAmount = [formatter numberFromString:_cashAmountTextField.text];
                selectedPaymentMethod = @"CASH";
            }else if ([_preferred_payment_method isEqualToString:@"pos"]){
                selectedPaymentMethod = @"VISA";
            }else{
                selectedPaymentMethod = @"online";
            }
            
            if (![_addressDptoTextField.text isEqualToString:@""]) {
                completeAddressString = [completeAddressString stringByAppendingString: [NSString stringWithFormat:@" - %@", _addressDptoTextField.text]];
            }
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"order": @{
                                                                                @"contact_name":_nameTextField.text,
                                                                                @"delivery_address": completeAddressString,
                                                                                @"address_reference": _referencesTextView.text,
                                                                                @"contact_phone": _cellphoneTextField.text,
                                                                                @"company_id": [AppDelegate sharedAppDelegate].closestCompany,
                                                                                @"platform": @"iOS",
                                                                                @"total": [NSNumber numberWithFloat:[[AppDelegate sharedShoppingCart] total]],
                                                                                @"cash_amount": cashAmount,
                                                                                @"payment_method": selectedPaymentMethod,
                                                                                @"state_id": @1,
                                                                                @"latitude": [NSNumber numberWithDouble:_selectedLocationCoordinates.latitude],
                                                                                @"longitude": [NSNumber numberWithDouble: _selectedLocationCoordinates.longitude],
                                                                                @"order_details_attributes": products,
                                                                                @"combo_orders_attributes": combos}
                                                                                          }];
            if ([AppDelegate sharedAppDelegate].couponApplied.count > 0) {
                [params setObject:[AppDelegate sharedAppDelegate].couponApplied forKey:@"coupon"];
            }
            
            if ([AppDelegate sharedAppDelegate].deliveryFee) {
                [params setObject:[AppDelegate sharedAppDelegate].deliveryFee forKey:@"delivery_fee"];
            }
            
            if ([selectedPaymentMethod isEqualToString:@"online"]) {
                [params setObject:@{@"id_card": _card_selected_id} forKey:@"payment_data"];
            }
            
            [r create_order_with_company:[NSNumber numberWithInteger:2] parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){

                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
                    @autoreleasepool {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            [self trackOrderCreated];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"order_created" object:nil userInfo:responseObject];
                            [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
                            ((UIButton*)sender).userInteractionEnabled = YES;
                            [activityLoader stopAndRemoveLoader];
                        });
                    }
                });

            } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){

                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
                    @autoreleasepool {
                        ((UIButton*)sender).userInteractionEnabled = YES;
                        [activityLoader stopAndRemoveLoader];
                        NSLog(@"FAIL: %@", responseObject);
                    }
                });

            }];

        }else{
            [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
        }
    }else{
        [self showValidationAlert:@"Por favor, complete todos los campos para hacer su pedido."];
    }
}

-(void)trackOrderCreated{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"accion_ui" action:@"click_confirmar" label:nil value:nil] build]];
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _contentScrollView.contentInset = contentInsets;
    _contentScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _activeField.frame.origin) ) {
        [_contentScrollView scrollRectToVisible:_activeField.frame animated:YES];
    }

}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [_nameTextField endEditing:YES];
    [_addressTextField endEditing:YES];
    [_referencesTextView endEditing:YES];
    [_cellphoneTextField endEditing:YES];
    [_addressNumberTextField endEditing:YES];
    [_addressDptoTextField endEditing:YES];

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    _contentScrollView.contentInset = contentInsets;
    _contentScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeField = nil;
}

-(void)setupNavigationView{
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.navigationItem.title attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
}

-(void) addressValidated{
    _addressTextField.text = [AppDelegate sharedAppDelegate].selectedLocationAddress;
    [((AJWValidator*)[self.validationData objectAtIndex:2][kInputValidator]) validate:_addressTextField.text];
}

-(void) addressNumberValidated{
    [((AJWValidator*)[self.validationData objectAtIndex:3][kInputValidator]) validate:_addressNumberTextField.text];
}

-(void)cashAmountValidated{
    [((AJWValidator*)[self.validationData objectAtIndex:4][kInputValidator]) validate:@""];
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Página de formulario de pedido"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
}

- (IBAction)goBack:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}


- (IBAction)resignPhoneNumberResponder:(id)sender {
    [_cellphoneTextField resignFirstResponder];
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    PaymentMethodOrderDetailsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"paymentMethodOrderDetailsCellId"];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

-(void)configureCell:(PaymentMethodOrderDetailsCell*)cell atIndexPath:(NSIndexPath*)indexPath{
}

- (void)cardsListDidClosed{
    [self setupPaymentMethodButton];
    [self cashAmountValidated];
}

- (IBAction)showCardsList:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CardsListViewController *cvc = (CardsListViewController *)[storyboard instantiateViewControllerWithIdentifier:@"cardsListViewControllerId"];
    cvc.origin = card_list_origin_order_details;
    cvc.parentVC = self;
    
    UINavigationController *cardsNavigationViewController = [[UINavigationController alloc] initWithRootViewController:cvc];
    [self.navigationController presentViewController:cardsNavigationViewController animated:true completion:nil];
}

@end
