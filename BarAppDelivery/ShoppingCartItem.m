//
//  ShoppingCartItem.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 13/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "ShoppingCartItem.h"

@implementation ShoppingCartItem

@synthesize itemId = _itemId;
@synthesize name = _name;
@synthesize price = _price;
@synthesize qty = _qty;
@synthesize isCombo = _isCombo;
@synthesize itemImage = _itemImage;
@synthesize isAlcoholic = _isAlcoholic;

-(id)initWithItemId:(NSNumber *) itemId
               name:(NSString *) name
              price:(NSNumber *) price
                qty:(NSNumber *) qty
            isCombo:(bool) isCombo
          itemImage:(NSString *) itemImage
    itemDescription:(NSString*) itemDescription
        isAlcoholic:(bool) isAlcoholic{
    self = [super init];
    if (self) {
        _itemId = itemId;
        _name = name;
        _price = price;
        _qty = qty;
        _isCombo = isCombo;
        _itemImage = itemImage;
        _itemDescription = itemDescription;
        _isAlcoholic = isAlcoholic;
    }
    return self;
}

@end
