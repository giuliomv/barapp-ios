//
//  CardRegistrationViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 21/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "CardRegistrationViewController.h"
#import "UIColorHelper.h"
#import <Caishen/Caishen-Swift.h>
#import "Luhn.h"
#import "Expiry.h"
#import "AJWValidator.h"
#import "CMPopTipView.h"
#import "Constants.h"
#import "BAUser.h"
#import "RequestBuilder.h"
#import "AppDelegate.h"
#import "CardsListViewController.h"
#import "CardItem+CoreDataClass.h"
#import "CustomLoaderView.h"
#import <Google/Analytics.h>

@import Culqi;

#define kInputValidator @"kInputValidator"
#define kInputField @"kInputField"
#define kErrorView @"kErrorView"

@interface CardRegistrationViewController (){
    CustomLoaderView *activityLoader;
}

@property (strong, nonatomic) NSArray *validationData;
@property (nonatomic, strong) CMPopTipView *currentPopTipViewTarget;
@property (nonatomic, strong) NSString *culqiToken;

- (void)keyboardWillBeHidden:(NSNotification *)sender;
- (void)keyboardWasShown:(NSNotification *)sender;

@end

@implementation CardRegistrationViewController

@synthesize cardNumberTextField = _cardNumberTextField;
@synthesize monthExpiryTextField = _monthExpiryTextField;
@synthesize yearExpiryTextField = _yearExpiryTextField;
@synthesize cvvTextField = _cvvTextField;
@synthesize cardTypeImageView = _cardTypeImageView;
@synthesize cardNumberErrorView = _cardNumberErrorView;
@synthesize expiryErrorView = _expiryErrorView;
@synthesize cvvErrorView = _cvvErrorView;
@synthesize bottomFromSaveButtonConstraint = _bottomFromSaveButtonConstraint;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    [self creditCardFieldsSetup];
    [self hideKeyboard];
    [self setupCulqi];
    // Do any additional setup after loading the view.
}

-(void)setupCulqi{
    [Culqi setApiKey:culqi_public_key];
}

-(void)hideKeyboard{
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardWillBeHidden:)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

-(void)viewWillAppear:(BOOL)animated{
    [self setupNavigationBar];
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneDragRevealEnabled:false forDirection:MSDynamicsDrawerDirectionLeft];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [_bottomFromSaveButtonConstraint setConstant:kbSize.height+ 20.0];
    
}

-(void)setupNavigationBar{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Gobold" size:14]}];
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"MÉTODO DE PAGO" attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
}

-(void)trackPageWithGA:(NSString*) origin{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:origin];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(BOOL)validateInputs{
    BOOL validated = TRUE;
    for (int i=0; i < self.validationData.count; i++) {
        validated &= ((AJWValidator *)[self.validationData objectAtIndex:i][kInputValidator]).state ==AJWValidatorValidationStateValid;
    }
    return validated;
}

-(void)creditCardFieldsSetup{
    
    _cardNumberTextField.numberInputTextFieldDelegate = self;
    _monthExpiryTextField.cardInfoTextFieldDelegate = self;
    _yearExpiryTextField.cardInfoTextFieldDelegate = self;
    _cvvTextField.cardInfoTextFieldDelegate = self;
    _monthExpiryTextField.deleteBackwardCallback = ^(UITextField * _Nonnull mTextField) {
        [_cardNumberTextField becomeFirstResponder];
    };
    _yearExpiryTextField.deleteBackwardCallback = ^(UITextField * _Nonnull mTextField) {
        [_monthExpiryTextField becomeFirstResponder];
    };
    _cvvTextField.deleteBackwardCallback = ^(UITextField * _Nonnull mTextField) {
        [_yearExpiryTextField becomeFirstResponder];
    };
    _cardTypeImageView.image = [UIImage imageNamed:@"card_unknown"];
}

-(void)setupValidations{
    self.validationData = @[
                            @{
                                kInputValidator: [self cardNumberValidator],
                                kInputField: _cardNumberTextField,
                                kErrorView:_cardNumberErrorView
                                },
                            @{
                                kInputValidator: [self expiryValidator],
                                kInputField: _monthExpiryTextField,
                                kErrorView:_expiryErrorView
                                },
                            @{
                                kInputValidator: [self expiryValidator],
                                kInputField:_yearExpiryTextField,
                                kErrorView:_expiryErrorView
                                },
                            @{
                                kInputValidator: [self cvvValidator],
                                kInputField:_cvvTextField,
                                kErrorView:_cvvErrorView
                                }
                            ];

}

-(void) showMarkerLoader{
    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando"];
    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

-(void)setupInputBorderToView:(UIView *)view{
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColorHelper practicalColorWithRed:180.0 green:180.0 blue:180.0 alpha:1.0] CGColor];
    upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(view.frame), 1.0f);
    [view.layer addSublayer:upperBorder];
}

-(void)markFocusedField:(UITextField *)textField inErrorView:(UIView*)errorView {
    ((CALayer *)[errorView.layer.sublayers lastObject]).backgroundColor = [[UIColorHelper practicalColorWithRed:65.0 green:18.0 blue:142.0 alpha:1.0] CGColor];
    ((UILabel *)[errorView.subviews firstObject]).text = @"";
}

-(void)markCompletedField:(UITextField *)textField inErrorView:(UIView*)errorView {
    ((UILabel *)[errorView.subviews firstObject]).text = @"";
}

-(void)markFieldUnhighlighted:(UITextField *)textField inErrorView:(UIView*)errorView {
    ((CALayer *)[errorView.layer.sublayers lastObject]).backgroundColor = [[UIColorHelper practicalColorWithRed:180.0 green:180.0 blue:180.0 alpha:1.0] CGColor];
}


-(void)markInvalidField:(UITextField *)textField inErrorView:(UIView*)errorView withErrorMessage:(NSString *)errorMessage {
    ((UILabel *)[errorView.subviews firstObject]).text = errorMessage;
}


#pragma mark Field Validators
-(AJWValidator *)cardNumberValidator{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeNumeric];
    [validator addValidationToEnsureCustomConditionIsSatisfiedWithBlock:^BOOL(NSString *instance) {
        return [instance isValidCreditCardNumber];
    } invalidMessage:@"Por favor, ingrese un número correcto"];
    return validator;
}

-(AJWValidator *)expiryValidator{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeNumeric];
    [validator addValidationToEnsureCustomConditionIsSatisfiedWithBlock:^BOOL(NSString *instance) {
        Expiry *expiration = [[Expiry alloc] initWithString:[NSString stringWithFormat:@"%@/%@", _monthExpiryTextField.text, _yearExpiryTextField.text]];
        return expiration!=nil;
    } invalidMessage:@"Por favor, ingrese una fecha de expiración correcta"];
    return validator;
}

-(AJWValidator *)cvvValidator{
    AJWValidator *validator = [AJWValidator validatorWithType:AJWValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"/^[0-9]{3,4}$/" invalidMessage:@"Ingrese un CVV correcto"];
    return validator;
}

-(void)viewDidAppear:(BOOL)animated{
    [self setupInputBorderToView:_cardNumberErrorView];
    [self setupInputBorderToView:_expiryErrorView];
    [self setupInputBorderToView:_cvvErrorView];
}


//func cardTextFieldShouldShowAccessoryImage(_ cardTextField: CardTextField) -> UIImage? {
//    return nil
//}
//
//func cardTextFieldShouldProvideAccessoryAction(_ cardTextField: CardTextField) -> (() -> ())? {
//    return nil
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)numberInputTextFieldDidChangeText:(NumberInputTextField * _Nonnull)numberInputTextField {
    [self allowSubmit];
    [self showCardImage];
}

-(BOOL)cvvValid{
    NSString *ccNumber = _cardNumberTextField.text;
    OLCreditCardType cardType = [ccNumber creditCardType];
    NSString *cvvLength = (cardType == OLCreditCardTypeAmex ? @"4" : @"3");
    NSPredicate* cvvRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", [NSString stringWithFormat:@"^[0-9]{%@}$",cvvLength]];
    NSString * t = _cvvTextField.text;
    BOOL b = [cvvRegex evaluateWithObject:t];
    return b;
}

-(BOOL)expiryDateValid{
    Expiry *expiration = [[Expiry alloc] initWithString:[NSString stringWithFormat:@"%@/%@", _monthExpiryTextField.text, _yearExpiryTextField.text]];
    return expiration!=nil;
}

-(BOOL)cardNumberValid{
    return [_cardNumberTextField.text isValidCreditCardNumber];
}

-(BOOL)validateCardInfo{
    return [self expiryDateValid] && [self cardNumberValid] && [self cvvValid];
}

-(void)showCardImage{
    NSString *ccNumber = _cardNumberTextField.text;
    OLCreditCardType cardType = [ccNumber creditCardType];
    switch (cardType) {
        case OLCreditCardTypeAmex:
            _cardTypeImageView.image = [UIImage imageNamed:@"card_amex"];
            break;
        case OLCreditCardTypeVisa:
            _cardTypeImageView.image = [UIImage imageNamed:@"card_visa"];
            break;
        case OLCreditCardTypeMastercard:
            _cardTypeImageView.image = [UIImage imageNamed:@"card_mastercard"];
            break;
        case OLCreditCardTypeDinersClub:
            _cardTypeImageView.image = [UIImage imageNamed:@"card_diners_club"];
            break;
        default:
            _cardTypeImageView.image = [UIImage imageNamed:@"card_unknown"];
            break;
    }
    
}

- (void)numberInputTextFieldDidComplete:(NumberInputTextField * _Nonnull)numberInputTextField {
    [self allowSubmit];
    [self markCompletedField:_cardNumberTextField inErrorView:_cardNumberErrorView];
    [_monthExpiryTextField becomeFirstResponder];
}

-(void)numberInputTextFieldIsInvalid:(NumberInputTextField * _Nonnull)numberInputTextField {
    [self allowSubmit];
    [self markInvalidField:_cardNumberTextField inErrorView:_cardNumberErrorView withErrorMessage:@"Por favor, ingrese un número correcto de tarjeta"];
}

-(void)cardDetailTextFieldDidBeginEditing:(UITextField *)textField{
    if ([textField isKindOfClass:[MonthInputTextField class]] || [textField isKindOfClass:[YearInputTextField class]]) {
        [self markFocusedField:_monthExpiryTextField inErrorView:_expiryErrorView];
    }else if ([textField isKindOfClass:[CVCInputTextField class]]){
        [self markFocusedField:_cvvTextField inErrorView:_cvvErrorView];
    }
}

-(void)cardDetailTextFieldDidEndEditing:(UITextField *)textField{
    if ([textField isKindOfClass:[MonthInputTextField class]] || [textField isKindOfClass:[YearInputTextField class]]) {
        [self markFieldUnhighlighted:_monthExpiryTextField inErrorView:_expiryErrorView];
    }else if ([textField isKindOfClass:[CVCInputTextField class]]){
        [self markFieldUnhighlighted:_cvvTextField inErrorView:_cvvErrorView];
    }
}

-(void)cardNumberTextFieldDidEndEditing:(NumberInputTextField *)numberInputTextField{
    [self markFieldUnhighlighted:_cardNumberTextField inErrorView:_cardNumberErrorView];
}

-(void)cardNumberTextFieldDidBeginEditing:(NumberInputTextField *)numberInputTextField{
    [self markFocusedField:_cardNumberTextField inErrorView:_cardNumberErrorView];
}

- (void)setupCardTypeImage:(UIImage * _Nonnull)cardTypeImage {
    _cardTypeImageView.image = cardTypeImage;
}


- (void)textField:(UITextField * _Nonnull)textField didEnterOverflowInfo:(NSString * _Nonnull)overFlowDigits {
}

- (void)allowSubmit {
    [_saveButton setEnabled:[self validateCardInfo]];
    if ([self validateCardInfo]) {
        [_saveButton setBackgroundColor:[UIColorHelper practicalColorWithRed:65.0 green:18.0 blue:142.0 alpha:1.0]];
    } else {
        [_saveButton setBackgroundColor:[UIColorHelper practicalColorWithRed:189.0 green:189.0 blue:189.0 alpha:1.0]];
    }
}

- (void)textField:(UITextField * _Nonnull)textField didEnterPartiallyValidInfo:(NSString * _Nonnull)didEnterPartiallyValidInfo {
    [self allowSubmit];
}

- (void)textField:(UITextField * _Nonnull)textField didEnterValidInfo:(NSString * _Nonnull)didEnterValidInfo {
    if ([textField isKindOfClass:[MonthInputTextField class]]) {
        [_yearExpiryTextField becomeFirstResponder];
    } else if([textField isKindOfClass:[YearInputTextField class]]){
        [_cvvTextField becomeFirstResponder];
        [self markCompletedField:_yearExpiryTextField inErrorView:_expiryErrorView];
    }else if ([textField isKindOfClass:[CVCInputTextField class]]){
        [self markCompletedField:_cvvTextField inErrorView:_cvvErrorView];
    }
    [self allowSubmit];
}

-(void)textField:(UITextField *)textField didEnterInvalidInfo:(NSString *)didEnterInvalidInfo{
    [self allowSubmit];
}

- (BOOL)validateCVV: (NSString *) cvvString{
    OLCreditCardType cardType = [_cardNumberTextField.text creditCardType];
    if (cardType == OLCreditCardTypeAmex) {
        return (cvvString.length <= 4);
    }else{
        return (cvvString.length <= 3);
    }
}

- (IBAction)saveCard:(id)sender {
    [self createCulqiToken];
}

-(void)createCulqiToken{
    [self showMarkerLoader];
    NSString * rawCardNumber = [_cardNumberTextField.text stringByReplacingOccurrencesOfString:_cardNumberTextField.cardNumberSeparator withString:@""];
    NSString * rawYear = [NSString stringWithFormat:@"20%@",_yearExpiryTextField.text];
    [[Culqi sharedInstance] createTokenWithCardNumber: rawCardNumber
                                                  cvv:_cvvTextField.text
                                      expirationMonth:_monthExpiryTextField.text
                                       expirationYear: rawYear
                                                email:[BAUser getLoggedInUser].userEmail
                                             metadata:nil
                                              success:^(CLQResponseHeaders * _Nonnull responseHeaders, CLQToken * _Nonnull token) {
                                                    NSLog(@"Did create token with identifier: %@", token.identifier);
                                                  [self requestCardCreation:token.identifier];
                                              } failure:^(CLQResponseHeaders * _Nonnull responseHeaders, CLQError * _Nonnull businessError, NSError * _Nonnull error) {
                                                  [activityLoader stopAndRemoveLoader];
                                                  [self showCardCreationError:businessError.merchantMessage];
                                                  NSLog(@"Error Creating token\nLocalized error: %@\nBusiness Error: %@", error.localizedDescription, businessError.merchantMessage);
                                              }];
}

-(void)requestCardCreation:(NSString*) token{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"address":[[AppDelegate sharedAppDelegate] selectedLocationAddress],
                                                                                  @"address_city": @"Lima",
                                                                                  @"culqi_token": token}
                                                                                  ];
    [r create_user_card:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSObject *jsonObj = (NSObject *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if ([[jsonObj valueForKey:@"success"] boolValue]) {
                        NSError *error = nil;
                        CardItem *record = [[CardItem alloc] initWithEntity:[NSEntityDescription entityForName:@"CardItem" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
                        record.itemId = [[jsonObj valueForKey:@"card"] valueForKey:@"id"];
                        record.lastFour = [[jsonObj valueForKey:@"card"] valueForKey:@"last_four"];
                        record.cardBrand = [[jsonObj valueForKey:@"card"] valueForKey:@"card_brand"];;
                        
                        if (![self.managedObjectContext save:&error]) {
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                        [activityLoader stopAndRemoveLoader];
                        [self goBackToCardsList];
                        [(CardsListViewController *)self.parentVC cardWasCreated];
                    }else{
                        [activityLoader stopAndRemoveLoader];
                        [self showCardCreationError:[jsonObj valueForKey:@"card"]];
                    }
                    
                });
            }
        });
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

-(void)showCardCreationError:(NSString*)errorMessage{
    UIAlertController * alert= [UIAlertController
                                alertControllerWithTitle:@"Error al crear tarjeta"
                                message:errorMessage
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void)goBackToCardsList{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)didTapExpiryInfoButton:(id)sender {
    [self trackPageWithGA:@"Botón de información de fecha de expiración"];
    [self showInfoButtonsAlertsWithTitle:@"Fecha de expiración" content:@"Puedes encontrar la fecha debajo de tu número de tarjeta." fromSender:(UIButton*)sender];
}

- (IBAction)didTapCVVInfoButton:(id)sender {
    [self trackPageWithGA:@"Botón de información de CVV"];
    [self showInfoButtonsAlertsWithTitle:@"CVV" content:@"Este código de 3-4 dígitos sueles encontrarlo a la espalda de tu tarjeta de crédito." fromSender:(UIButton*)sender];
}

- (IBAction)closeCardRegistration:(id)sender {
    [self showCloseConfirmationModal];
    
}

- (IBAction)tapDisabledSaveButton:(id)sender {
    [self trackPageWithGA:@"Seleccionó el botón de Guardar cuando estaba deshabilitado"];
}

-(void)showCloseConfirmationModal{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"¿Seguro deseas salir?"
                                 message:@"Estás a punto de abandonar sin registrar tu tarjeta de crédito"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self trackPageWithGA:@"Aceptó salir del registro de tarjeta de crédito"];
                                    [self goBackToCardsList];
                                }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancelar"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self trackPageWithGA:@"Rechazó salir del registro de tarjeta de crédito"];
                                   }];
    
    [alert addAction:yesButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void)showInfoButtonsAlertsWithTitle:(NSString*)title content:(NSString*)content fromSender:(UIButton*)sender{
    if (nil == self.currentPopTipViewTarget) {
        self.currentPopTipViewTarget = [[CMPopTipView alloc] initWithTitle:title message:content];
        self.currentPopTipViewTarget.titleFont = [UIFont fontWithName:@"Aller-Bold" size:13.0];
        self.currentPopTipViewTarget.titleAlignment = NSTextAlignmentLeft;
        self.currentPopTipViewTarget.textFont = [UIFont fontWithName:@"Aller" size:13.0];
        self.currentPopTipViewTarget.textAlignment = NSTextAlignmentLeft;
        self.currentPopTipViewTarget.delegate = self;
        self.currentPopTipViewTarget.backgroundColor = [UIColorHelper practicalColorWithRed:65.0 green:18.0 blue:142.0 alpha:1.0];
        self.currentPopTipViewTarget.textColor = [UIColor whiteColor];
        self.currentPopTipViewTarget.titleColor = [UIColor whiteColor];
        self.currentPopTipViewTarget.borderColor = [UIColorHelper practicalColorWithRed:65.0 green:18.0 blue:142.0 alpha:1.0];
        self.currentPopTipViewTarget.borderWidth = 0.0;
        self.currentPopTipViewTarget.cornerRadius = 5.0;
        self.currentPopTipViewTarget.shouldEnforceCustomViewPadding = YES;
        self.currentPopTipViewTarget.dismissTapAnywhere = YES;
        self.currentPopTipViewTarget.has3DStyle = NO;
        self.currentPopTipViewTarget.hasShadow = NO;
        self.currentPopTipViewTarget.bubblePaddingX = 16;
        self.currentPopTipViewTarget.bubblePaddingY = 10;
        
        [self.currentPopTipViewTarget presentPointingAtView:sender inView:self.view animated:YES];
        //[self trackDeliveryIcon];
    }
    else {
        // Dismiss
        [self.currentPopTipViewTarget dismissAnimated:YES];
        self.currentPopTipViewTarget = nil;
    }
}

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView{
    self.currentPopTipViewTarget = nil;
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [_bottomFromSaveButtonConstraint setConstant:20.0];
    [_cardNumberTextField endEditing:YES];
    [_monthExpiryTextField endEditing:YES];
    [_yearExpiryTextField endEditing:YES];
    [_cvvTextField endEditing:YES];
}


@end
