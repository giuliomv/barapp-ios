//
//  Schedule+CoreDataProperties.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Schedule+CoreDataProperties.h"

@implementation Schedule (CoreDataProperties)

@dynamic scheduleId;
@dynamic dayOfWeek;
@dynamic initialHour;
@dynamic initialMnutes;
@dynamic endHour;
@dynamic endMinutes;

@end
