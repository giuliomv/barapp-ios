//
//  CardItem+CoreDataProperties.h
//  
//
//  Created by Giulio Mondoñedo on 28/05/18.
//
//

#import "CardItem+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CardItem (CoreDataProperties)

+ (NSFetchRequest<CardItem *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, copy) NSString *lastFour;
@property (nullable, nonatomic, copy) NSString *cardBrand;

@end

NS_ASSUME_NONNULL_END
