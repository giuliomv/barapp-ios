//
//  CardRegistrationViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 21/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Caishen/Caishen-Swift.h>
#import "AJWValidator.h"
#import "CMPopTipView.h"

@interface CardRegistrationViewController : UIViewController< NumberInputTextFieldDelegate, CardInfoTextFieldDelegate, CMPopTipViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet NumberInputTextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet MonthInputTextField *monthExpiryTextField;
@property (weak, nonatomic) IBOutlet YearInputTextField *yearExpiryTextField;
@property (weak, nonatomic) IBOutlet CVCInputTextField *cvvTextField;
@property (weak, nonatomic) IBOutlet UIImageView *cardTypeImageView;
@property (weak, nonatomic) IBOutlet UIView *cardNumberErrorView;
@property (weak, nonatomic) IBOutlet UIView *expiryErrorView;
@property (weak, nonatomic) IBOutlet UIView *cvvErrorView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) UIViewController *parentVC;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)saveCard:(id)sender;
- (IBAction)didTapExpiryInfoButton:(id)sender;
- (IBAction)didTapCVVInfoButton:(id)sender;
- (IBAction)closeCardRegistration:(id)sender;
- (IBAction)tapDisabledSaveButton:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomFromSaveButtonConstraint;


@end
