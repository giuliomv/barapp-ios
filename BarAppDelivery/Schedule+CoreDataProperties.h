//
//  Schedule+CoreDataProperties.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Schedule.h"

NS_ASSUME_NONNULL_BEGIN

@interface Schedule (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *scheduleId;
@property (nullable, nonatomic, retain) NSNumber *dayOfWeek;
@property (nullable, nonatomic, retain) NSNumber *initialHour;
@property (nullable, nonatomic, retain) NSNumber *initialMnutes;
@property (nullable, nonatomic, retain) NSNumber *endHour;
@property (nullable, nonatomic, retain) NSNumber *endMinutes;

@end

NS_ASSUME_NONNULL_END
