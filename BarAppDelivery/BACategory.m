//
//  Category.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "BACategory.h"
#import "AppDelegate.h"

@implementation BACategory

// Insert code here to add functionality to your managed object subclass

+(void)deleteAllCategories:(NSManagedObjectContext *)context{
    NSFetchRequest *allCategories = [[NSFetchRequest alloc] init];
    [allCategories setEntity:[NSEntityDescription entityForName:@"BACategory" inManagedObjectContext:context]];
    [allCategories setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *categories = [context executeFetchRequest:allCategories error:&error];
    //error handling goes here
    for (NSManagedObject *category in categories) {
        [context deleteObject:category];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

@end
