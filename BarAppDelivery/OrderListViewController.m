//
//  OrderListViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 1/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "OrderListViewController.h"
#import "CustomOrderListCell.h"
#import "RequestBuilder.h"
#import "OrderInfoViewController.h"
#import "UIColorHelper.h"
#import "AppDelegate.h"

@interface OrderListViewController ()

@end

@implementation OrderListViewController{
    NSMutableArray *previousOrders;
    NSMutableArray *activeOrders;
    enum order_states {recibido = 1, en_transito, aceptado, rechazado, finalizado, pago_pendiente};
    UIRefreshControl *refresher;
}

@synthesize orderListCollectionView = _orderListCollectionView;
@synthesize showingPreviousOrders = _showingPreviousOrders;
@synthesize emptyOrdersView = _emptyOrdersView;
@synthesize emptyOrdersMessageLabel = _emptyOrdersMessageLabel;

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.showingPreviousOrders = true;
    }
    return self;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.showingPreviousOrders = true;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    refresher = [[UIRefreshControl alloc] init];
    [refresher addTarget:self action:@selector(refreshCollection:) forControlEvents:UIControlEventValueChanged];
    _orderListCollectionView.refreshControl = refresher;
    [refresher beginRefreshing];
    [self initLists];
    [self requestMyOrders];
    // Do any additional setup after loading the view.
}

-(void)refreshCollection:(id)sender{
    [self requestMyOrders];
}

-(void)viewDidAppear:(BOOL)animated{
    [self setupInputBorderToView:_activeOrdersButton];
    [self setupInputBorderToView:_previousOrdersButton];
    [_activeOrdersButton setEnabled:_showingPreviousOrders];
    [_previousOrdersButton setEnabled:!_showingPreviousOrders];
    [self markButtonBorderView:_activeOrdersButton];
    [self markButtonBorderView:_previousOrdersButton];
}

-(void)viewWillAppear:(BOOL)animated{
    [self setupNavigationBar];
}

-(void)viewDidDisappear:(BOOL)animated{
    [self removeInputBorderFromView:_activeOrdersButton];
    [self removeInputBorderFromView:_previousOrdersButton];
}

-(void)setupNavigationBar{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Gobold" size:14]}];
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"PEDIDOS" attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
}

-(void)setupInputBorderToView:(UIView *)view{
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColorHelper practicalColorWithRed:44.0 green:45.0 blue:74.0 alpha:1.0] CGColor];
    upperBorder.frame = CGRectMake(0, 45.0, CGRectGetWidth(view.frame), 2.0f);
    [view.layer addSublayer:upperBorder];
}

-(void)removeInputBorderFromView:(UIView *)view{
    [((CALayer *)[view.layer.sublayers lastObject]) removeFromSuperlayer];
}

-(void)markButtonBorderView:(UIButton*)button{
    if (button.isEnabled) {
        ((CALayer *)[button.layer.sublayers lastObject]).backgroundColor = [[UIColorHelper practicalColorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0] CGColor];
    } else {
        ((CALayer *)[button.layer.sublayers lastObject]).backgroundColor = [[UIColorHelper practicalColorWithRed:44.0 green:45.0 blue:74.0 alpha:1.0] CGColor];
    }
}

-(void)initLists{
    previousOrders = [NSMutableArray array];
    activeOrders = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CustomOrderListCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"customOrderListCellId" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark UICollectionViewDelegate

- (void)configureCell:(CustomOrderListCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
//    BACategory *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSArray*orderList = [NSArray array];
    if (_showingPreviousOrders) {
        orderList = previousOrders;
        NSNumber* orderState = (NSNumber*)[[orderList objectAtIndex:indexPath.row] valueForKey:@"state_id"];
        if ([orderState isEqual:[NSNumber numberWithInt:rechazado]]) {
            [cell.extraInfoLabel setText:@"Rechazado"];
            [cell.extraInfoLabel setTextColor:[UIColorHelper practicalColorWithRed:234.0 green:67.0 blue:54.0 alpha:1.0]];
        }else{
            [cell.extraInfoLabel setText:[NSString stringWithFormat:@"S/. %.02f", ((NSNumber*)[[orderList objectAtIndex:indexPath.row] valueForKey:@"total"]).floatValue]];
            [cell.extraInfoLabel setTextColor:[UIColorHelper practicalColorWithRed:130.0 green:131.0 blue:148.0 alpha:1.0]];
        }
    } else {
        orderList = activeOrders;
        NSNumber* orderState = (NSNumber*)[[orderList objectAtIndex:indexPath.row] valueForKey:@"state_id"];
        switch (orderState.intValue) {
            case recibido:
            case pago_pendiente:
                [cell.extraInfoLabel setTextColor:[UIColorHelper practicalColorWithRed:130.0 green:131.0 blue:148.0 alpha:1.0]];
                [cell.extraInfoLabel setText:@"Procesando..."];
                break;
            case aceptado:
                [cell.extraInfoLabel setTextColor:[UIColorHelper practicalColorWithRed:68.0 green:182.0 blue:194.0 alpha:1.0]];
                [cell.extraInfoLabel setText:@"Aceptado"];
                break;
            case en_transito:
                [cell.extraInfoLabel setTextColor:[UIColorHelper practicalColorWithRed:68.0 green:182.0 blue:194.0 alpha:1.0]];
                [cell.extraInfoLabel setText:@"En tránsito"];
            default:
                break;
        }
    }
    
    [cell.addressLabel setText:[[orderList objectAtIndex:indexPath.row] valueForKey:@"delivery_address"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSz"];
    NSDate *date = [formatter dateFromString:[[orderList objectAtIndex:indexPath.row] valueForKey:@"created_at"]];
    [formatter setDateFormat:@"dd 'de' MMMM, HH:mm"];
    [cell.dateTimeLabel setText:[formatter stringFromDate:date]];
    
    // Update Cell
//    [cell.categoryNameLabel setText:record.name.capitalizedString];
//    [cell.categoryImageView setImage:([UIImage imageNamed: [NSString stringWithFormat:@"%@_collection_view_image",record.categoryImgCode]])];
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSArray*orderList = [NSArray array];
    if (_showingPreviousOrders) {
        orderList = previousOrders;
        [_emptyOrdersMessageLabel setText:@"No tienes pedidos registrados"];
    } else {
        orderList = activeOrders;
        [_emptyOrdersMessageLabel setText:@"No tienes pedidos activos"];
    }
    [collectionView setHidden:(orderList.count==0)];
    [_emptyOrdersView setHidden:!(orderList.count==0)];
    
    return orderList.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(CGRectGetWidth(collectionView.frame)-36, 157.0);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OrderInfoViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"orderInfoViewControllerId"];
    NSObject *orderObj = nil;
    if (_showingPreviousOrders) {
        orderObj = [previousOrders objectAtIndex:[collectionView.indexPathsForSelectedItems objectAtIndex:0].row];
    } else {
        orderObj = [activeOrders objectAtIndex:[collectionView.indexPathsForSelectedItems objectAtIndex:0].row];
    }
    [collectionView deselectItemAtIndexPath:indexPath animated:TRUE];
    vc.orderObj = orderObj;
    [[self navigationController] pushViewController:vc animated:YES];
    
}

-(void)requestMyOrders{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_user_orders:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                NSObject *jsonObj = (NSObject *) responseObject;
                NSArray *jsonArray = (NSArray*)[jsonObj valueForKey:@"orders"];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self initLists];
                    for (int i=0; i<jsonArray.count; i++) {
                        NSArray *previousStates = @[[NSNumber numberWithInt:finalizado], [NSNumber numberWithInt:rechazado]];
                        NSNumber* order_state = [[jsonArray objectAtIndex:i] valueForKey:@"state_id"];
                        if ([previousStates containsObject:order_state]) {
                            [previousOrders addObject:[jsonArray objectAtIndex:i]];
                        } else {
                            [activeOrders addObject:[jsonArray objectAtIndex:i]];
                        }
                    }
                    [_orderListCollectionView reloadData];
                    [refresher endRefreshing];
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController* destination = [segue destinationViewController];
    if (destination.class == OrderInfoViewController.class) {
        NSObject *orderObj = nil;
        if (_showingPreviousOrders) {
            orderObj = [previousOrders objectAtIndex:[_orderListCollectionView.indexPathsForSelectedItems objectAtIndex:0].row];
        } else {
            orderObj = [activeOrders objectAtIndex:[_orderListCollectionView.indexPathsForSelectedItems objectAtIndex:0].row];
        }
        ((OrderInfoViewController*)destination).orderObj = orderObj;
    }
}

- (IBAction)selectPreviousOrders:(id)sender {
    [self selectOrdersShowingPreviousOrders:true];
}

- (IBAction)selectActiveOrders:(id)sender {
    [self selectOrdersShowingPreviousOrders:false];
}

-(void)selectOrdersShowingPreviousOrders:(BOOL)showingPrevious{
    [_activeOrdersButton setEnabled:showingPrevious];
    [_previousOrdersButton setEnabled:!showingPrevious];
    [self markButtonBorderView:_activeOrdersButton];
    [self markButtonBorderView:_previousOrdersButton];
    _showingPreviousOrders = showingPrevious;
    [_orderListCollectionView reloadData];
}

- (IBAction)showSlider:(id)sender {
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:YES completion:^{
    }];
}
@end
