//
//  BAConfiguration.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "BAConfiguration.h"

@implementation BAConfiguration

// Insert code here to add functionality to your managed object subclass


+(NSString*)getActiveSchedulesText:(NSManagedObjectContext *)context{
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"BAConfiguration"];
    NSArray *configs = [context executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no combo found" format:@"%@", [error localizedDescription]];
    }
    
    NSString *scheduleText = @"";
    
    if (configs.count>0) {
        BAConfiguration* config = configs.lastObject;
        scheduleText = config.scheduleText;
    }else{
        scheduleText = @"¡Lo sentimos! En estos momentos no estamos atiendo. Espéranos pronto, saludos 😉";
    }
    
    return scheduleText;
}

+(void)deleteAllBAConfiguration:(NSManagedObjectContext *)context{
    NSFetchRequest *allConfiguration = [[NSFetchRequest alloc] init];
    [allConfiguration setEntity:[NSEntityDescription entityForName:@"BAConfiguration" inManagedObjectContext:context]];
    [allConfiguration setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *configurations = [context executeFetchRequest:allConfiguration error:&error];
    //error handling goes here
    for (NSManagedObject *config in configurations) {
        [context deleteObject:config];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

@end
