//
//  Request.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 4/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>

@interface Request : NSObject

@property (nonatomic, copy, nullable) AFHTTPSessionManager *manager;

-(id)initWithBaseURL:(NSString*) baseUrl;

- (nullable NSURLSessionDataTask *)get:(nullable NSString *)URLString
                            parameters:(nullable id)parameters
                               success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success
                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure;

- (nullable NSURLSessionDataTask *)post:(nullable NSString *)URLString
                            parameters:(nullable id)parameters
                               success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success
                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure;

-(nullable NSURLSessionDataTask *)delete:(NSString*)path
    parameters:(nullable id)parameters
    success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
    failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(nullable NSURLSessionDataTask *)put:(NSString*)path
parameters:(nullable id)parameters
   success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success
   failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure;

@end
