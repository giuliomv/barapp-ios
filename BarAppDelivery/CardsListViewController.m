//
//  CardsListViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 15/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "CardsListViewController.h"
#import "CardRegistrationViewController.h"
#import "CardTableViewCell.h"
#import "CardItem+CoreDataClass.h"
#import "RequestBuilder.h"
#import "AppDelegate.h"
#import "UIColorHelper.h"
#import "CustomLoaderView.h"
#import "Constants.h"
#import "OrderDetailsViewController.h"
#import "BAUser.h"

@interface CardsListViewController ()<NSFetchedResultsControllerDelegate>{
    CustomLoaderView *activityLoader;
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

@implementation CardsListViewController{
    BOOL deleteMode;
    NSString *preferred_payment_method;
}

//@synthesize cardItems = _cardItems;
@synthesize cardsTableView = _cardsTableView;
@synthesize cardBannerView = _cardBannerView;
@synthesize emptyBackgroundView = _emptyBackgroundView;
@synthesize chooseCardLabel = _chooseCardLabel;
@synthesize bannerTextLabel = _bannerTextLabel;
@synthesize slider_bar_button = _slider_bar_button;
@synthesize close_bar_button = _close_bar_button;
@synthesize delete_card_button = _delete_card_button;
@synthesize addNewCardButton = _addNewCardButton;

#pragma mark - UIViewController lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCardsTable];
    [self configurationBasedOnOrigin];
    [self setupBasedOnAuth];
    [self setupUI];
    deleteMode = FALSE;
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    [self initFetchController];
}

-(void)setupBasedOnAuth{
    if ([BAUser isUserLoggedIn]) {
        self.navigationItem.rightBarButtonItem = _delete_card_button;
    }else{
        [_addNewCardButton setHidden:TRUE];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [self setupNavigationBar];
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneDragRevealEnabled:true forDirection:MSDynamicsDrawerDirectionLeft];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController* destination = [segue destinationViewController];
    if (destination.class == CardRegistrationViewController.class) {
        if ([AppDelegate sharedAppDelegate].internetConnection) {
            if (deleteMode) {
                [self changeDeleteMode:nil];
            }
            ((CardRegistrationViewController*)destination).parentVC = self;
        }else{
            [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
        }
    }
}

#pragma mark - UI Setup

- (void)setupCardsTable {
    preferred_payment_method = [[NSUserDefaults standardUserDefaults] valueForKey:@"preferred_payment_method"];
    UIView * uv = [[UIView alloc] init];
    _cardsTableView.tableFooterView = uv;
    _emptyBackgroundView.hidden = true;
}

-(void)configurationBasedOnOrigin{

    if ([self.origin isEqualToString:card_list_origin_order_details]){
        self.navigationItem.leftBarButtonItem = _close_bar_button;
    }else{
        self.navigationItem.leftBarButtonItem = _slider_bar_button;
    }
}

-(void)setupNavigationBar{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Gobold" size:14]}];
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"MÉTODO DE PAGO" attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
}

-(void)setupUI{
    [_delete_card_button setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Aller" size:15]} forState:UIControlStateNormal];
    [_delete_card_button setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Aller" size:15]} forState:UIControlStateHighlighted];
}

#pragma mark - UITableViewDataSource and UITableViewDelegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CardTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cardTableViewCellId"];
    [self configureCell:cell atIndexPath:indexPath];
    cell.selectButton.tag = indexPath.section;
//    [cell.selectButton addTarget:self action:@selector(tappedSelected:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)configureCell:(CardTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath* iPath = nil;
    if (indexPath.section < 1) {
        iPath = indexPath;
        switch (indexPath.section) {
            case 0:
                [cell.lastFourLabel setText:@"POS VISA"];
                [cell.cardBrandImageView setImage:[UIImage imageNamed:@"icon_POS"]];
            default:
                break;
        }
        [cell.deleteButton setHidden:TRUE];
        [cell.selectButton setHidden:deleteMode];
        if (!deleteMode) {
            [cell.selectButton setSelected:[preferred_payment_method isEqualToString:@"pos"]];
        }
    } else {
        iPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section-1];
        CardItem *record = [self.fetchedResultsController objectAtIndexPath:iPath];
        [cell.lastFourLabel setText:[NSString stringWithFormat:@"**** %@",record.lastFour]];
        [cell.cardBrandImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"card_%@", [record.cardBrand lowercaseString]]]];
        [cell.deleteButton setHidden:!deleteMode];
        [cell.selectButton setHidden:deleteMode];
        if (!deleteMode) {
            [cell.selectButton setSelected:[[NSString stringWithFormat:@"%@",record.itemId] isEqualToString: preferred_payment_method]];
        }
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.borderColor = [UIColorHelper practicalColorWithRed:208.0 green:208.0 blue:213.0 alpha:1.0] .CGColor;
    cell.layer.borderWidth = 1.0;
    cell.layer.cornerRadius = 5;
    cell.clipsToBounds = true;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (deleteMode) {
        if (indexPath.section>=1) {
            [self showConfirmationToDeleteCard:indexPath];
        }
    } else {
        if (indexPath.section == 0) {
            if (![preferred_payment_method isEqualToString:@"pos"]) {
                preferred_payment_method = @"pos";
                [[NSUserDefaults standardUserDefaults] setValue: preferred_payment_method forKey:@"preferred_payment_method"];
                [tableView reloadData];
            }
        } else {
            CardItem* card = (CardItem*)[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section-1]];
            if (![[NSString stringWithFormat:@"%@",card.itemId] isEqual:preferred_payment_method] ) {
                preferred_payment_method = [NSString stringWithFormat:@"%@",card.itemId];
                [[NSUserDefaults standardUserDefaults] setValue: preferred_payment_method forKey:@"preferred_payment_method"];
                [tableView reloadData];
            }
        }
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 61.0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.fetchedResultsController.sections.count + 1;
}

-(void)removeCardRemotely:(NSIndexPath*)indexPath{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"card_id":((CardItem*)[self.fetchedResultsController objectAtIndexPath:indexPath]).itemId
                                                                                  }
                                   ];
    [r delete_user_card:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSObject *jsonObj = (NSObject *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [activityLoader stopAndRemoveLoader];
                    if ([[jsonObj valueForKey:@"success"] boolValue]) {
                        CardItem *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
                        [CardItem deleteCard:record withContext:_managedObjectContext];
                        [self cardWasDeleted];
                        if (self.fetchedResultsController.sections.count==0) {
                            [self changeDeleteMode:nil];
                            preferred_payment_method = @"pos";
                            [[NSUserDefaults standardUserDefaults] setValue: @"pos" forKey:@"preferred_payment_method"];
                            [_cardsTableView reloadData];
                        }
                    }
                });
            }
        });
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                [activityLoader stopAndRemoveLoader];
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

#pragma mark - UI Callbacks

-(void)cardWasCreated{
    [self bannerIsShowing:true];
    [self performSelector:@selector(bannerIsShowing:) withObject:false afterDelay:2.0];
}

-(void)cardWasDeleted{
    [_cardBannerView setBackgroundColor:[UIColorHelper practicalColorWithRed:211.0 green:47.0 blue:47.0 alpha:1.0]];
    [_bannerTextLabel setText:@"Tarjeta de crédito eliminada"];
    [self bannerIsShowing:true];
    [self performSelector:@selector(bannerIsShowing:) withObject:false afterDelay:2.0];
}

- (void)bannerIsShowing:(BOOL)showing{
    CATransition *animation = [CATransition animation];
    if (showing) {
        animation.type = kCATransitionMoveIn;
        animation.subtype = kCATransitionFromBottom;
    } else {
        animation.type = kCATransitionReveal;
        animation.subtype = kCATransitionFromTop;
    }
    animation.duration = 1;
    [_cardBannerView.layer addAnimation:animation forKey:nil];
    [_cardBannerView setHidden:!showing];
}

- (IBAction)showSlider:(id)sender {
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:YES completion:^{
    }];
}

- (IBAction)closeCardsView:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:true completion:^{
        [((OrderDetailsViewController*)self.parentVC) cardsListDidClosed];
    }];
}

-(void)showConfirmationToDeleteCard:(NSIndexPath *)indexPath{
    CardItem *card = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section-1]];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"¿Deseas eliminar esta tarjeta?"
                                 message:[NSString stringWithFormat:@"Estás a punto de eliminar la información de la tarjeta que termina en %@", card.lastFour]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self showMarkerLoader];
                                    [self removeCardRemotely:[NSIndexPath indexPathForRow:0 inSection:indexPath.section-1]];
                                }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancelar"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                   }];
    
    [alert addAction:yesButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
    
}

-(void) showMarkerLoader{
    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando"];
    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)changeDeleteMode:(id)sender {
    deleteMode = !deleteMode;
    [_delete_card_button setTitle: deleteMode ? @"Guardar" : @"Editar"];
    [_cardsTableView reloadData];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)initFetchController {
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CardItem"];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"itemId" ascending:YES]]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"itemId" cacheName:nil];
    [self.fetchedResultsController setDelegate:self];
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [_cardsTableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [_cardsTableView endUpdates];
}

-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [_cardsTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex + 1] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [_cardsTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex + 1] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [_cardsTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:newIndexPath.section+1]] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [_cardsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:indexPath.section+1]] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self configureCell:(CardTableViewCell *)[_cardsTableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [_cardsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:indexPath.section+1]] withRowAnimation:UITableViewRowAnimationFade];
            [_cardsTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:newIndexPath.section+1]] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

@end
