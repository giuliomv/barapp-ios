    //
//  CombosViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 10/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "CombosViewController.h"
#import "RequestBuilder.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Combo.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "SVProgressHUD.h"
#import "Constants.h"

@interface CombosViewController () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation CombosViewController

@synthesize combosScrollView = _combosScrollView;
@synthesize combosArray = _combosArray;
@synthesize companyId = _companyId;
@synthesize addButton = _addButton;

- (void)viewDidLoad {
    [super viewDidLoad];
//    _combosScrollView.contentInset = UIEdgeInsetsZero;
//    _combosScrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
//    _combosScrollView.contentOffset = CGPointMake(0.0, 0.0);
    _companyId = [AppDelegate sharedAppDelegate].closestCompany;
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    [self requestBanners];
}

- (void)save_banner:(NSEntityDescription *)comboEntity j:(int)j jsonArray:(NSArray *)jsonArray {
    NSNumber *comboId = [[jsonArray objectAtIndex:j] valueForKey:@"id"];
    NSString *bannerType = [[jsonArray objectAtIndex:j] valueForKey:@"banner_type"];
    NSError *error = nil;
    Combo *combo = [Combo getComboById:comboId bannerType:bannerType context:self.managedObjectContext];
    if (error) {
        [NSException raise:@"no combo find" format:@"%@", [error localizedDescription]];
    }
    
    Combo *record = nil;
    if (combo!=nil) {
        record = combo;
    }else {
        record = [[Combo alloc] initWithEntity:comboEntity insertIntoManagedObjectContext:self.managedObjectContext];
        record.qty = [NSNumber numberWithInt:(0)];
        record.addedToCart = [NSNumber numberWithInt:(0)];
    }
    
    record.name = [[jsonArray objectAtIndex:j] valueForKey:@"name"];
    record.comboId = [[jsonArray objectAtIndex:j] valueForKey:@"id"];
    record.orderKey = [[jsonArray objectAtIndex:j] valueForKey:@"order_key"];
    record.bannerType = [[jsonArray objectAtIndex:j] valueForKey:@"banner_type"];
    
    if ([record.bannerType isEqualToString:@"Combo"]) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        record.price = [f numberFromString:[[jsonArray objectAtIndex:j] valueForKey:@"price"]];
        id val = [[jsonArray objectAtIndex:j] valueForKey:@"description"];
        val = (val == [NSNull null] ? nil : val);
        
        record.deletedObj = [[jsonArray objectAtIndex:j] valueForKey:@"deleted"];
        record.flgStock = [[jsonArray objectAtIndex:j] valueForKey:@"flg_stock"];
        record.shoppingCartImgUrl = [[jsonArray objectAtIndex:j] valueForKey:@"img_shopping_cart_url"];
        record.imgUrl = [[jsonArray objectAtIndex:j] valueForKey:@"img_v2_url"];
    }else{
        record.deletedObj = [NSNumber numberWithInt:(0)];;
        record.flgStock = [NSNumber numberWithInt:(1)];;
        record.imgUrl = [[jsonArray objectAtIndex:j] valueForKey:@"img_url"];
    }
    if ([self.managedObjectContext save:&error]) {
        
    } else {
        if (error) {
            NSLog(@"Unable to save record.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
    }
}

- (void)requestBanners{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_banners_by_company:_companyId parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                // Create Entity
                
                NSDictionary *jsonObj = (NSDictionary *) responseObject;
                NSEntityDescription *comboEntity = [NSEntityDescription entityForName:@"Combo" inManagedObjectContext:self.managedObjectContext];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    NSArray *combosArray = (NSArray *) [jsonObj valueForKey:@"combo_banners"];
                    NSArray *adsArray = (NSArray *) [jsonObj valueForKey:@"ad_banners"];
                    
                    for (int j=0; j<combosArray.count; j++) {
                        [self save_banner:comboEntity j:j jsonArray:combosArray];
                    }
                    
                    for (int j=0; j<adsArray.count; j++) {
                        [self save_banner:comboEntity j:j jsonArray:adsArray];
                    }
                    
                    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                    [fetchRequest setEntity:comboEntity];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deletedObj = 0 AND flgStock = 1"];
                    [fetchRequest setPredicate:predicate];
                    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"orderKey"
                                                                                   ascending:YES];
                    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
                    
                    NSError *error = nil;
                    NSArray *fetchedObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
                    if (fetchedObjects == nil) {
                    }else{
                        _combosArray = fetchedObjects;
                        [self setupCombosScrollView];
                    }
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

//- (void)requestCombos{
//    RequestBuilder *r = [[RequestBuilder alloc] init];
//    [r get_combos_by_company:_companyId parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
//            @autoreleasepool {
//                // Create Entity
//                
//                NSArray *jsonArray = (NSArray *) responseObject;
//                NSEntityDescription *comboEntity = [NSEntityDescription entityForName:@"Combo" inManagedObjectContext:self.managedObjectContext];
//                dispatch_async(dispatch_get_main_queue(), ^(void){
////                    [Combo deleteAllCombos:self.managedObjectContext];
//                    for (int i=0; i<jsonArray.count; i++) {
//                        NSNumber *comboId = [[jsonArray objectAtIndex:i] valueForKey:@"id"];
//                        NSError *error = nil;
//                        Combo *combo = [Combo getComboById:comboId context:self.managedObjectContext];
//                        if (error) {
//                            [NSException raise:@"no category find" format:@"%@", [error localizedDescription]];
//                        }
//                        Combo *record = nil;
//                        if (combo!=nil) {
//                            record = combo;
//                        }else {
//                            record = [[Combo alloc] initWithEntity:comboEntity insertIntoManagedObjectContext:self.managedObjectContext];
//                            record.qty = [NSNumber numberWithInt:(0)];
//                            record.addedToCart = [NSNumber numberWithInt:(0)];
//                        }
//                        record.name = [[jsonArray objectAtIndex:i] valueForKey:@"name"];
//                        record.comboId = [[jsonArray objectAtIndex:i] valueForKey:@"id"];
//                        record.orderKey = [[jsonArray objectAtIndex:i] valueForKey:@"order_key"];
//                        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//                        f.numberStyle = NSNumberFormatterDecimalStyle;
//                        record.price = [f numberFromString:[[jsonArray objectAtIndex:i] valueForKey:@"price"]];
//                        id val = [[jsonArray objectAtIndex:i] valueForKey:@"description"];
//                        val = (val == [NSNull null] ? nil : val);
//                        record.imgUrl = [[jsonArray objectAtIndex:i] valueForKey:@"img_v2_url"];
//                        record.deletedObj = [[jsonArray objectAtIndex:i] valueForKey:@"deleted"];
//                        record.flgStock = [[jsonArray objectAtIndex:i] valueForKey:@"flg_stock"];
//                        record.shoppingCartImgUrl = [[jsonArray objectAtIndex:i] valueForKey:@"img_shopping_cart_url"];
//                        // Save Record
//                        if ([self.managedObjectContext save:&error]) {
//                            
//                        } else {
//                            if (error) {
//                                NSLog(@"Unable to save record.");
//                                NSLog(@"%@, %@", error, error.localizedDescription);
//                            }
//                        }
//                    }
//
//                    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//                    [fetchRequest setEntity:comboEntity];
//                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deletedObj = 0 AND flgStock = 1"];
//                    [fetchRequest setPredicate:predicate];
//                    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"orderKey"
//                                                                                   ascending:YES];
//                    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
//                    
//                    NSError *error = nil;
//                    NSArray *fetchedObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
//                    if (fetchedObjects == nil) {
//                    }else{
//                        _combosArray = fetchedObjects;
//                        [self setupCombosScrollView];
//                    }
//                });
//            }
//        });
//        
//    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
//            @autoreleasepool {
//                NSLog(@"FAIL: %@", responseObject);
//            }
//        });
//        
//    }];
//}

-(void)viewDidLayoutSubviews{
    
}

-(void)setupCombosScrollView{
    for (int i = 0; i< _combosArray.count; i++) {
        CGRect frame = CGRectMake(0,0,0,0);
        frame.origin.x = _combosScrollView.frame.size.width * i;
        frame.size = _combosScrollView.frame.size;
        UIImageView* comboImage = [[UIImageView alloc] initWithFrame:frame];
        [comboImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", server_base_url, ((Combo*)[_combosArray objectAtIndex:i ]).imgUrl]]];
        [_combosScrollView addSubview:comboImage];
    }
    _combosScrollPageControl.numberOfPages = _combosArray.count;
    _combosScrollPageControl.currentPage = 0;
    _combosScrollView.contentSize = CGSizeMake(_combosScrollView.frame.size.width * _combosArray.count, _combosScrollView.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = _combosScrollView.frame.size.width;
    float fractionalPage = _combosScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    _combosScrollPageControl.currentPage = page;
    if ([((Combo*)[_combosArray objectAtIndex:page]).bannerType isEqualToString:@"Ad"]) {
        [_addButton setHidden:true];
    }else{
        [_addButton setHidden:false];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}   
*/

- (IBAction)addComboAction:(id)sender {
    Combo *c = [_combosArray objectAtIndex:_combosScrollPageControl.currentPage];
    [[AppDelegate sharedShoppingCart] addComboToShoppingCart:c quantity: [NSNumber numberWithInt:c.qty.intValue+1]];
    [SVProgressHUD setMinimumDismissTimeInterval:2.0];
    [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"Se añadió %@ a su carrito de compras", c.name] maskType:SVProgressHUDMaskTypeBlack];
}

@end
