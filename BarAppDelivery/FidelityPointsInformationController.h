//
//  FidelityPointsInformationController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 14/03/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FidelityPointsInformationController : UIViewController

@property (strong, nonatomic) NSMutableArray *fidelityBenefitItems;
@property (strong, nonatomic) NSMutableArray *fidelityTermConditionItems;
@property (weak, nonatomic) IBOutlet UITableView *termsConditionsTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsConditionsHeightConstraint;
- (IBAction)openSliderDrawer:(id)sender;

@end
