//
//  MyLocationAnnotation.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 29/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyLocationAnnotation : NSObject <MKAnnotation>{
    CLLocationCoordinate2D coordinate;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
- (id)initWithLocation:(CLLocationCoordinate2D)coord;

// Other methods and properties.

@end
