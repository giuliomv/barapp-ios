//
//  CardItem+CoreDataClass.m
//  
//
//  Created by Giulio Mondoñedo on 28/05/18.
//
//

#import "CardItem+CoreDataClass.h"

@implementation CardItem

+(void)deleteAllCards:(NSManagedObjectContext *)context{
    NSFetchRequest *allCategories = [[NSFetchRequest alloc] init];
    [allCategories setEntity:[NSEntityDescription entityForName:@"CardItem" inManagedObjectContext:context]];
    [allCategories setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *cards = [context executeFetchRequest:allCategories error:&error];
    //error handling goes here
    for (NSManagedObject *card in cards) {
        [context deleteObject:card];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

+(void)deleteCard:(CardItem*)card withContext:(NSManagedObjectContext *)context{
    [context deleteObject:card];
    NSError *saveError = nil;
    [context save:&saveError];
}

+(CardItem*)getCardById:(NSString *)cardId
                  context:(NSManagedObjectContext *)context{
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"CardItem"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"itemId == %@", cardId];
    request.predicate=predicate;
    NSArray *cards = [context executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no product found" format:@"%@", [error localizedDescription]];
    }
    
    return cards.lastObject;
}

@end
