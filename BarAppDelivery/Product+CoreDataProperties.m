//
//  Product+CoreDataProperties.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product+CoreDataProperties.h"

@implementation Product (CoreDataProperties)

@dynamic productId;
@dynamic name;
@dynamic unitPrice;
@dynamic qty;
@dynamic productImgCode;
@dynamic brandId;
@dynamic categoryId;
@dynamic addedToCart;
@dynamic deletedObj;
@dynamic flgStock;
@dynamic imgUrl;
@dynamic orderKey;

@end
