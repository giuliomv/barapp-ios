//
//  TermsConditionModalViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 11/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsConditionModalViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *termsConditionsTextView;
@property (strong, nonatomic) UIViewController *parentVC;
- (IBAction)cancelAction:(id)sender;

@end
