//
//  Combo+CoreDataProperties.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Combo+CoreDataProperties.h"

@implementation Combo (CoreDataProperties)

@dynamic comboId;
@dynamic name;
@dynamic price;
@dynamic imgUrl;
@dynamic qty;
@dynamic addedToCart;
@dynamic deletedObj;
@dynamic flgStock;
@dynamic orderKey;
@dynamic shoppingCartImgUrl;
@dynamic bannerType;
@dynamic comboDescription;

@end
