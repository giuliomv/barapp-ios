//
//  FidelityBenefitCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 14/03/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FidelityBenefitCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *benefitIcon;
@property (weak, nonatomic) IBOutlet UILabel *benefitTitle;
@property (weak, nonatomic) IBOutlet UILabel *benefitDescription;

@end
