//
//  Category+CoreDataProperties.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BACategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface BACategory (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *categoryId;
@property (nullable, nonatomic, retain) NSString *categoryDescription;
@property (nullable, nonatomic, retain) NSString *categoryImgCode;
@property (nullable, nonatomic, retain) NSString *bannerImgUrl;
@property (nullable, nonatomic, assign) NSNumber *deletedObj;
@property (nullable, nonatomic, assign) NSNumber *orderKey;
@property (nullable, nonatomic, assign) NSNumber *isAlcoholic;

@end

NS_ASSUME_NONNULL_END
