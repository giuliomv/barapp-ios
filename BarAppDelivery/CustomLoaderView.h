//
//  CustomLoaderView.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 16/09/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLoaderView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *animatedImageView;
@property (weak, nonatomic) IBOutlet UILabel *loadingMessageLabel;

- (id)initWithTitle:(NSString*) title;
-(void)setupLoader:(UIView *)aSuperView;
-(void)stopAndRemoveLoader;


@end
