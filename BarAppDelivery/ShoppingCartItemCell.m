//
//  ShoppingCartItemCell.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 17/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "ShoppingCartItemCell.h"

@implementation ShoppingCartItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    for(UIView *subview in self.subviews){
        if([subview isKindOfClass:[UIScrollView class]]){
            UIScrollView *theScrollView = (UIScrollView *)subview;
            theScrollView.delegate = self;
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    static CGFloat kTargetOffset = 82.0f;
    
    if(scrollView.contentOffset.x >= kTargetOffset){
        scrollView.contentOffset = CGPointMake(kTargetOffset, 0.0f);
    }
}

@end
