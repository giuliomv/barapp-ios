//
//  PaymentMethodOrderDetailsCell.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 8/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "PaymentMethodOrderDetailsCell.h"

@implementation PaymentMethodOrderDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
