//
//  ContentViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 4/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *informatioTextView;
@property (strong, nonatomic) NSNumber *rowSelected;
- (IBAction)goBack:(id)sender;
- (IBAction)openSlider:(id)sender;

@end
