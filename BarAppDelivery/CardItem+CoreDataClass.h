//
//  CardItem+CoreDataClass.h
//  
//
//  Created by Giulio Mondoñedo on 28/05/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CardItem : NSManagedObject

+(void)deleteAllCards:(NSManagedObjectContext *)context;
+(void)deleteCard:(CardItem*)card withContext:(NSManagedObjectContext *)context;
+(CardItem*)getCardById:(NSString *)cardId
                context:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "CardItem+CoreDataProperties.h"
