//
//  InitialLoaderViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 13/11/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import "InitialLoaderViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>
#import <Google/Analytics.h>
#import "RequestBuilder.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ShoppingCart.h"
#import "BAConfiguration.h"
#import "ShoppingCartItem.h"
#import "BAUser.h"
#import "CategoriesViewController.h"
#import "SlideDrawerViewController.h"
#import "Schedule.h"
#import "CardItem+CoreDataClass.h"
#import "CustomAnimatedBackgroundView.h"
#import "UIColorHelper.h"
#import "Reachability.h"
#import "LocationPermissionRequestViewController.h"
#import "OrderListViewController.h"

@interface InitialLoaderViewController ()<CLLocationManagerDelegate>

@end

@implementation InitialLoaderViewController{
    int currentColorArrayIndex;
    BOOL locationAcquired;
    enum order_states {recibido = 1, en_transito, aceptado, rechazado, finalizado, pago_pendiente};
}


@synthesize locationManager = _locationManager;
@synthesize locationActivityIndicator = _locationActivityIndicator;
@synthesize selectedLocationAddress = _selectedLocationAddress;
@synthesize selectedLocationCoordinates = _selectedLocationCoordinates;
@synthesize colorsArray = _colorsArray;
@synthesize gradientLayer = _gradientLayer;

- (void)viewDidLoad {
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    locationAcquired = false;
    [super viewDidLoad];
    currentColorArrayIndex = 0;
    [self setupBackgroundColors];
    [self setupInitialPreferredPaymentMethod];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
}

-(void)setupInitialPreferredPaymentMethod{
    NSString *preferred_card_method;
    preferred_card_method = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferred_payment_method"];
    if (preferred_card_method == nil) {
        preferred_card_method = @"pos";
        [[NSUserDefaults standardUserDefaults] setValue: preferred_card_method forKey:@"preferred_payment_method"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [_locationActivityIndicator startAnimating];
    [self animateBackgroundView];
    [self startRequestingLocation];
}

-(void)reachabilityChanged:(NSNotification *)note{
    if ([AppDelegate sharedAppDelegate].internetConnection) {
        [self startRequestingLocation];
    }
}

-(void)startRequestingLocation{
    if ([AppDelegate sharedAppDelegate].internetConnection) {
        [self setupMapAndLocationServices];
        [self trackPageWithGA];
    }else{
        [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)animateBackgroundView{
    currentColorArrayIndex = (currentColorArrayIndex == _colorsArray.count - 1)? 0 : currentColorArrayIndex + 1;
    [UIView transitionWithView:self.view duration:2 options:UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionAllowAnimatedContent animations:^{
        _gradientLayer.colors = [NSArray arrayWithObjects:(id)[[[_colorsArray objectAtIndex:currentColorArrayIndex] objectForKey:@"first_color"] CGColor],(id)[[[_colorsArray objectAtIndex:currentColorArrayIndex] objectForKey:@"second_color"] CGColor], nil];
    } completion:^(BOOL success){
        [self animateBackgroundView];
    }];
}

-(void)setupBackgroundColors{
    _colorsArray = [NSMutableArray array];
    [_colorsArray addObject:@{
                              @"first_color" : [UIColorHelper practicalColorWithRed:124.0 green:62.0 blue:119.0 alpha:1.0],
                              @"second_color" : [UIColorHelper practicalColorWithRed:143 green:59.0 blue:106.0 alpha:1.0]
                              }];
    [_colorsArray addObject:@{
                              @"first_color" : [UIColorHelper practicalColorWithRed:143.0 green:59.0 blue:106.0 alpha:1.0],
                              @"second_color" : [UIColorHelper practicalColorWithRed:128.0 green:51.0 blue:89.0 alpha:1.0]
                              }];
    [_colorsArray addObject:@{
                            @"first_color" : [UIColorHelper practicalColorWithRed:128.0 green:51.0 blue:89.0 alpha:1.0],
                            @"second_color" : [UIColorHelper practicalColorWithRed:129.0 green:44.0 blue:72.0 alpha:1.0]
                            }];
    [_colorsArray addObject:@{
                            @"first_color" : [UIColorHelper practicalColorWithRed:129.0 green:44.0 blue:72.0 alpha:1.0],
                            @"second_color" : [UIColorHelper practicalColorWithRed:139.0 green:40.0 blue:59.0 alpha:1.0]
                            }];
    [_colorsArray addObject:@{
                            @"first_color" : [UIColorHelper practicalColorWithRed:139.0 green:40.0 blue:59.0 alpha:1.0],
                            @"second_color" : [UIColorHelper practicalColorWithRed:137.0 green:42.0 blue:64.0 alpha:1.0]
                            }];
    [_colorsArray addObject:@{
                            @"first_color" : [UIColorHelper practicalColorWithRed:137.0 green:42.0 blue:64.0 alpha:1.0],
                            @"second_color" : [UIColorHelper practicalColorWithRed:132.0 green:48.0 blue:81.0 alpha:1.0]
                            }];
    [_colorsArray addObject:@{
                            @"first_color" : [UIColorHelper practicalColorWithRed:132.0 green:48.0 blue:81.0 alpha:1.0],
                            @"second_color" : [UIColorHelper practicalColorWithRed:124.0 green:62.0 blue:119.0 alpha:1.0]
                            }];
    _gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.view.bounds;
    
    _gradientLayer.colors = [NSArray arrayWithObjects:(id)[[[_colorsArray objectAtIndex:0] objectForKey:@"first_color"] CGColor],(id)[[[_colorsArray objectAtIndex:0] objectForKey:@"second_color"] CGColor], nil];
    _gradientLayer.endPoint = CGPointMake(1, 0);
    [self.view.layer insertSublayer:_gradientLayer atIndex:0];
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Página inicial de obtención de ubicación"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)setupMapAndLocationServices {
    _locationManager = [[CLLocationManager alloc] init];
    
    [_locationManager setDelegate:self];
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
            [_locationManager startUpdatingLocation];
        }else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
            [self showLocationPermissionRequestView];
        }else{
            [self showNoLocationAlert];
        }
    }
    
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if (status==kCLAuthorizationStatusAuthorizedWhenInUse) {
        [manager startUpdatingLocation];
    }else if (status==kCLAuthorizationStatusDenied || status==kCLAuthorizationStatusRestricted){
        [self showNoLocationAlert];
    }
}


- (void)showLocationPermissionRequestView {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LocationPermissionRequestViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"locationPermissionRequestViewControllerId"];
    ((LocationPermissionRequestViewController*)ivc).parentVC = self;
    ivc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:ivc animated:true completion:nil];
}

-(void)locationPermissionRequestWasAccepted{
    [_locationManager requestWhenInUseAuthorization];
}

-(void)locationPermissionRequestWasDenied{
    [self showNoLocationAlert];
}

-(void)showNoLocationAlert{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Información"
                                 message:@"Habilite la ubicación seleccionando \"Cuando se use la aplicación\" en Configuración o vaya al mapa para seleccionar su ubicación manualmente"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* settingsAction = [UIAlertAction
                                actionWithTitle:@"Abrir Configuración"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                    [UIApplication.sharedApplication openURL:url];
                                }];
    
    UIAlertAction* mapAction = [UIAlertAction
                                   actionWithTitle:@"Ir al mapa"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self showMap:nil];
                                   }];
    
    [alert addAction:settingsAction];
    [alert addAction:mapAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations.lastObject;
    [_locationManager stopUpdatingLocation];
    if (CLLocationCoordinate2DIsValid(location.coordinate) && !locationAcquired) {
        locationAcquired = true;
        [self geocodeLocation:location.coordinate];
    }
}

- (void)geocodeLocation:(CLLocationCoordinate2D)coordinates{
    CLLocation *locObj = [[CLLocation alloc] initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:locObj completionHandler:^(NSArray *placemarks, NSError *error) {
        if(error){
            NSLog(@"error: %@", error.localizedDescription);
        }
        if(placemarks && placemarks.count){
            CLPlacemark *placemark2 = placemarks[0];
            NSDictionary *addressDictionary = placemark2.addressDictionary;
            NSString *street = [addressDictionary objectForKey:(NSString*)kABPersonAddressStreetKey];
            NSString *state = [addressDictionary objectForKey:(NSString*)kABPersonAddressStateKey];
            _selectedLocationCoordinates = coordinates;
            _selectedLocationAddress = [NSString stringWithFormat:@"%@ %@",street,state];
            [self trackSelectionLocationEvent];
            [self requestClosestLocation];
        }
    }];
}

-(void)trackSelectionLocationEvent{
    if (CLLocationCoordinate2DIsValid(_selectedLocationCoordinates)) {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Click"
                                                              action:[NSString stringWithFormat: @"%f,%f",_selectedLocationCoordinates.latitude, _selectedLocationCoordinates.longitude]
                                                               label:@"Ubicación seleccionada"
                                                               value:nil] build]];
    }
}

-(void)requestClosestLocation{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_closest_company_with_latitude: _selectedLocationCoordinates.latitude longitude:_selectedLocationCoordinates.longitude success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                // Create Entity
                NSObject *jsonObject = (NSObject *) responseObject;;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    NSNumber *companyId = [jsonObject valueForKey:@"id"];
                    BOOL isValidCompany = [[jsonObject valueForKey:@"is_valid_company"] boolValue];
                    NSArray *deliveryTiers = [jsonObject valueForKey: @"delivery_tiers"];
                    NSNumber *highestDeliveryFee = [jsonObject valueForKey:@"highest_delivery_fee"];
                    NSNumber *deliverySelected = [jsonObject valueForKey:@"delivery_fee"];
                    [self storeClosestCompany:companyId];
                    [self storeDeliveryFee:deliverySelected];
                    [self storeMinimumAmount:[jsonObject valueForKey:@"minimum_amount"]];
                    [self storeDeliveryLocation];
                    [self storeReachableCompany:isValidCompany];
                    [self defineDeliveryMessageWithTiers:deliveryTiers highestDeliveryFee:highestDeliveryFee selectedDelivery:deliverySelected];
                    [self resetInventory];
                    if ([BAUser isUserLoggedIn]) {
                        [self requestUserInfo];
                    }else{
                        [self goToCategories];
                    }
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

-(void)goToCategories{
    [_locationActivityIndicator stopAnimating];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController* vc = nil;
    SlideDrawerViewController *menuViewController = (SlideDrawerViewController *)[storyboard instantiateViewControllerWithIdentifier:@"slideDrawerViewControllerId"];
    NSDictionary *retrievedDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"new_order_update"];
    if (retrievedDictionary==nil) {
        vc = (CategoriesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"categoriesViewControllerId"];
        menuViewController.paneViewControllerType = @"CATEGORIES";
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"new_order_update"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        vc = (OrderListViewController *)[storyboard instantiateViewControllerWithIdentifier:@"orderListViewControllerId"];
        if ([(NSNumber*)retrievedDictionary[@"state_id"] isEqual: [NSNumber numberWithInt:rechazado]]) {
            ((OrderListViewController *)vc).showingPreviousOrders = true;
        }else{
            ((OrderListViewController *)vc).showingPreviousOrders = false;
        }
        menuViewController.paneViewControllerType = @"ORDERS";
    }
    
    MSDynamicsDrawerViewController *dynamicsViewController = (MSDynamicsDrawerViewController *)[storyboard instantiateViewControllerWithIdentifier:@"msDynamicsDrawerViewControllerId"];
    [AppDelegate sharedAppDelegate].dynamicsDrawerViewController = dynamicsViewController;
    [AppDelegate sharedAppDelegate].menuViewController = menuViewController;
    menuViewController.dynamicsDrawerViewController = dynamicsViewController;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    dynamicsViewController.paneViewController = navController;
    [dynamicsViewController addStylersFromArray:@[[MSDynamicsDrawerResizeStyler styler],[MSDynamicsDrawerParallaxStyler styler]] forDirection:MSDynamicsDrawerDirectionLeft];
    [dynamicsViewController setDrawerViewController:menuViewController forDirection:MSDynamicsDrawerDirectionLeft];
    [UIApplication sharedApplication].keyWindow.rootViewController = dynamicsViewController;
}

-(void)requestUserInfo{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_user_fidelity_points:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                // Create Entity
                NSObject *jsonObject = (NSObject *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if ([[jsonObject valueForKey:@"success"] boolValue]) {
                        NSError *error = nil;
                        NSNumber *fidelityPoints = [jsonObject valueForKey:@"fidelity_points"];
                        NSString *avatarUrl = [jsonObject valueForKey:@"avatar_url"];
                        NSString *userEmail = [jsonObject valueForKey:@"user_email"];
                        BAUser* user = [BAUser getLoggedInUser];
                        user.fidelityPoints = fidelityPoints;
                        user.avatarUrl = avatarUrl;
                        user.userEmail = userEmail;
                        if ([self.managedObjectContext save:&error]) {
                            [self validateSentDeviceToken];
                            [self requestUserCards];
                        } else {
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                });
            }
        });
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
    }];
}

-(void)requestUserCards{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_user_cards:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    NSError *error = nil;
                    NSObject *jsonObj = (NSObject *) responseObject;
                    NSArray *jsonArray = (NSArray*)[jsonObj valueForKey:@"cards"];
                    for (int i=0; i<jsonArray.count; i++) {
                        CardItem *record = [[CardItem alloc] initWithEntity:[NSEntityDescription entityForName:@"CardItem" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
                        record.itemId = [[jsonArray objectAtIndex:i] valueForKey:@"id"];
                        record.lastFour = [[jsonArray objectAtIndex:i] valueForKey:@"last_four"];
                        record.cardBrand = [[jsonArray objectAtIndex:i] valueForKey:@"card_brand"];;
                        
                        if ([self.managedObjectContext save:&error]) {
                        }else{
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                    [self goToCategories];
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

-(void)validateSentDeviceToken{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"device_token_sent"]) {
        [[AppDelegate sharedAppDelegate] registerForPushNotifications:[UIApplication sharedApplication]];
    }
}

-(void)resetInventory{
    NSManagedObjectContext *managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    [self clearShoppingCart];
    [Combo deleteAllCombos:managedObjectContext];
    [Product deleteAllProducts:managedObjectContext];
    [BAConfiguration deleteAllBAConfiguration:managedObjectContext];
    [Schedule deleteAllSchedules:managedObjectContext];
    [CardItem deleteAllCards:self.managedObjectContext];
}

-(void)clearShoppingCart{
    NSMutableArray * shoppingCartItems = [NSMutableArray array];
    [shoppingCartItems addObjectsFromArray:([[AppDelegate sharedShoppingCart] shoppingCartCombosItems])];
    [shoppingCartItems addObjectsFromArray:([[AppDelegate sharedShoppingCart] shoppingCartProductsItems])];
    for (int i = 0; i < shoppingCartItems.count ; i++) {
        ShoppingCartItem *record = [shoppingCartItems objectAtIndex:i];
        if (record.isCombo) {
            [[AppDelegate sharedShoppingCart] removeComboFromShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
        }else{
            [[AppDelegate sharedShoppingCart] removeProductFromShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
        }
    }
    [shoppingCartItems removeAllObjects];
}

-(void)defineDeliveryMessageWithTiers:(NSArray*)tiers highestDeliveryFee:(NSNumber*)highest selectedDelivery:(NSNumber*)selected{
    NSArray* totalTiers = [tiers arrayByAddingObject:@{@"delivery_value":highest}];
    NSNumber *lowerValue = [((NSObject *)totalTiers[(totalTiers.count / 3) - 1]) valueForKey:@"delivery_value"];
    NSNumber *upperValue = [((NSObject *)totalTiers[(totalTiers.count * 2 / 3)]) valueForKey:@"delivery_value"];
    if ([selected doubleValue] <= [lowerValue doubleValue]) {
        [self storeDeliveryMessageOption:lowest_delivery_value];
    }else if (selected >= upperValue){
        [self storeDeliveryMessageOption:highest_delivery_value];
    }else{
        [self storeDeliveryMessageOption:average_delivery_value];
    }
}

-(void) storeDeliveryFee:(NSNumber*)delivery_fee{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:delivery_fee forKey:@"deliveryFee"];
    [defaults synchronize];
    [AppDelegate sharedAppDelegate].deliveryFee = delivery_fee;
}

-(void) storeMinimumAmount:(NSNumber*)minimum_amount{
    [AppDelegate sharedAppDelegate].minimumAmount = minimum_amount;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:minimum_amount forKey:@"minimumAmount"];
    [defaults synchronize];
}

-(void) storeReachableCompany:(BOOL)reachable_company{
    [AppDelegate sharedAppDelegate].reachableCompany = reachable_company;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:reachable_company forKey:@"reachableCompany"];
    [defaults synchronize];
}

-(void) storeDeliveryMessageOption:(NSString*)option{
    [AppDelegate sharedAppDelegate].deliveryMessageOption = option;
}

-(void) storeClosestCompany:(NSNumber*)companyId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:companyId forKey:@"closestCompany"];
    [defaults synchronize];
    [AppDelegate sharedAppDelegate].closestCompany = companyId;
}

- (void) storeDeliveryLocation{
    [AppDelegate sharedAppDelegate].selectedLocationAddress = _selectedLocationAddress;
    [AppDelegate sharedAppDelegate].selectedLocationCoordinates =  _selectedLocationCoordinates;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_selectedLocationAddress forKey:@"selectedLocationAddress"];
    [defaults setDouble:_selectedLocationCoordinates.latitude forKey:@"selectedLocationCoordinatesLatitude"];
    [defaults setDouble:_selectedLocationCoordinates.longitude forKey:@"selectedLocationCoordinatesLongitude"];
    [defaults synchronize];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)showMap:(id)sender {
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"selectLocationViewControllerId"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ivc];
    
    [UIView transitionWithView:[UIApplication sharedApplication].keyWindow
                      duration:0.5
                       options:UIViewAnimationOptionCurveEaseOut
                    animations:^{ [UIApplication sharedApplication].keyWindow.rootViewController = navigationController; }
                    completion:nil];
}
@end
