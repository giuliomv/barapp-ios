//
//  Request.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 4/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import "Request.h"
#import <AFNetworking/AFHTTPSessionManager.h>

@implementation Request
@synthesize manager = _manager;

-(id)initWithBaseURL:(NSString*) baseUrl{
    self = [super init];
    if (self) {
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
    }
    return self;
}

-(NSURLSessionDataTask *)get:(NSString*)path
parameters:(NSString*)parameters
   success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
   failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure
{
    return [self.manager GET:path
           parameters:parameters
             progress:nil
              success:success
              failure:failure];
}

-(NSURLSessionDataTask *)post:(NSString*)path
parameters:(NSDictionary*)parameters
   success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
   failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure
{
   return [self.manager POST:path
           parameters:parameters
             progress:nil
              success:success
              failure:failure];
}

-(NSURLSessionDataTask *)put:(NSString*)path
parameters:(NSDictionary*)parameters
   success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success
   failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure
{
    return [self.manager PUT:path
           parameters:parameters
              success:success
              failure:failure];
}

-(NSURLSessionDataTask *)delete:(NSString*)path
   parameters:(NSDictionary*)parameters
      success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
      failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure
{
    return [self.manager DELETE:path
           parameters:parameters
              success:success
              failure:failure];
}

@end
