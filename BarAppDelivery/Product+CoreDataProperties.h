//
//  Product+CoreDataProperties.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product.h"

NS_ASSUME_NONNULL_BEGIN

@interface Product (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *productId;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *unitPrice;
@property (nullable, nonatomic, retain) NSNumber *qty;
@property (nullable, nonatomic, retain) NSString *productImgCode;
@property (nullable, nonatomic, retain) NSNumber *brandId;
@property (nullable, nonatomic, retain) NSNumber *categoryId;
@property (nullable, nonatomic, assign) NSNumber *addedToCart;
@property (nullable, nonatomic, assign) NSNumber *deletedObj;
@property (nullable, nonatomic, assign) NSNumber *flgStock;
@property (nullable, nonatomic, retain) NSString *imgUrl;
@property (nullable, nonatomic, retain) NSString *productDescription;
@property (nullable, nonatomic, retain) NSNumber *orderKey;

@end

NS_ASSUME_NONNULL_END
