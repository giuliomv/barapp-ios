//
//  CustomCategoryCell.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import "CustomCategoryCell.h"

@implementation CustomCategoryCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.layer.masksToBounds = NO;
    self.layer.shadowOpacity = 0.7f;
    self.layer.shadowRadius = 5.0f;
    self.layer.shadowOffset = CGSizeMake(1.0, 3.0);
    self.layer.shadowColor = [UIColor blackColor].CGColor;
}

@end
