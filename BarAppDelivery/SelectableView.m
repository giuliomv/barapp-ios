//
//  SelectableView.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 8/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "SelectableView.h"
@interface SelectableView()
@end

@implementation SelectableView

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.backgroundColor = [UIColor lightGrayColor];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    self.backgroundColor = [UIColor clearColor];
}

@end
