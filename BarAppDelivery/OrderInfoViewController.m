//
//  OrderInfoViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 1/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "OrderInfoViewController.h"
#import "OrderLocationAnnotation.h"
#import "CustomOrderInfoProductsTableCell.h"
#import <MapKit/MapKit.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Constants.h"
#import "UIColorHelper.h"
#import "CardItem+CoreDataClass.h"
#import "AppDelegate.h"

@interface OrderInfoViewController ()

@end

@implementation OrderInfoViewController{
    enum order_states {recibido = 1, en_transito, aceptado, rechazado, finalizado, pago_pendiente};
}

@synthesize orderObj = _orderObj;
@synthesize orderLocationMap = _orderLocationMap;
@synthesize orderAddressLabel = _orderAddressLabel;
@synthesize orderDateTimeLabel = _orderDateTimeLabel;
@synthesize orderIdLabel = _orderIdLabel;
@synthesize orderStateLabel = _orderStateLabel;
@synthesize orderDetailsTable = _orderDetailsTable;
@synthesize deliveryCostLabel = _deliveryCostLabel;
@synthesize totalAmountLabel = _totalAmountLabel;
@synthesize pointsWonLabel = _pointsWonLabel;
@synthesize orderLat = _orderLat;
@synthesize orderLng = _orderLng;
@synthesize previousAppliedCardImage = _previousAppliedCardImage;
@synthesize previousAppliedLastFourCardLabel = _previousAppliedLastFourCardLabel;
@synthesize previousAppliedCardContainerView = _previousAppliedCardContainerView;
@synthesize activeAppliedCardImage = _activeAppliedCardImage;
@synthesize previousCashLabel = _previousCashLabel;
@synthesize amountPayedLabel = _amountPayedLabel;
@synthesize activePaysWithLabel = _activePaysWithLabel;
@synthesize tableViewTopConstraint = _tableViewTopConstraint;
@synthesize ordersTableViewHeight = _ordersTableViewHeight;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    _orderDetailsTable.rowHeight = UITableViewAutomaticDimension;
    _orderDetailsTable.estimatedRowHeight = 100.0;
    [self setupOrderInfo];
}

-(void)viewWillAppear:(BOOL)animated{
    [self setupNavigationBar];
//    [_orderDetailsTable reloadData];
//    [_orderDetailsTable layoutIfNeeded];
//    _ordersTableViewHeight.constant = _orderDetailsTable.contentSize.height;
}

-(void)viewWillLayoutSubviews{
    [super updateViewConstraints];
    _ordersTableViewHeight.constant = _orderDetailsTable.contentSize.height;
}

-(void)setupNavigationBar{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Gobold" size:14]}];
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"PEDIDOS" attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
}


-(void)setupOrderInfo{
    _orderAddressLabel.text = [_orderObj valueForKey:@"delivery_address"];
    _orderIdLabel.text = [NSString stringWithFormat:@"ID de orden: %010d", ((NSNumber*)[_orderObj valueForKey:@"id"]).intValue];
    _deliveryCostLabel.text = [NSString stringWithFormat:@"S/. %.02f", ((NSNumber*)[_orderObj valueForKey:@"order_delivery_fee"]).floatValue];
    _totalAmountLabel.text = [NSString stringWithFormat:@"S/. %.02f", ((NSNumber*)[_orderObj valueForKey:@"total"]).floatValue];
    _pointsWonLabel.text = [NSString stringWithFormat:@"%@ puntos", ((NSNumber*)[_orderObj valueForKey:@"fidelity_points"])];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSz"];
    NSDate *date = [formatter dateFromString:[_orderObj valueForKey:@"created_at"]];
    [formatter setDateFormat:@"dd 'de' MMMM, HH:mm"];
    _orderDateTimeLabel.text = [formatter stringFromDate:date];
    
    _orderLat = (NSNumber*)[_orderObj valueForKey:@"latitude"];
    _orderLng = (NSNumber*)[_orderObj valueForKey:@"longitude"];
    [self setupOrderLocation];
    [self setupPaymentInfo];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [self viewWillLayoutSubviews];
}

-(void)setupOrderLocation{
    CLLocationCoordinate2D orderLocation;
    orderLocation.latitude = _orderLat.floatValue;
    orderLocation.longitude = _orderLng.floatValue;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(orderLocation, 500, 500);
    [_orderLocationMap setRegion:viewRegion];
    OrderLocationAnnotation *ann = [[OrderLocationAnnotation alloc] init];
    ann.coordinate = CLLocationCoordinate2DMake(_orderLat.floatValue, _orderLng.floatValue);
    [_orderLocationMap addAnnotation:ann];
    
}

-(void)activeSetup{
    [_orderStateLabel setHidden:FALSE];
    [_previousAppliedCardContainerView setHidden:TRUE];
    [_previousCashLabel setHidden:TRUE];
    if ([[_orderObj valueForKey:@"payment_method"] isEqualToString:@"CASH"]) {
        [_activeAppliedCardImage setHidden:TRUE];
        [_amountPayedLabel setText:[NSString stringWithFormat:@"S/. %.02f", ((NSNumber*)[_orderObj valueForKey:@"cash_amount"]).floatValue]];
    }else{
        [_amountPayedLabel setHidden:TRUE];
        if ([[_orderObj valueForKey:@"payment_method"] isEqualToString:@"VISA"]) {
            [_activeAppliedCardImage setImage:[UIImage imageNamed:@"icon_POS"]];
        } else {
            NSString* card_selected = (NSString*)[_orderObj valueForKey:@"card_id_selected"];
            CardItem *card = [CardItem getCardById:card_selected context:self.managedObjectContext];
            [_activeAppliedCardImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"card_%@", [card.cardBrand lowercaseString]]]];
        }
    }
}

-(void)previousSetup{
    [_activePaysWithLabel setHidden:TRUE];
    [_activeAppliedCardImage setHidden:TRUE];
    if ([[_orderObj valueForKey:@"payment_method"] isEqualToString:@"CASH"]) {
        [_amountPayedLabel setText:[NSString stringWithFormat:@"S/. %.02f", ((NSNumber*)[_orderObj valueForKey:@"cash_amount"]).floatValue]];
        [_previousAppliedCardContainerView setHidden:TRUE];
    } else {
        [_amountPayedLabel setText:[NSString stringWithFormat:@"S/. %.02f", ((NSNumber*)[_orderObj valueForKey:@"total"]).floatValue]];
        [_previousCashLabel setHidden:TRUE];
        if ([[_orderObj valueForKey:@"payment_method"] isEqualToString:@"VISA"]) {
            [_previousAppliedCardImage setImage:[UIImage imageNamed:@"icon_POS"]];
            [_previousAppliedLastFourCardLabel setText:@"POS VISA"];
        }else{
            NSString* card_selected = (NSString*)[_orderObj valueForKey:@"card_id_selected"];
            CardItem *card = [CardItem getCardById:card_selected context:self.managedObjectContext];
            [_previousAppliedCardImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"card_%@", [card.cardBrand lowercaseString]]]];
            [_previousAppliedLastFourCardLabel setText:[NSString stringWithFormat:@"*%@",card.lastFour]];
        }
    }
}

-(void)setupPaymentInfo{
    int order_state = ((NSNumber*)[_orderObj valueForKey:@"state_id"]).intValue;
    _tableViewTopConstraint.constant = 50.0;
    switch (order_state) {
        case pago_pendiente:
        case recibido:
            [self activeSetup];
            [_orderStateLabel setText:@"Procesando..."];
            [_orderStateLabel setTextColor:[UIColorHelper practicalColorWithRed:130.0 green:131.0 blue:148.0 alpha:1.0]];
            break;
        case en_transito:
            [self activeSetup];
            [_orderStateLabel setText:@"En Tránsito"];
            [_orderStateLabel setTextColor:[UIColorHelper practicalColorWithRed:68.0 green:182.0 blue:194.0 alpha:1.0]];
            break;
        case aceptado:
            [self activeSetup];
            [_orderStateLabel setText:@"Aceptado"];
            [_orderStateLabel setTextColor:[UIColorHelper practicalColorWithRed:68.0 green:182.0 blue:194.0 alpha:1.0]];
            break;
        case rechazado:
            [_orderStateLabel setTextColor:[UIColorHelper practicalColorWithRed:234.0 green:67.0 blue:54.0 alpha:1.0]];
            [_orderStateLabel setText:@"Rechazado"];
            [self previousSetup];
            [_amountPayedLabel setText:@"S/. 0.00"];
            [_pointsWonLabel setText:@"0 puntos"];
            break;
        case finalizado:
            [_orderStateLabel removeFromSuperview];
            _tableViewTopConstraint.constant = 25.0;
            [self previousSetup];
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    MKAnnotationView *annView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"orderLocationPin"];
    if (annView==nil) {
        annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"orderLocationPin"];
        annView.canShowCallout = TRUE;
        annView.image = [UIImage imageNamed:@"tag_botella"];
    }
    
    return annView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CustomOrderInfoProductsTableCell *cell=[tableView dequeueReusableCellWithIdentifier:@"customOrderInfoProductsTableCellId"];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(CustomOrderInfoProductsTableCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSUInteger productsCount = ((NSArray*)[_orderObj valueForKey:@"order_details"]).count;
    if (productsCount > indexPath.row) {
        NSObject *item = [[((NSArray*)[_orderObj valueForKey:@"order_details"]) objectAtIndex:indexPath.row] valueForKey:@"product"];
        [cell.itemNameLabel setText:[item valueForKey:@"name"]];
        [cell.itemDescriptionLabel setText:[item valueForKey:@"description"]];
        [cell.itemPriceLabel setText:[NSString stringWithFormat:@"S/. %.02f", ((NSNumber*)[item valueForKey:@"unitPrice"]).floatValue]];
        [cell.itemQty setText:[NSString stringWithFormat:@"x%@", ((NSNumber*)[[((NSArray*)[_orderObj valueForKey:@"order_details"]) objectAtIndex:indexPath.row] valueForKey:@"qty"])]];
        [cell.itemImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", server_base_url, [item valueForKey:@"product_img_v2_url"]]] placeholderImage:[UIImage imageNamed:@"ba_product_placeholder"]];
    } else {
        NSObject *item = [[((NSArray*)[_orderObj valueForKey:@"combo_orders"]) objectAtIndex:indexPath.row - productsCount] valueForKey:@"combo"];
        [cell.itemNameLabel setText:[item valueForKey:@"name"]];
        [cell.itemDescriptionLabel setText:[item valueForKey:@"description"]];
        [cell.itemPriceLabel setText:[NSString stringWithFormat:@"S/. %.02f", ((NSNumber*)[item valueForKey:@"price"]).floatValue]];
        [cell.itemQty setText:[NSString stringWithFormat:@"x%@", ((NSNumber*)[[((NSArray*)[_orderObj valueForKey:@"combo_orders"]) objectAtIndex:indexPath.row - productsCount] valueForKey:@"qty"])]];
        [cell.itemImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", server_base_url, [item valueForKey:@"img_shopping_cart_url"]]] placeholderImage:[UIImage imageNamed:@"ba_product_placeholder"]];
    }
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSArray*)[_orderObj valueForKey:@"order_details"]).count + ((NSArray*)[_orderObj valueForKey:@"combo_orders"]).count;
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
@end
