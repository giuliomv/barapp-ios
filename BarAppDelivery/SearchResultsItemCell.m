//
//  SearchResultsItemCell.m
//  BarAppDelivery
//
//  Created by Home on 6/8/20.
//  Copyright © 2020 Giulio Mondoñedo. All rights reserved.
//

#import "SearchResultsItemCell.h"

@implementation SearchResultsItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
