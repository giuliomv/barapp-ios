//
//  Category+CoreDataProperties.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Category+CoreDataProperties.h"

@implementation BACategory (CoreDataProperties)

@dynamic name;
@dynamic categoryId;
@dynamic categoryDescription;
@dynamic categoryImgCode;
@dynamic deletedObj;
@dynamic orderKey;
@dynamic bannerImgUrl;
@dynamic isAlcoholic;

@end
