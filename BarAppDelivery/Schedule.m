//
//  Schedule.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "Schedule.h"

@implementation Schedule

// Insert code here to add functionality to your managed object subclass

+(NSMutableDictionary*)getFormattedSchedules:(NSManagedObjectContext *)context{
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Schedule"];
    NSArray *schedules = [context executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no combo found" format:@"%@", [error localizedDescription]];
    }
    
    NSMutableDictionary *schedulesDict = [NSMutableDictionary dictionary];
    for (int i = 0; i < schedules.count; i++) {
        Schedule *sch = [schedules objectAtIndex:i];
        
        NSMutableArray *tempArrayList = [NSMutableArray array];
        [tempArrayList addObject: [NSNumber numberWithInteger:sch.initialHour.integerValue]];
        [tempArrayList addObject: [NSNumber numberWithInteger:sch.initialMnutes.integerValue]];
        [tempArrayList addObject: [NSNumber numberWithInteger:sch.endHour.integerValue]];
        [tempArrayList addObject: [NSNumber numberWithInteger:sch.endMinutes.integerValue]];
        
        if ([schedulesDict objectForKey:sch.dayOfWeek] == nil) {
            [schedulesDict setObject:tempArrayList forKey:sch.dayOfWeek];
        }else{
            [[schedulesDict objectForKey:sch.dayOfWeek] addObjectsFromArray:tempArrayList];
        }
    }
    
    return schedulesDict;
}

+(void)deleteAllSchedules:(NSManagedObjectContext *)context{
    NSFetchRequest *allSchedule = [[NSFetchRequest alloc] init];
    [allSchedule setEntity:[NSEntityDescription entityForName:@"Schedule" inManagedObjectContext:context]];
    [allSchedule setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *schedules = [context executeFetchRequest:allSchedule error:&error];
    //error handling goes here
    for (NSManagedObject *schedule in schedules) {
        [context deleteObject:schedule];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

@end
