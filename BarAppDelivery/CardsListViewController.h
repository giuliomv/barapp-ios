//
//  CardsListViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 15/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardsListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *emptyBackgroundView;
@property (weak, nonatomic) IBOutlet UITableView *cardsTableView;
//@property (strong, nonatomic) NSMutableArray *cardItems;
@property (weak, nonatomic) IBOutlet UILabel *chooseCardLabel;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UIView *cardBannerView;
@property (weak, nonatomic) IBOutlet UILabel *bannerTextLabel;
@property (strong, nonatomic) NSString *origin;
@property (strong, nonatomic) UIViewController *parentVC;
- (IBAction)changeDeleteMode:(id)sender;
- (void)cardWasCreated;
- (IBAction)showSlider:(id)sender;
- (IBAction)closeCardsView:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *slider_bar_button;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *close_bar_button;
@property (weak, nonatomic) IBOutlet UIButton *addNewCardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *delete_card_button;

@end
