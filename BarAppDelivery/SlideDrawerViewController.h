//
//  SlideDrawerViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 9/09/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSDynamicsDrawerViewController.h"

@interface SlideDrawerViewController : UIViewController

@property (nonatomic, weak) MSDynamicsDrawerViewController *dynamicsDrawerViewController;
@property (strong, nonatomic) NSString *paneViewControllerType;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userCurrentLocationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userAvatarImageView;
- (IBAction)goToAuthDashboard:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIView *userInfoContainerView;
@property (weak, nonatomic) IBOutlet UIView *pointsLabelContainer;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UIView *fidelityPointsContainerView;
- (IBAction)showFidelityPointsInfoSection:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *menuOptionsTableView;

@end
