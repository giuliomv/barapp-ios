//
//  PurchaseOrder+CoreDataProperties.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PurchaseOrder.h"

NS_ASSUME_NONNULL_BEGIN

@interface PurchaseOrder (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *purchaseOrderId;
@property (nullable, nonatomic, retain) NSString *contactName;
@property (nullable, nonatomic, retain) NSString *deliveryAddress;
@property (nullable, nonatomic, retain) NSString *addressReference;
@property (nullable, nonatomic, retain) NSString *contactPhone;
@property (nullable, nonatomic, retain) NSNumber *total;
@property (nullable, nonatomic, retain) NSString *paymentMethod;
@property (nullable, nonatomic, retain) NSString *purchaseDate;
@property (nullable, nonatomic, retain) NSNumber *cashAmount;

@end

NS_ASSUME_NONNULL_END
