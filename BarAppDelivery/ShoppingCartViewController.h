//
//  ShoppingCartViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 17/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"

@interface ShoppingCartViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CMPopTipViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *shoppingCartTotalLabel;
@property (weak, nonatomic) IBOutlet UITableView *shoppingCartItemsTableView;
@property (weak, nonatomic) IBOutlet UILabel *codeInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *shoppingCartConfirmButton;
@property (weak, nonatomic) IBOutlet UIView *shoppingCartPriceInfoContainer;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel;
@property (strong, nonatomic) NSMutableArray *shoppingCartItems;
@property (strong, nonatomic) UIViewController *parentVC;
@property (weak, nonatomic) IBOutlet UIButton *applyCouponButton;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIView *minimumLabelContainerView;
@property (weak, nonatomic) IBOutlet UILabel *minimumLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

- (IBAction)confirmButtonAction:(id)sender;
- (IBAction)addItemToShoppingCart:(id)sender;
- (IBAction)substractItemFromShoppingCart:(id)sender;
- (IBAction)clearShoppingCart:(id)sender;
- (IBAction)closeShoppingCart:(id)sender;
- (IBAction)applyCode:(id)sender;
- (IBAction)closeMinimumLabel:(id)sender;
- (IBAction)didTapDeliveryInfo:(id)sender;

- (void)goToOrderDetails:(UIStoryboard *)storyboard;


@end
