//
//  CustomLoaderView.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 16/09/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "CustomLoaderView.h"

@implementation CustomLoaderView

@synthesize animatedImageView = _animatedImageView;
@synthesize loadingMessageLabel = _loadingMessageLabel;

#define kPckAnimationSequenceCount 17
#define kPckAnimationSequenceDuration 1.7

-(void)setupLoader:(UIView *)aSuperView{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0x0), ^(void) {
        NSMutableArray *images= [NSMutableArray array];
        for (int i = 1; i<=kPckAnimationSequenceCount; i++) {
            UIImage *marker_image = [UIImage imageNamed: [NSString stringWithFormat: @"animated_marker_%d",i]];
            [images addObject:marker_image];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self setFrame:[aSuperView bounds]];
            [aSuperView addSubview:self];
            [_animatedImageView setImage:[UIImage animatedImageWithImages:images duration:kPckAnimationSequenceDuration]];
        });
    });
}

-(void)stopAndRemoveLoader{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [_animatedImageView stopAnimating];
        [UIView animateWithDuration:0.0f
                         animations:^{
                             self.alpha = 0.0f;
                         }
                         completion:^(BOOL finished) {
                             [self removeFromSuperview];
                         }];
    });
}

-(id)initWithNibNamed:(NSString*)nibName bundle:(NSBundle*)bundle {
    if(bundle == nil) {
        bundle = [NSBundle mainBundle];
    }
    
    NSArray *topLevelObjects = [bundle loadNibNamed:nibName owner:self options:nil];
    
    for(NSObject *currentObject in topLevelObjects) {
        if([currentObject isKindOfClass:[self class]]) {
            self = (CustomLoaderView *)currentObject;
            break;
        }
    }
    
    return self;
}

- (id)initWithTitle:(NSString*) title
{
    self = [self initWithNibNamed:@"CustomLoaderView" bundle:nil];
    
    if (self) {
        _loadingMessageLabel.text = title;
        UIImage *firstImageInSequence = [UIImage imageNamed:@"animated_marker_1"];
        [_animatedImageView setImage:firstImageInSequence];
    }
    
    return self;
}

@end

