//
//  NotificationPermissionRequestViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 2/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "NotificationPermissionRequestViewController.h"
#import "AuthDashboardViewController.h"
#import "AppDelegate.h"
#import <Google/Analytics.h>

@interface NotificationPermissionRequestViewController ()

@end

@implementation NotificationPermissionRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)trackPageWithGA:(NSString*) origin{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:origin];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (IBAction)acceptAction:(id)sender {
    [self trackPageWithGA:@"Aceptar ver permiso de notificaciones"];
    [((AuthDashboardViewController *)[self parentVC]) showNotificationDefaultAlert];
    [self goBack];
}

- (IBAction)cancelAction:(id)sender {
    [self trackPageWithGA:@"Rechazar ver permiso de notificaciones"];
    [((AuthDashboardViewController *)[self parentVC]) closeAuthDashboard];
    [self goBack];
}

- (void)goBack {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}


@end
