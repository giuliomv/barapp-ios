//
//  SlideDrawerViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 9/09/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "SlideDrawerViewController.h"
#import "CustomSliderCell.h"
#import "CategoriesViewController.h"
#import "SelectLocationViewController.h"
#import "ContentViewController.h"
#import "FidelityPointsInformationController.h"
#import "Constants.h"
#import "AuthDashboardViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "UIColorHelper.h"
#import "BAUser.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "CardsListViewController.h"
#import "OrderListViewController.h"
#import <Google/Analytics.h>

@interface SlideDrawerViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *optionsNamesArray;
@property (nonatomic, strong) NSArray *optionsSelectorsArray;

@end

@implementation SlideDrawerViewController{
    UITapGestureRecognizer *signInTap;
}

@synthesize userNameLabel = _userNameLabel;
@synthesize userCurrentLocationLabel = _userCurrentLocationLabel;
@synthesize userAvatarImageView = _userAvatarImageView;
@synthesize signInButton = _signInButton;
@synthesize userInfoContainerView = _userInfoContainerView;
@synthesize pointsLabelContainer = _pointsLabelContainer;
@synthesize fidelityPointsContainerView = _fidelityPointsContainerView;
@synthesize menuOptionsTableView = _menuOptionsTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userStateChanged) name:@"user_state_changed" object:nil];
    [self fidelityPointsContainerUI];
    [self authBasedUI];
    [self setupSliderOptions];
    [self setupDrawerBackground];
}

-(void)setupDrawerBackground{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:67.0 green:76.0 blue:154.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:138.0 green:64.0 blue:107.0 alpha:1.0];
    CGRect frame =self.view.frame;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.5, 0.0) endPoint:CGPointMake(0.5, 1.0) frame:frame];
    [self.view.layer insertSublayer:ca atIndex:0];
}

- (void)fidelityPointsContainerUI {
    _pointsLabelContainer.layer.borderColor = [UIColorHelper practicalColorWithRed:250.0 green:249.0 blue:250.0 alpha:1.0].CGColor;
    _pointsLabelContainer.layer.borderWidth = 2.0f;
    _pointsLabelContainer.layer.masksToBounds = YES;
    _pointsLabelContainer.layer.cornerRadius = 10.0f;
}

-(void)userStateChanged{
    [self setupSliderOptions];
    [self authBasedUI];
    [_menuOptionsTableView reloadData];
}

-(void)authBasedUI{
    if ([BAUser isUserLoggedIn]) {
        [_fidelityPointsContainerView setHidden:FALSE];
        [_signInButton setHidden:YES];
        [_userInfoContainerView setHidden:NO];
        BAUser *currentUser = [BAUser getLoggedInUser];
        if (currentUser.avatarUrl!=nil) {
            [_userAvatarImageView setImageWithURL:[NSURL URLWithString:currentUser.avatarUrl]];
        }
        _pointsLabel.text = [NSString stringWithFormat:@"%@",currentUser.fidelityPoints];
        _userNameLabel.text = currentUser.fullName;
        _userCurrentLocationLabel.text = [AppDelegate sharedAppDelegate].selectedLocationAddress;
        if (signInTap) {
            [_userAvatarImageView removeGestureRecognizer:signInTap];
            _userAvatarImageView.userInteractionEnabled = NO;
        }
    }else{
        [_fidelityPointsContainerView setHidden:TRUE];
        signInTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToSignIn)];
        [_userAvatarImageView addGestureRecognizer:signInTap];
        _userAvatarImageView.userInteractionEnabled = YES;
        [_signInButton setHidden:NO];
        [_userInfoContainerView setHidden:YES];
    }
}

-(void)goToSignIn{
    [self showAuthDashboard];
    [self setupSliderOptions];
    [_menuOptionsTableView reloadData];
}

-(void)setupSliderOptions{
    if ([BAUser isUserLoggedIn]) {
        _optionsNamesArray = @[@[@"UBICACIÓN", @"CATEGORÍAS", @"MÉTODOS DE PAGO", @"MIS PEDIDOS", @"ACERCA DE BAR APP", @"TÉRMINOS Y CONDICIONES", @"LLÁMANOS"]];
        _optionsSelectorsArray = @[@[@"showSelectionLocation", @"showCategories", @"showMyCards", @"showMyOrders", @"showAboutUs", @"showTermsAndConditions", @"callUs"]];
    }else{
        _optionsNamesArray = @[@[@"UBICACIÓN", @"CATEGORÍAS", @"ACERCA DE BAR APP", @"TÉRMINOS Y CONDICIONES", @"LLÁMANOS"]];
        _optionsSelectorsArray = @[@[@"showSelectionLocation", @"showCategories", @"showAboutUs", @"showTermsAndConditions", @"callUs"]];
    }
}

-(void)showSelectionLocation{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Información"
                                 message:@"Se borrarán los productos agregados a tu carro de compras"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self goToSelectLocation];
                                }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancelar"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                   }];
    
    [alert addAction:yesButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void)goToSelectLocation{
    if([_dynamicsDrawerViewController presentingViewController]){
        [[_dynamicsDrawerViewController presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"selectLocationViewControllerId"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ivc];
        [UIView transitionWithView:[UIApplication sharedApplication].keyWindow
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ [UIApplication sharedApplication].keyWindow.rootViewController = navigationController; }
                        completion:nil];
    }

}

-(void)showAuthDashboard{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AuthDashboardViewController *cvc = (AuthDashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"authDashboardControllerId"];
    cvc.originVc = auth_dashboard_origin_slider;
    [self transitionToViewController:cvc paneViewType:@"AUTH_DASHBOARD"];
    self.paneViewControllerType = nil;
}

-(void)showCategories{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CategoriesViewController *cvc = (CategoriesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"categoriesViewControllerId"];
    [self transitionToViewController:cvc paneViewType:@"CATEGORIES"];
}

-(void)showMyCards{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CardsListViewController *cvc = (CardsListViewController *)[storyboard instantiateViewControllerWithIdentifier:@"cardsListViewControllerId"];
    cvc.origin = card_list_origin_slide_drawer;
    [self transitionToViewController:cvc paneViewType:@"CARDS"];
}

-(void)showMyOrders{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OrderListViewController *cvc = (OrderListViewController *)[storyboard instantiateViewControllerWithIdentifier:@"orderListViewControllerId"];
    [self transitionToViewController:cvc paneViewType:@"ORDERS"];
}

-(void)showAboutUs{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContentViewController *cvc = (ContentViewController *)[storyboard instantiateViewControllerWithIdentifier:@"contentViewControllerId"];
    cvc.rowSelected = [NSNumber numberWithInteger:1];
    [self transitionToViewController:cvc paneViewType:@"ABOUT_US"];
}

-(void)callUs{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:barapp_phone_number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)showTermsAndConditions{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContentViewController *cvc = (ContentViewController *)[storyboard instantiateViewControllerWithIdentifier:@"contentViewControllerId"];
    cvc.rowSelected = [NSNumber numberWithInteger:0];
    [self transitionToViewController:cvc paneViewType:@"TERMS_CONDITIONS"];
}

-(void)showFidelityPointsInformation{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FidelityPointsInformationController *cvc = (FidelityPointsInformationController *)[storyboard instantiateViewControllerWithIdentifier:@"fidelityPointsInformationControllerId"];
    [self transitionToViewController:cvc paneViewType:@"FIDELITY_POINTS"];
}

- (void)transitionToViewController:(UIViewController*) viewController paneViewType:(NSString *)paneViewControllerType
{
    if (paneViewControllerType == self.paneViewControllerType) {
        [self.dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:nil];
        return;
    }
    UINavigationController *paneNavigationViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.dynamicsDrawerViewController setPaneViewController:paneNavigationViewController animated:YES completion:nil];
    self.paneViewControllerType = paneViewControllerType;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _optionsNamesArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray* )[_optionsNamesArray objectAtIndex:section]).count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomSliderCell *cell=[tableView dequeueReusableCellWithIdentifier:@"customSliderCell"];
    cell.optionNameLabel.text = [((NSArray* )[_optionsNamesArray objectAtIndex:indexPath.section]) objectAtIndex:indexPath.row];
    NSArray* formattedArray = @[@"ACERCA DE BAR APP", @"TÉRMINOS Y CONDICIONES", @"LLÁMANOS"];
    if ([formattedArray containsObject:cell.optionNameLabel.text]) {
        [cell.optionNameLabel setFont:[UIFont fontWithName:@"Aller-Light" size:13.0]];
    }else{
        [cell.optionNameLabel setFont:[UIFont fontWithName:@"Aller" size:13.0]];
    }
//    if (indexPath.section==1) {
//        [cell.optionNameLabel setFont:[UIFont fontWithName:@"Aller-Light" size:12.0]];
//    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!self) { return; }
    SEL selector =  NSSelectorFromString([((NSArray* )[_optionsSelectorsArray objectAtIndex:indexPath.section]) objectAtIndex:indexPath.row]);
    IMP imp = [self methodForSelector:selector];
    void (*func)(id, SEL) = (void *)imp;
    func(self, selector);
    // Prevent visual display bug with cell dividers
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [tableView reloadData];
    });
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGRect sepFrame = CGRectMake(0, 15, tableView.frame.size.width - 30, 1);
    UIView *seperatorView =[[UIView alloc] initWithFrame:sepFrame];
    seperatorView.backgroundColor = [UIColor clearColor];
    if (section==1) {
        seperatorView.backgroundColor = [UIColorHelper practicalColorWithRed:255.0 green:255.0 blue:255.0 alpha:0.5];
    }
    return seperatorView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)trackPageWithGA:(NSString*) origin{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:origin];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (IBAction)goToAuthDashboard:(id)sender {
    [self showAuthDashboard];
}

- (IBAction)showFidelityPointsInfoSection:(id)sender {
    [self trackPageWithGA:@"Seleccionar sección de puntos acumulados"];
    [self showFidelityPointsInformation];
}
@end
