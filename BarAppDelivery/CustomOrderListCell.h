//
//  CustomOrderListCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 1/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomOrderListCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *extraInfoLabel;

@end
