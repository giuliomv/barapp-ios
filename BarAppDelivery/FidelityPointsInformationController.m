//
//  FidelityPointsInformationController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 14/03/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import "FidelityPointsInformationController.h"
#import "FidelityBenefitCell.h"
#import "TermsConditionsFidelityCell.h"
#import "UIColorHelper.h"
#import "AppDelegate.h"
#import <Google/Analytics.h>

@implementation FidelityPointsInformationController

@synthesize fidelityBenefitItems = _fidelityBenefitItems;
@synthesize fidelityTermConditionItems = _fidelityTermConditionItems;
@synthesize termsConditionsTableView = _termsConditionsTableView;
@synthesize termsConditionsHeightConstraint = _termsConditionsHeightConstraint;

-(void)viewDidLoad{
    _termsConditionsTableView.rowHeight = UITableViewAutomaticDimension;
    _termsConditionsTableView.estimatedRowHeight = 120;
    _fidelityBenefitItems = [[NSMutableArray alloc] init];
    [_fidelityBenefitItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"icon":@"fidelity_gift_icon", @"title":@"Premios", @"description":@"¡Obtén premios que complementen tus reuniones con amigos!"}]];
    [_fidelityBenefitItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"icon":@"fidelity_beer_icon", @"title":@"¿Cómo funciona?", @"description":@"Por cada compra acumulas puntos que podrás canjear por productos del catálogo de recompensas."}]];
    [_fidelityBenefitItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"icon":@"fidelity_jewel_icon", @"title":@"¿Cómo canjeo mis premios?", @"description":@"Al concretar una compra a través de nuestra aplicación, indica que deseas hacer uso de tus puntos en la llamada de confirmación."}]];
    
    _fidelityTermConditionItems = [[NSMutableArray alloc] init];
    [_fidelityTermConditionItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"title":@"¿Qué es el programa Lealtad a la diversión?", @"description":@"El programa Lealtad a la Diversión es nuestro programa de beneficios en Perú, el cual le permite adquirir puntos por la compra de productos ofrecidos a través de nuestra aplicación y canjearlo por premios."}]];
    [_fidelityTermConditionItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"title":@"¿Puedo obtener puntos si realizo una compra por teléfono en vez de por la aplicación?", @"description":@"No. Lamentablemente las compras que se realizan por teléfono no pueden quedar registradas en nuestro sistema."}]];
    [_fidelityTermConditionItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"title":@"¿Qué le sucederá a mis puntos cuando los intercambie por premios?", @"description":@"Los puntos utilizados serán descontados de tu cuenta."}]];
    [_fidelityTermConditionItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"title":@"¿Qué sucederá con mis puntos si cancelo una orden?", @"description":@"Los puntos de Lealtad a la diversión se otorgan por cada producto que es pagado. Si un producto es cancelado de tu orden, no recibirás puntos por él."}]];
    [_fidelityTermConditionItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"title":@"¿Debo realizar una compra para poder canjear un premio?", @"description":@"Sí, para efectuar el canje de tus puntos primero debes realizar una compra a través de nuestra plataforma."}]];
    [_fidelityTermConditionItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"title":@"¿Que pasa si en ese momento se quedan sin stock de premios?", @"description":@"Esta sujeto a el stock de la red de librerías que Bar App maneja."}]];
    [_fidelityTermConditionItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"title":@"¿Qué son los premios?", @"description":@"Los premios son productos que benefician tus experiencias compartidas con amigos. Puedes escoger cualquier producto del Catálogo de recompensas si cuentas con la cantidad suficiente de puntos en tu cuenta para algún producto en particular."}]];
    [_fidelityTermConditionItems addObject:[NSMutableDictionary dictionaryWithDictionary:@{@"title":@"¿Puedo devolver un premio?", @"description":@"Desafortunadamente, las recompensas no pueden ser devueltas o intercambiadas por puntos, dinero u otro producto."}]];
}

- (void)setupNavigationUI {
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Gobold" size:14]}];
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    UIImage *image = [UIImage imageNamed:@"nav-logo"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
}

-(void)viewWillAppear:(BOOL)animated{
    [self setupNavigationUI];
    [self trackPageWithGA];
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Sección Informativa del Programa de Fidelización"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidLayoutSubviews{
    _termsConditionsHeightConstraint.constant = _termsConditionsTableView.contentSize.height;
    [self.view layoutIfNeeded];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag==301) {
        return [_fidelityBenefitItems count];
    } else {
        return [_fidelityTermConditionItems count];
    }
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (tableView.tag==301) {
//        return 220.0;
//    }
//    return ;
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==301) {
        FidelityBenefitCell *cellF=[tableView dequeueReusableCellWithIdentifier:@"fidelityBenefitCell"];
        [self configureCellBenefit:cellF atIndexPath:indexPath];
        return cellF;
    } else {
        TermsConditionsFidelityCell *cellT=[tableView dequeueReusableCellWithIdentifier:@"termsConditionsFidelityCell"];
        [self configureCellTermCondition:cellT atIndexPath:indexPath];
        return cellT;
    }
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:
(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)configureCellBenefit:(FidelityBenefitCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Update Cell
    [cell.benefitTitle setText:[_fidelityBenefitItems objectAtIndex:indexPath.row][@"title"]];
    [cell.benefitDescription setText:[_fidelityBenefitItems objectAtIndex:indexPath.row][@"description"]];
    [cell.benefitIcon setImage:[UIImage imageNamed:[_fidelityBenefitItems objectAtIndex:indexPath.row][@"icon"]]];
}

- (void)configureCellTermCondition:(TermsConditionsFidelityCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Update Cell
    [cell.termConditionTitle setText:[_fidelityTermConditionItems objectAtIndex:indexPath.row][@"title"]];
    [cell.termConditionDescription setText:[_fidelityTermConditionItems objectAtIndex:indexPath.row][@"description"]];
}


- (IBAction)openSliderDrawer:(id)sender {
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:YES completion:^{
    }];
}
@end
