//
//  BAUser+CoreDataProperties.m
//  
//
//  Created by Giulio Mondoñedo on 1/12/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BAUser+CoreDataProperties.h"

@implementation BAUser (CoreDataProperties)

@dynamic fullName;
@dynamic firstName;
@dynamic phoneNumber;
@dynamic birthday;
@dynamic avatarUrl;
@dynamic fidelityPoints;
@dynamic userEmail;

@end
