//
//  NotificationPermissionRequestViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 2/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationPermissionRequestViewController : UIViewController

@property (strong, nonatomic) UIViewController *parentVC;

- (IBAction)acceptAction:(id)sender;
- (IBAction)cancelAction:(id)sender;

@end
