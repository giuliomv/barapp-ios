//
//  Combo.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Combo : NSManagedObject

+(Combo*)getComboById:(NSNumber *)comboId
           bannerType:(NSString *)bannerType
              context:(NSManagedObjectContext *)context;

+(void)deleteAllCombos:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Combo+CoreDataProperties.h"
