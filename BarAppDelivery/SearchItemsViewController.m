//
//  SearchItemsViewController.m
//  BarAppDelivery
//
//  Created by Home on 6/3/20.
//  Copyright © 2020 Giulio Mondoñedo. All rights reserved.
//

#import "SearchItemsViewController.h"
#import "RequestBuilder.h"
#import "AppDelegate.h"
#import "SearchResultsItemCell.h"
#import "ShoppingCartItem.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Constants.h"

@interface SearchItemsViewController ()

@end

@implementation SearchItemsViewController

@synthesize companyId = _companyId;
@synthesize resultsItems = _resultsItems;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    _companyId = [AppDelegate sharedAppDelegate].closestCompany;
    // Do any additional setup after loading the view.
}

- (void)handleSearchInput:(NSString *)text{
    NSLog(@"QUERY: %@", text);
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_items_by_query:text companyId:_companyId parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                
                NSDictionary *jsonObj = (NSDictionary *) responseObject;
                NSArray *productsArray = jsonObj[@"products"];
                NSArray *combosArray = jsonObj[@"combos"];
                _resultsItems = [NSMutableArray array];
//                NSString *scheduleText = jsonObj[@"schedule_text"][@"current_text"];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
//                    [_activityIndicatorContainerView setHidden:true];
                    NSLog(@"PRODD %ld", productsArray.count);
                    NSLog(@"COMBO %ld", combosArray.count);
                    for (int i=0; i<productsArray.count; i++) {
                        NSNumber *prodId = [[productsArray objectAtIndex:i] valueForKey:@"id"];
                        NSError *error = nil;
                        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
                        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"productId==%@",prodId];
                        request.predicate=predicate;
                        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
                        NSArray *products = [_managedObjectContext executeFetchRequest:request error:&error];
                        if (error) {
                            [NSException raise:@"no product find" format:@"%@", [error localizedDescription]];
                        }
                        Product *record = nil;
                        if (products.count > 0) {
                            record = [products lastObject];
                        }else {
                            record = [[Product alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
                            record.qty = [NSNumber numberWithInt:(0)];
                            record.addedToCart = [NSNumber numberWithInt:(0)];
                        }
                        
                        record.name = [[productsArray objectAtIndex:i] valueForKey:@"name"];
                        record.productId = [[productsArray objectAtIndex:i] valueForKey:@"id"];
                        record.categoryId = [[productsArray objectAtIndex:i] valueForKey:@"category_id"];;
                        record.deletedObj = [[productsArray objectAtIndex:i] valueForKey:@"deleted"];
                        record.flgStock = [[productsArray objectAtIndex:i] valueForKey:@"flg_stock"];
                        if ([[productsArray objectAtIndex:i] valueForKey:@"order_key"] == [NSNull null]) {
                            record.orderKey = @10;
                        } else {
                            record.orderKey = [[productsArray objectAtIndex:i] valueForKey:@"order_key"];
                        }
                        if ([[productsArray objectAtIndex:i] valueForKey:@"product_img_v2_url"] == [NSNull null]) {
                            record.imgUrl = @"";
                        } else {
                            record.imgUrl = [[productsArray objectAtIndex:i] valueForKey:@"product_img_v2_url"];
                        }
                        
                        id val = [[productsArray objectAtIndex:i] valueForKey:@"description"];
                        val = (val == [NSNull null] ? @"" : val);
                        record.productDescription = val;
                        
//                        record.productImgCode = _productImgCode;
                        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                        f.numberStyle = NSNumberFormatterDecimalStyle;
                        record.unitPrice = [f numberFromString:[[productsArray objectAtIndex:i] valueForKey:@"unitPrice"]];

                        if ([self.managedObjectContext save:&error]) {
                            ShoppingCartItem* item = [[ShoppingCartItem alloc] initWithItemId:record.productId name:record.name price:record.unitPrice qty:record.qty isCombo:false itemImage:record.imgUrl itemDescription:record.productDescription isAlcoholic:[record isAlcoholicProduct:self.managedObjectContext]];
                            [_resultsItems addObject:item];
                        } else {
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                    
                    NSEntityDescription *comboEntity = [NSEntityDescription entityForName:@"Combo" inManagedObjectContext:self.managedObjectContext];
                    for (int j=0; j<combosArray.count; j++) {
                        NSNumber *comboId = [[combosArray objectAtIndex:j] valueForKey:@"id"];
                        NSString *bannerType = [[combosArray objectAtIndex:j] valueForKey:@"banner_type"];
                        NSError *error = nil;
                        Combo *combo = [Combo getComboById:comboId bannerType:bannerType context:self.managedObjectContext];
                        if (error) {
                            [NSException raise:@"no combo find" format:@"%@", [error localizedDescription]];
                        }
                        
                        Combo *record = nil;
                        if (combo!=nil) {
                            record = combo;
                        }else {
                            record = [[Combo alloc] initWithEntity:comboEntity insertIntoManagedObjectContext:self.managedObjectContext];
                            record.qty = [NSNumber numberWithInt:(0)];
                            record.addedToCart = [NSNumber numberWithInt:(0)];
                        }
                        
                        record.name = [[combosArray objectAtIndex:j] valueForKey:@"name"];
                        record.comboId = [[combosArray objectAtIndex:j] valueForKey:@"id"];
                        record.orderKey = [[combosArray objectAtIndex:j] valueForKey:@"order_key"];
                        record.bannerType = [[combosArray objectAtIndex:j] valueForKey:@"banner_type"];
                        
                        if ([record.bannerType isEqualToString:@"Combo"]) {
                            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                            f.numberStyle = NSNumberFormatterDecimalStyle;
                            record.price = [f numberFromString:[[combosArray objectAtIndex:j] valueForKey:@"price"]];
                            id val = [[combosArray objectAtIndex:j] valueForKey:@"description"];
                            val = (val == [NSNull null] ? nil : val);
                            
                            record.deletedObj = [[combosArray objectAtIndex:j] valueForKey:@"deleted"];
                            record.flgStock = [[combosArray objectAtIndex:j] valueForKey:@"flg_stock"];
                            record.shoppingCartImgUrl = [[combosArray objectAtIndex:j] valueForKey:@"img_shopping_cart_url"];
                            record.imgUrl = [[combosArray objectAtIndex:j] valueForKey:@"img_v2_url"];
                        }else{
                            record.deletedObj = [NSNumber numberWithInt:(0)];;
                            record.flgStock = [NSNumber numberWithInt:(1)];;
                            record.imgUrl = [[combosArray objectAtIndex:j] valueForKey:@"img_url"];
                        }
                        if ([self.managedObjectContext save:&error]) {
                            ShoppingCartItem *item = [[ShoppingCartItem alloc] initWithItemId:record.comboId name:record.name price:record.price qty:record.qty isCombo:true itemImage:record.imgUrl itemDescription:record.comboDescription isAlcoholic:true];
                            [_resultsItems addObject:item];
                        } else {
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                    NSLog(@"%ld", _resultsItems.count);
                    [_resultsTableView reloadData];
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
//                [_activityIndicatorContainerView setHidden:true];
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _resultsItems.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    SearchResultsItemCell *cell=[tableView dequeueReusableCellWithIdentifier:@"customSearchResultsItemCellId"];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(SearchResultsItemCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    ShoppingCartItem *record = [_resultsItems objectAtIndex:indexPath.row];
    
    // Update Cell
    NSLog(@"QTY SEA %@", record.qty);
    [cell.searchResultsItemName setText:record.name];
    [cell.searchResultsItemQty setText:[NSString stringWithFormat:@"%@",record.qty]];
    [cell.searchResultsItemPrice setText:[NSString stringWithFormat:@"S/. %@", record.price]];
    [cell.searchResultsThumbnailImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", server_base_url, record.itemImage]] placeholderImage:[UIImage imageNamed:@"ba_product_placeholder"]];
    [cell.searchResultsItemDescription setText:record.itemDescription];
}


- (IBAction)searchMinusSelected:(id)sender {
    UITableViewCell *cell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [_resultsTableView indexPathForCell:cell];
    ShoppingCartItem *record = [_resultsItems objectAtIndex:indexPath.row];
    if (record.qty.intValue > 1) {
        if (record.isCombo) {
            [[AppDelegate sharedShoppingCart] addComboToShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]]) quantity:[NSNumber numberWithInt:(record.qty.intValue - 1)]];
        }else{
            [[AppDelegate sharedShoppingCart] addProductToShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]]) quantity:[NSNumber numberWithInt:(record.qty.intValue - 1)]];
        }
        record.qty = [NSNumber numberWithInt:(record.qty.intValue - 1)];
        [_resultsTableView reloadData];
    }
}

- (IBAction)searchPlusSelected:(id)sender {
    UITableViewCell *cell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [_resultsTableView indexPathForCell:cell];
    ShoppingCartItem *record = [_resultsItems objectAtIndex:indexPath.row];
    NSLog(@"FAIL1: %@", ((ShoppingCartItem*)[self.resultsItems objectAtIndex:0]).qty);
    if (record.isCombo) {
        [[AppDelegate sharedShoppingCart] addComboToShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]]) quantity:[NSNumber numberWithInt:(record.qty.intValue + 1)]];
    }else{
        [[AppDelegate sharedShoppingCart] addProductToShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]]) quantity:[NSNumber numberWithInt:(record.qty.intValue + 1)]];
    }
    record.qty = [NSNumber numberWithInt:(record.qty.intValue + 1)];
    NSLog(@"FAIL2: %@", ((ShoppingCartItem*)[self.resultsItems objectAtIndex:0]).qty);
    [_resultsTableView reloadData];
}

@end
