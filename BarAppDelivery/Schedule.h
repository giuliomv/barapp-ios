//
//  Schedule.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Schedule : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+(NSMutableDictionary*)getFormattedSchedules:(NSManagedObjectContext *)context;
+(void)deleteAllSchedules:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Schedule+CoreDataProperties.h"
