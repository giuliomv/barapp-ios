//
//  Product.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "Product.h"
#import "BACategory.h"
#import "AppDelegate.h"

@implementation Product

// Insert code here to add functionality to your managed object subclass

+(Product*)getProductById:(NSNumber *)productId
              context:(NSManagedObjectContext *)context{
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"productId == %@", productId];
    request.predicate=predicate;
    NSArray *products = [context executeFetchRequest:request error:&error];
    if (error) {
        [NSException raise:@"no product found" format:@"%@", [error localizedDescription]];
    }
    
    return products.lastObject;
}

-(bool)isAlcoholicProduct:(NSManagedObjectContext *)context{
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"BACategory"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"categoryId == %@", self.categoryId];
    [request setPredicate:predicate];
    NSArray *categories = [context executeFetchRequest:request error:&error];
    if (!error) {
        return (((BACategory *)categories.lastObject).isAlcoholic.intValue == 1);
    }
    return true;
}

+(void)deleteAllProducts:(NSManagedObjectContext *)context{
    NSFetchRequest *allProducts = [[NSFetchRequest alloc] init];
    [allProducts setEntity:[NSEntityDescription entityForName:@"Product" inManagedObjectContext:context]];
    [allProducts setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *products = [context executeFetchRequest:allProducts error:&error];
    //error handling goes here
    for (NSManagedObject *prod in products) {
        [context deleteObject:prod];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}


@end
