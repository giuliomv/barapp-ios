//
//  TermsConditionsFidelityCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 14/03/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsConditionsFidelityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *termConditionTitle;
@property (weak, nonatomic) IBOutlet UILabel *termConditionDescription;

@end
