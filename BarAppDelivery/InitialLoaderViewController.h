//
//  InitialLoaderViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 13/11/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CustomAnimatedBackgroundView.h"

@interface InitialLoaderViewController : UIViewController

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *locationActivityIndicator;
@property (strong, nonatomic) NSString *selectedLocationAddress;
@property (nonatomic, assign) CLLocationCoordinate2D selectedLocationCoordinates;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSMutableArray *colorsArray;
@property (nonatomic) CAGradientLayer *gradientLayer;

-(void)locationPermissionRequestWasDenied;
-(void)locationPermissionRequestWasAccepted;

@end
