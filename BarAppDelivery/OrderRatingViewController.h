//
//  OrderRatingViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 31/05/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceHolderTextView.h"
#import "HCSStarRatingView.h"

@interface OrderRatingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *commentsTextView;
@property (strong, nonatomic) NSNumber *orderId;
- (IBAction)submitRating:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingStarsView;
@property (weak, nonatomic) IBOutlet UIView *thanksContainerView;

@end
