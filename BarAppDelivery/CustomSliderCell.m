//
//  CustomSliderCell.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 22/09/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "CustomSliderCell.h"
#import "UIColorHelper.h"

@implementation CustomSliderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    

    // Configure the view for the selected state
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    if (highlighted) {
        self.contentView.backgroundColor = [UIColorHelper practicalColorWithRed:83.0 green:109 blue:254 alpha:0.3];
    } else {
        self.contentView.backgroundColor = [UIColor clearColor];
    }
}

@end
