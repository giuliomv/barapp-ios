//
//  ProductsTableViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import "ProductsTableViewController.h"
#import "ShoppingCartViewController.h"
#import "CustomProductCell.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "RequestBuilder.h"
#import "Product.h"
#import "UIColorHelper.h"
#import "ShoppingCart.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "ProductImageViewController.h"
#import "BBBadgeBarButtonItem.h"
#import <AudioToolbox/AudioServices.h>
#import <Google/Analytics.h>
#import "CustomLoaderView.h"
#import "Constants.h"
#import "UIScrollView+APParallaxHeader.h"
#import "BrandBanner+CoreDataClass.h"

@interface ProductsTableViewController () <NSFetchedResultsControllerDelegate>{
    CustomLoaderView *activityLoader;
    NSTimer *scrollTimer;
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) BBBadgeBarButtonItem *shoppingCartBarButtonItem;

@end

@implementation ProductsTableViewController

@synthesize productsTableView = _productsTableView;
@synthesize categoryId = _categoryId;
@synthesize companyId = _companyId;
@synthesize productImgCode = _productImgCode;
@synthesize categoryName = _categoryName;
@synthesize shoppingCartButton = _shoppingCartButton;
@synthesize shoppingCartZeroButton = _shoppingCartZeroButton;
@synthesize productsActivityIndicator = _productsActivityIndicator;
@synthesize brandsScrollView = _brandsScrollView;

- (void)viewDidLoad {
    [super viewDidLoad];
    _productsTableView.rowHeight = UITableViewAutomaticDimension;
    _productsTableView.estimatedRowHeight = 125;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    [self setupBannerSlider];
    [self initFetchController];
    
    [self requestProductsByCategory:_categoryId];
    [self setupShoppingCartBarButtonItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateShoppingCartBadge) name:@"shopping_cart_modified" object:nil];
}

-(void)setupBannerSlider{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BrandBanner" inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categoryId = %@", _categoryId];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSArray *fetchedObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count>0) {
        [_brandsScrollView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 107.0f)];
        for (int i = 0; i< fetchedObjects.count; i++) {
            CGRect frame = CGRectMake(0,0,0,0);
            frame.origin.x = _brandsScrollView.frame.size.width * i;
            frame.size = _brandsScrollView.frame.size;
            UIImageView* comboImage = [[UIImageView alloc] initWithFrame:frame];
            [comboImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", server_base_url, ((BrandBanner*)[fetchedObjects objectAtIndex:i ]).imgUrl]] placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_product_header",_productImgCode]]];
            [_brandsScrollView addSubview:comboImage];
        }
        [_productsTableView setTableHeaderView:_brandsScrollView];
        _brandsScrollView.contentSize = CGSizeMake(_brandsScrollView.frame.size.width * fetchedObjects.count, _brandsScrollView.frame.size.height);
         scrollTimer = [NSTimer scheduledTimerWithTimeInterval:3.5 target:self selector:@selector(scrollBanner) userInfo:nil repeats:YES];
    } else {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_product_header",_productImgCode]]];
        CGRect frame = imageView.bounds;
        frame.size.height = 107.0f;
        [imageView setFrame:frame];
        [_productsTableView setTableHeaderView:imageView];
    }
}

-(void)scrollBanner{
    CGFloat contentOffset = _brandsScrollView.contentOffset.x;
    int nextPage = (int)(contentOffset/_brandsScrollView.frame.size.width) + 1;
    if( nextPage != _brandsScrollView.subviews.count){
        [_brandsScrollView scrollRectToVisible:CGRectMake(nextPage*_brandsScrollView.frame.size.width, 0, _brandsScrollView.frame.size.width, _brandsScrollView.frame.size.height) animated:YES];
    }else{
        [_brandsScrollView scrollRectToVisible:CGRectMake(0, 0, _brandsScrollView.frame.size.width, _brandsScrollView.frame.size.height) animated:YES];
    }
}

-(void)setupNavigationBar{
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = false;
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:_categoryName.uppercaseString attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
}

-(void)updateShoppingCartBadge{
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    [self setupShoppingCartAmount];
}

-(void)setupShoppingCartAmount{
    long combos = [[AppDelegate sharedShoppingCart] amountOfCombosByQty];
    long products = [[AppDelegate sharedShoppingCart] amountOfProductsByQty];
    _shoppingCartBarButtonItem.badgeValue = [NSString stringWithFormat:@"%ld",combos + products];
}

-(void)setupShoppingCartBarButtonItem{
    BBBadgeBarButtonItem *barButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:_shoppingCartButton zeroUIButton:_shoppingCartZeroButton];
    _shoppingCartBarButtonItem = barButton;
    barButton.badgeOriginX = 5.0;
    barButton.badgeOriginY = -2.0;
    barButton.badgeMinSize = 10.0;
    barButton.badgeBGColor = [UIColor clearColor];
    [self setupShoppingCartAmount];
    self.navigationItem.rightBarButtonItem = barButton;
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
 [_productsTableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [_productsTableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [_productsTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [_productsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self configureCell:(CustomProductCell *)[_productsTableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [_productsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [_productsTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

- (void)configureCell:(CustomProductCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    Product *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Update Cell
    [cell.productNameLabel setText:record.name];
    if ([record.imgUrl isEqual: @""]) {
        [cell.productImageView setImage:[UIImage imageNamed:@"ba_product_placeholder"]];
    }else{
        [cell.productImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", server_base_url, record.imgUrl]] placeholderImage:[UIImage imageNamed:@"ba_product_placeholder"]];
    }
    
    [cell.productUnitCostLabel setText:[NSString stringWithFormat:@"S/.%.02f",[record.unitPrice floatValue]]];
    [cell.productQtyLabel setText:[NSString stringWithFormat:@"%@",record.qty]];
    [cell.productDescriptionLabel setText:record.productDescription];
}

-(void) showMarkerLoader{
    [_productsTableView setBackgroundView: _productsActivityIndicator];
    [_productsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    //[_productsActivityIndicator setCenter:self.view.center];
    [_productsActivityIndicator setHidden:false];
//    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando"];
//    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

- (void)removeLoadingActivity {
    [_productsTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_productsActivityIndicator stopAnimating];
    [_productsActivityIndicator setHidden:true];
}

-(void) requestProductsByCategory:(NSNumber*)categoryId{
    [self showMarkerLoader];
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_products_by_category:categoryId companyId:_companyId parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                NSArray *jsonArray = (NSArray *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self removeLoadingActivity];
                    for (int i=0; i<jsonArray.count; i++) {
                        NSNumber *prodId = [[jsonArray objectAtIndex:i] valueForKey:@"id"];
                        NSError *error = nil;
                        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
                        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"productId==%@",prodId];
                        request.predicate=predicate;
                        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
                        NSArray *products = [_managedObjectContext executeFetchRequest:request error:&error];
                        if (error) {
                            [NSException raise:@"no product find" format:@"%@", [error localizedDescription]];
                        }
                        Product *record = nil;
                        if (products.count > 0) {
                            record = [products lastObject];
                        }else {
                            record = [[Product alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
                            record.qty = [NSNumber numberWithInt:(0)];
                            record.addedToCart = [NSNumber numberWithInt:(0)];
                        }
                        
                        record.name = [[jsonArray objectAtIndex:i] valueForKey:@"name"];
                        record.productId = [[jsonArray objectAtIndex:i] valueForKey:@"id"];
                        record.categoryId = _categoryId;
                        record.deletedObj = [[jsonArray objectAtIndex:i] valueForKey:@"deleted"];
                        record.flgStock = [[jsonArray objectAtIndex:i] valueForKey:@"flg_stock"];
                        if ([[jsonArray objectAtIndex:i] valueForKey:@"order_key"] == [NSNull null]) {
                            record.orderKey = @10;
                        } else {
                            record.orderKey = [[jsonArray objectAtIndex:i] valueForKey:@"order_key"];
                        }
                        if ([[jsonArray objectAtIndex:i] valueForKey:@"product_img_v2_url"] == [NSNull null]) {
                            record.imgUrl = @"";
                        } else {
                            record.imgUrl = [[jsonArray objectAtIndex:i] valueForKey:@"product_img_v2_url"];
                        }
                        
                        id val = [[jsonArray objectAtIndex:i] valueForKey:@"description"];
                        val = (val == [NSNull null] ? @"" : val);
                        record.productDescription = val;
                        
                        record.productImgCode = _productImgCode;
                        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                        f.numberStyle = NSNumberFormatterDecimalStyle;
                        record.unitPrice = [f numberFromString:[[jsonArray objectAtIndex:i] valueForKey:@"unitPrice"]];

                        if ([self.managedObjectContext save:&error]) {
                            
                        } else {
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                [self removeLoadingActivity];
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];

}

- (void)initFetchController {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categoryId = %@ AND deletedObj = 0 AND flgStock = 1",_categoryId];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"orderKey" ascending:YES]]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsController.fetchRequest setPredicate:predicate];
    [self.fetchedResultsController setDelegate:self];
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [self setupNavigationBar];
    [self trackPageWithGA];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self invalidateTimer];
}


- (void)invalidateTimer{
    [scrollTimer invalidate];
    scrollTimer = nil;
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Página de productos por categoría: %@", _categoryName]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) qtyChanged:(UIButton *)sender value:(double)val{
    UITableViewCell *cell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [_productsTableView indexPathForCell:cell];
    Product *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    double newQty = record.qty.doubleValue + val;
    if (newQty>0) {
        [[AppDelegate sharedShoppingCart] addProductToShoppingCart:record quantity:[NSNumber numberWithDouble:newQty]];
    }else if (newQty==0){
        [[AppDelegate sharedShoppingCart] removeProductFromShoppingCart:record];
    }
}

- (IBAction)minusSelected:(id)sender {
    [self qtyChanged:sender value:-1];
}

- (IBAction)plusSelected:(id)sender {
    [self qtyChanged:sender value:1];
}

- (IBAction)goBack:(id)sender {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"shopping_cart_modified"
                                                  object:nil];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)goToShoppingCart:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ShoppingCartViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"shoppingCartViewControllerNavId"];
    [[self navigationController] presentViewController:vc animated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController* destination = [segue destinationViewController];
    if (destination.class == ProductImageViewController.class) {
        Product *record = [self.fetchedResultsController objectAtIndexPath:[_productsTableView indexPathForSelectedRow]];
        ((ProductImageViewController*)destination).product = record;
        if ([record.imgUrl isEqualToString:@""]) {
            ((ProductImageViewController*)destination).productImgUrl = nil;
            ((ProductImageViewController*)destination).productImgName = record.productImgCode;
        } else {
            ((ProductImageViewController*)destination).productImgUrl = record.imgUrl;
            ((ProductImageViewController*)destination).productImgName = nil;
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomProductCell *cell=[tableView dequeueReusableCellWithIdentifier:@"customProductCell"];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 107.0f;
//}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIImageView *newProductImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_product_header",_productImgCode]]];
//    return newProductImage;
//}


@end
