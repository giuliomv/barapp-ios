//
//  CustomAnimatedBackgroundView.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 17/11/17.
//  Copyright © 2017 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface CustomAnimatedBackgroundView : UIView

@property (nonatomic) IBInspectable UIColor *firstColor;
@property (nonatomic) IBInspectable UIColor *secondColor;
@property (nonatomic) IBInspectable BOOL horizontalGradient;
- (void)customInit;

@end
