//
//  CardTableViewCell.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 15/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "CardTableViewCell.h"

@implementation CardTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.origin.x += 18.0;
    frame.size.width -= 2 * 18.0;
    [super setFrame:frame];
}


@end
