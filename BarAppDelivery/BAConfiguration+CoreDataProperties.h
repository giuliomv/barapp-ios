//
//  BAConfiguration+CoreDataProperties.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BAConfiguration.h"

NS_ASSUME_NONNULL_BEGIN

@interface BAConfiguration (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *scheduleText;

@end

NS_ASSUME_NONNULL_END
