//
//  ConfirmedOrderViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 30/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "ConfirmedOrderViewController.h"
#import "ShoppingCartItem.h"
#import "AppDelegate.h"
#import <Google/Analytics.h>
#import "UIColorHelper.h"
#import "BAUser.h"
#import "CardItem+CoreDataClass.h"
#import "CategoriesViewController.h"

@interface ConfirmedOrderViewController ()

@end

@implementation ConfirmedOrderViewController

@synthesize address = _address;
@synthesize phoneNumber = _phoneNumber;
@synthesize contactName = _contactName;
@synthesize total = _total;
@synthesize addressReference = _addressReference;
@synthesize deliveryFee = _deliveryFee;
@synthesize paymentMethod = _paymentMethod;
@synthesize cashAmount = _cashAmount;
@synthesize orderPoints = _orderPoints;
@synthesize addressLabel = _addressLabel;
@synthesize contactNameLabel = _contactNameLabel;
@synthesize referenceLabel = _referenceLabel;
@synthesize deliveryLabel = _deliveryLabel;
@synthesize totalLabel = _totalLabel;
@synthesize phoneTextLabel = _phoneTextLabel;
@synthesize obtainedPointsLabel = _obtainedPointsLabel;
@synthesize cashAmountLabel = _cashAmountLabel;
@synthesize accumulatedPointsLabel = _accumulatedPointsLabel;
@synthesize pickingBottlesImageAnimation = _pickingBottlesImageAnimation;
@synthesize cardSelected = _cardSelected;
@synthesize cardSelectedImageView = _cardSelectedImageView;
@synthesize parentVC = _parentVC;


- (void)setupConfirmationTexts {
    _addressLabel.text = _address;
    _contactNameLabel.text = _contactName;
    _referenceLabel.text = _addressReference;
    NSString *deliveryFeeString = @"Gratis";
    if ([AppDelegate sharedAppDelegate].deliveryFee.integerValue != 0) {
        deliveryFeeString = [NSString stringWithFormat:@"S/. %.02f",[AppDelegate sharedAppDelegate].deliveryFee.floatValue];
    }
    [_deliveryLabel setText:deliveryFeeString];
    _totalLabel.text = [NSString stringWithFormat:@"S/. %@",_total];
    
    if ([_paymentMethod isEqualToString:@"CASH"]) {
        [_cardSelectedImageView setHidden:true];
        _cashAmountLabel.text = [NSString stringWithFormat:@"S/. %@",_cashAmount];
    }else if ([_paymentMethod isEqualToString:@"online"]){
        CardItem *card = [CardItem getCardById:_cardSelected context:[[AppDelegate sharedAppDelegate] managedObjectContext]];
        [_cardSelectedImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"card_%@", [card.cardBrand lowercaseString]]]];
        _cashAmountLabel.text = [NSString stringWithFormat:@"*%@",card.lastFour];
    }else{
        [_cardSelectedImageView setHidden:true];
       _cashAmountLabel.text = @"VISA";
    }
}

- (void)cleanShoppingCart {
    [AppDelegate sharedAppDelegate].couponApplied = nil;
    NSMutableArray * shoppingCartItems = [NSMutableArray array];
    [shoppingCartItems addObjectsFromArray:([[AppDelegate sharedShoppingCart] shoppingCartCombosItems])];
    [shoppingCartItems addObjectsFromArray:([[AppDelegate sharedShoppingCart] shoppingCartProductsItems])];
    for (int i = 0; i < shoppingCartItems.count ; i++) {
        ShoppingCartItem *record = [shoppingCartItems objectAtIndex:i];
        if (record.isCombo) {
            [[AppDelegate sharedShoppingCart] removeComboFromShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
        }else{
            [[AppDelegate sharedShoppingCart] removeProductFromShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
        }
    }
    [shoppingCartItems removeAllObjects];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _totalLabel.text = [NSString stringWithFormat:@"S/. %.02f", _total.floatValue];
    [self animatePickingBottles];
    [self cleanShoppingCart];
    [self setupConfirmationTexts];
    if ([BAUser isUserLoggedIn]) {
        _obtainedPointsLabel.text = [NSString stringWithFormat:@"%@ Puntos", _orderPoints];
    }else{
        [_obtainedPointsLabel setHidden:TRUE];
        [_accumulatedPointsLabel setHidden:TRUE];
    }
}

-(void)animatePickingBottles{
    NSMutableArray *pickingBottlesImageArray = [@[] mutableCopy];
    for (int i = 1; i <= 12; i++) {
        UIImage *bottleImage = [UIImage imageNamed:[NSString stringWithFormat:@"picking_bottles_%d", i]];
        [pickingBottlesImageArray addObject:bottleImage];
    }
    for (int i = 11; i >= 2; i--) {
        UIImage *bottleImage = [UIImage imageNamed:[NSString stringWithFormat:@"picking_bottles_%d", i]];
        [pickingBottlesImageArray addObject:bottleImage];
    }
    _pickingBottlesImageAnimation.animationImages = pickingBottlesImageArray;
    _pickingBottlesImageAnimation.animationRepeatCount = 0;
    _pickingBottlesImageAnimation.animationDuration = 3;
    [_pickingBottlesImageAnimation startAnimating];
    
}

-(void)setupNavigationView{
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.navigationItem.title attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
}

-(void)viewWillAppear:(BOOL)animated{
    [self trackPageWithGA];
    [self setupNavigationView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [_pickingBottlesImageAnimation stopAnimating];
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Página de pedido confirmado"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goBackToCategories:(id)sender {
    [[self navigationController]popViewControllerAnimated:YES];
    [((CategoriesViewController*)_parentVC) showOrdersList];
}
@end
