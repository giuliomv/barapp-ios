//
//  ShoppingCartViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 17/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import "ShoppingCart.h"
#import "ShoppingCartItem.h"
#import "Schedule.h"
#import "AppDelegate.h"
#import "ShoppingCartItemCell.h"
#import "SelectLocationViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "BAConfiguration.h"
#import <Google/Analytics.h>
#import "UIColorHelper.h"
#import "RequestBuilder.h"
#import "CustomLoaderView.h"
#import "Constants.h"
#import "BAUser.h"
#import "OrderDetailsViewController.h"
#import "AuthDashboardViewController.h"
#import "CMPopTipView.h"

@interface ShoppingCartViewController ()<UITextFieldDelegate>{
    CustomLoaderView *activityLoader;
}

@property (nonatomic, strong) UITextField *activeField;
@property (nonatomic, strong) CMPopTipView *currentPopTipViewTarget;
@property (nonatomic, strong) NSMutableDictionary *deliveryOptions;

@end

@implementation ShoppingCartViewController

@synthesize parentVC = _parentVC;
@synthesize shoppingCartItems = _shoppingCartItems;
@synthesize shoppingCartPriceInfoContainer = _shoppingCartPriceInfoContainer;
@synthesize codeTextField = _codeTextField;
@synthesize codeInfoLabel = _codeInfoLabel;
@synthesize shoppingCartConfirmButton = _shoppingCartConfirmButton;
@synthesize deliveryFeeLabel = _deliveryFeeLabel;
@synthesize minimumLabelContainerView = _minimumLabelContainerView;
@synthesize minimumLabel = _minimumLabel;
@synthesize contentScrollView = _contentScrollView;
@synthesize containerView = _containerView;
@synthesize tableViewHeightConstraint = _tableViewHeightConstraint;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCartItems];
    [self setupDeliveryOptions];
    [self setupInfoContainer];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardWillBeHidden:)];
    [_contentScrollView addGestureRecognizer:gestureRecognizer];
    // Do any additional setup after loading the view.
}

-(void)setupDeliveryOptions{
    _deliveryOptions = [NSMutableDictionary dictionary];
    [_deliveryOptions setValue:@"Nos alegra llevarte siempre la diversión, incluso si estás algo lejos :) Tomamos como base la distancia de tu ubicación a nosotros." forKey:highest_delivery_value];
    [_deliveryOptions setValue:@"Tomamos como base la distancia de tu ubicación a nosotros." forKey:average_delivery_value];
    [_deliveryOptions setValue:@"¡Qué suerte! Estamos muy cerca de ti :) Tomamos como base la distancia de tu ubicación a nosotros." forKey:lowest_delivery_value];
}

-(void)viewWillAppear:(BOOL)animated{
    [self trackPageWithGA];
    [self setupNavigationView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [_shoppingCartItemsTableView reloadData];
}

-(void)viewDidLayoutSubviews{
    _tableViewHeightConstraint.constant = [[UIScreen mainScreen] bounds].size.height- 227 - 64;
    [self.view layoutIfNeeded];
}

-(void) showMarkerLoader{
    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando"];
    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

-(void)setupInfoContainer{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = _shoppingCartPriceInfoContainer.bounds;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    [_shoppingCartPriceInfoContainer.layer insertSublayer:ca atIndex:0];
    NSString *deliveryFeeString = @"No disponible";
    if ([AppDelegate sharedAppDelegate].reachableCompany) {
        deliveryFeeString = @"Gratis";
        if ([AppDelegate sharedAppDelegate].deliveryFee.integerValue != 0) {
            deliveryFeeString = [NSString stringWithFormat:@"S/. %.02f",[AppDelegate sharedAppDelegate].deliveryFee.floatValue];
        }
    }
    [_deliveryFeeLabel setText:deliveryFeeString];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeField = nil;
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _contentScrollView.contentInset = contentInsets;
    _contentScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _activeField.frame.origin) ) {
        [_contentScrollView scrollRectToVisible:_activeField.frame animated:YES];
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [_codeTextField endEditing:YES];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    _contentScrollView.contentInset = contentInsets;
    _contentScrollView.scrollIndicatorInsets = contentInsets;
    
}

-(void)setupNavigationView{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
    
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.navigationItem.title attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Carrito de compras"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupCartItems{
    _shoppingCartItems = [NSMutableArray array];
    [_shoppingCartItems addObjectsFromArray:([[AppDelegate sharedShoppingCart] shoppingCartCombosItems])];
    [_shoppingCartItems addObjectsFromArray:([[AppDelegate sharedShoppingCart] shoppingCartProductsItems])];
    [self reloadTotal];
    [_shoppingCartItemsTableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_shoppingCartItems count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 124.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShoppingCartItemCell *cell=[tableView dequeueReusableCellWithIdentifier:@"customShoppingCartItemCell"];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:
(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"                " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        ShoppingCartItem *record = [self.shoppingCartItems objectAtIndex:indexPath.row];
                                        if (record.isCombo) {
                                            [[AppDelegate sharedShoppingCart] removeComboFromShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
                                        }else{
                                            [[AppDelegate sharedShoppingCart] removeProductFromShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
                                        }
                                        [_shoppingCartItems removeObjectAtIndex:indexPath.row];
                                        [self reloadTotal];
                                        [_shoppingCartItemsTableView reloadData];
                                    }];
    button.backgroundColor = [[UIColor alloc] initWithPatternImage:[self imageWithImage:[UIImage imageNamed:@"row-action-delete"] scaledToSize:CGSizeMake(118.0, tableView.rowHeight)]];
    
    return @[button];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


- (void)configureCell:(ShoppingCartItemCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    ShoppingCartItem *record = [_shoppingCartItems objectAtIndex:indexPath.row];
    
    // Update Cell
    NSLog(@"QTY SH %@", record.qty);
    [cell.shoppingCartItemName setText:record.name];
    [cell.shoppingCartItemQty setText:[NSString stringWithFormat:@"%@",record.qty]];
    [cell.shoppingCartItemPrice setText:[NSString stringWithFormat:@"S/. %@", record.price]];
    [cell.shoppingCartThumbnailImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", server_base_url, record.itemImage]] placeholderImage:[UIImage imageNamed:@"ba_product_placeholder"]];
    [cell.shoppingCartItemDescription setText:record.itemDescription];
}

- (void)goToOrderDetails:(UIStoryboard *)storyboard {
    OrderDetailsViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"orderDetailsViewControllerId"];
    [[self navigationController] pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)didDismissAuthDashboard{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self goToOrderDetails:storyboard];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)alcoholicProducts{
    for (int i = 0; i < _shoppingCartItems.count ; i++) {
        ShoppingCartItem *record = [self.shoppingCartItems objectAtIndex:i];
        if (record.isAlcoholic) {
            return TRUE;
        }
    }
    return FALSE;
}

- (IBAction)confirmButtonAction:(id)sender {
    if ([AppDelegate sharedAppDelegate].reachableCompany) {
        if ([self validTime]) {//
            if (_shoppingCartItems.count > 0) {
                if ([self alcoholicProducts]) {
                    if ([AppDelegate sharedAppDelegate].internetConnection) {
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        if ([BAUser isUserLoggedIn]) {
                            [self goToOrderDetails:storyboard];
                        }else{
                            [[NSNotificationCenter defaultCenter]
                             addObserver:self
                             selector:@selector(didDismissAuthDashboard)
                             name:@"AuthDashboardDismissed"
                             object:nil];
                            AuthDashboardViewController *avc = (AuthDashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"authDashboardControllerId"];
                            avc.originVc = auth_dashboard_origin_shopping_cart;
                            [[self navigationController] presentViewController:avc animated:YES completion:nil];
                        }
                    }else{
                        [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
                    }
                } else {
                    UIAlertController * alert = [UIAlertController
                                                 alertControllerWithTitle:@"¡Aqui falta el protagonista!"
                                                 message:@"Tu pedido debe salir con una botella de licor o chela 😏"
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yesButton = [UIAlertAction
                                                actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                                                {
                                                    [self trackAlcohollessOrder];
                                                }];
                    
                    [alert addAction:yesButton];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
                }
                
            }else{
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Información"
                                             message:@"Por favor, agregue productos al carrito para proseguir con la orden."
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                            {
                                            }];
                
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
            }
        }else{
            NSString *scheduleText = [BAConfiguration getActiveSchedulesText:[[AppDelegate sharedAppDelegate] managedObjectContext]];
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Información"
                                          message:scheduleText
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                        }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
        }
    } else {
        UIAlertController * alert= [UIAlertController
                                      alertControllerWithTitle:@"Oops"
                                      message:@"¡Aún no llegamos por tu zona pero muy pronto estaremos por ahí!"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
    }
    

}

- (IBAction)addItemToShoppingCart:(id)sender {;
    UITableViewCell *cell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [_shoppingCartItemsTableView indexPathForCell:cell];
    ShoppingCartItem *record = [self.shoppingCartItems objectAtIndex:indexPath.row];
    NSLog(@"FAIL1: %@", ((ShoppingCartItem*)[self.shoppingCartItems objectAtIndex:0]).qty);
    if (record.isCombo) {
        [[AppDelegate sharedShoppingCart] addComboToShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]]) quantity:[NSNumber numberWithInt:(record.qty.intValue + 1)]];
    }else{
        [[AppDelegate sharedShoppingCart] addProductToShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]]) quantity:[NSNumber numberWithInt:(record.qty.intValue + 1)]];
    }
    NSLog(@"FAIL2: %@", ((ShoppingCartItem*)[self.shoppingCartItems objectAtIndex:0]).qty);
    [self reloadTotal];
    NSLog(@"FAIL3: %@", ((ShoppingCartItem*)[self.shoppingCartItems objectAtIndex:0]).qty);
    [_shoppingCartItemsTableView reloadData];
}

- (IBAction)substractItemFromShoppingCart:(id)sender {
    UITableViewCell *cell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [_shoppingCartItemsTableView indexPathForCell:cell];
    ShoppingCartItem *record = [self.shoppingCartItems objectAtIndex:indexPath.row];
    if (record.qty.intValue > 1) {
        if (record.isCombo) {
            [[AppDelegate sharedShoppingCart] addComboToShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]]) quantity:[NSNumber numberWithInt:(record.qty.intValue - 1)]];
        }else{
            [[AppDelegate sharedShoppingCart] addProductToShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]]) quantity:[NSNumber numberWithInt:(record.qty.intValue - 1)]];
        }
        [self reloadTotal];
        [_shoppingCartItemsTableView reloadData];
    }
}

- (IBAction)clearShoppingCart:(id)sender {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Información"
                                 message:@"¿Estás seguro que deseas borrar los productos del carrito de compras?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self removeItemsFromShoppingCart];
                                }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancelar"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                   }];
    
    [alert addAction:yesButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
    [alert.view setTintColor:[UIColorHelper practicalColorWithRed:0 green:122 blue:255 alpha:1.0]];
}

-(void)removeItemsFromShoppingCart{
    for (int i = 0; i < _shoppingCartItems.count ; i++) {
        ShoppingCartItem *record = [self.shoppingCartItems objectAtIndex:i];
        if (record.isCombo) {
            [[AppDelegate sharedShoppingCart] removeComboFromShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
        }else{
            [[AppDelegate sharedShoppingCart] removeProductFromShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
        }
    }
    [_shoppingCartItems removeAllObjects];
    [self reloadTotal];
    [_shoppingCartItemsTableView reloadData];
}

-(void)reloadTotal{
    double newTotal = [[AppDelegate sharedShoppingCart]total];
    if ([AppDelegate sharedAppDelegate].couponApplied){
        NSNumber * discount = [AppDelegate sharedAppDelegate].couponApplied[@"discount_amount"];
        newTotal = newTotal * (100 - discount.intValue)*0.01f;
    }
    [_shoppingCartTotalLabel setText:[NSString stringWithFormat:@"S/. %.02f", newTotal + [AppDelegate sharedAppDelegate].deliveryFee.floatValue]];
    [self handleMinimumLabel];
    
}

-(void)showTotalLimitLabel:(BOOL)show{
    _shoppingCartConfirmButton.enabled = !show;
}

- (IBAction)closeShoppingCart:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)validTime{
    NSDate* sourceDate = [NSDate date];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate* currentDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSInteger dayWeek = [gregorian componentsInTimeZone:sourceTimeZone fromDate:currentDate].weekday;
    
    NSMutableDictionary * validSchedules = [Schedule getFormattedSchedules:[[AppDelegate sharedAppDelegate] managedObjectContext]];
    
    if ([validSchedules objectForKey:[NSNumber numberWithInteger:dayWeek]] != nil) {
        NSArray *availableHours = [validSchedules objectForKey:[NSNumber numberWithInteger:dayWeek]];
        for (int i = 0; i < availableHours.count; i+=4) {
            NSDateComponents *initialComponents = [gregorian componentsInTimeZone:sourceTimeZone fromDate: currentDate];
            NSDateComponents *endComponents = [gregorian componentsInTimeZone:sourceTimeZone fromDate: currentDate];
            [initialComponents setHour: ((NSNumber *)[availableHours objectAtIndex:i]).integerValue ];
            [initialComponents setMinute: ((NSNumber *)[availableHours objectAtIndex:i+1]).integerValue];
            [endComponents setHour: ((NSNumber *)[availableHours objectAtIndex:i+2]).integerValue];
            [endComponents setMinute: ((NSNumber *)[availableHours objectAtIndex:i+3]).integerValue];
            
            NSDate* initialTime = [gregorian dateFromComponents: initialComponents];
            NSDate* endTime = [gregorian dateFromComponents: endComponents];
            
            if (([currentDate compare:initialTime]==NSOrderedDescending) && ([currentDate compare:endTime]==NSOrderedAscending)){
                return YES;
            }
        }
        return NO;
    }else{
        return NO;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController* destination = [segue destinationViewController];
    if (destination.class == SelectLocationViewController.class){
        ((SelectLocationViewController*)destination).parentVC = _parentVC;
    }
}

-(void)adjustTotalWithDiscount{
    
}

-(void)markFormAsCodeApplied:(NSString*)codeDesc{
    _codeTextField.enabled = false;
    _codeTextField.layer.borderColor= [[UIColorHelper practicalColorWithRed:189.0 green:189.0 blue:189.0 alpha:1.0] CGColor];
    _codeTextField.layer.borderWidth= 1.0f;
    _applyCouponButton.enabled = false;
    _applyCouponButton.backgroundColor = [UIColorHelper practicalColorWithRed:189.0 green:189.0 blue:189.0 alpha:1.0];
    [_codeInfoLabel setTextColor:[UIColorHelper practicalColorWithRed:189.0 green:189.0 blue:189.0 alpha:1.0]];
    [_codeInfoLabel setText:codeDesc];
    [_codeInfoLabel setHidden:FALSE];
}

-(void)markFormAsCodeInvalid{
    _codeTextField.layer.masksToBounds=YES;
    _codeTextField.layer.borderColor=[[UIColor redColor]CGColor];
    _codeTextField.layer.borderWidth= 1.0f;
    _codeTextField.layer.cornerRadius=5.0f;
    [_codeInfoLabel setTextColor:[UIColor redColor]];
    [_codeInfoLabel setHidden:FALSE];
}

-(void)verifyMinimumAmount{
    
}

- (IBAction)applyCode:(id)sender {
    if ([AppDelegate sharedAppDelegate].internetConnection) {
        [self keyboardWillBeHidden:nil];
        [self showMarkerLoader];
        
        RequestBuilder *r = [[RequestBuilder alloc] init];
        NSString * inputCode = _codeTextField.text;
        
        [r validate_coupon:inputCode success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
                @autoreleasepool {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        NSDictionary *jsonObj = (NSDictionary *) responseObject;
                        NSNumber* valid_coupon = jsonObj[@"coupon_valid"];
                        if (valid_coupon.intValue==1) {
                            NSDictionary *couponObj = jsonObj[@"coupon"];
                            NSNumber * discount = couponObj[@"discount_amount"];
                            NSString * couponDesc = couponObj[@"description"];
                            double newTotal = [[AppDelegate sharedShoppingCart] total] * (100 - discount.intValue)*0.01f;
                            [_shoppingCartTotalLabel setText:[NSString stringWithFormat:@"S/. %.02f", newTotal + [AppDelegate sharedAppDelegate].deliveryFee.floatValue]];
                            [AppDelegate sharedAppDelegate].couponApplied = couponObj;
                            [self markFormAsCodeApplied:couponDesc];
                        }else{
                            [self markFormAsCodeInvalid];
                        }
                        [activityLoader stopAndRemoveLoader];
                    });
                }
            });
            
        } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
                @autoreleasepool {
                    [activityLoader stopAndRemoveLoader];
                    NSLog(@"FAIL: %@", responseObject);
                }
            });
            
        }];
    }else{
        [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
    }
}

-(void)handleMinimumLabel{
    if ([AppDelegate sharedAppDelegate].reachableCompany) {
        BOOL valid = [[AppDelegate sharedShoppingCart] total] >= [AppDelegate sharedAppDelegate].minimumAmount.floatValue;
        CATransition *animation = [CATransition animation];
        if (valid) {
            animation.type = kCATransitionReveal;
            animation.subtype = kCATransitionFromTop;
        } else {
            animation.type = kCATransitionMoveIn;
            animation.subtype = kCATransitionFromBottom;
        }
        animation.duration = 0.4;
        [_minimumLabelContainerView.layer addAnimation:animation forKey:nil];
        
        [_minimumLabelContainerView setHidden:valid];
        float limitToReachMinimum = [AppDelegate sharedAppDelegate].minimumAmount.floatValue - [[AppDelegate sharedShoppingCart] total];
        [_minimumLabel setText:[NSString stringWithFormat: @"¡Estas a %.02f soles del monto mínimo!", limitToReachMinimum]];
        [_shoppingCartConfirmButton setEnabled:valid];
    } else {
        [_minimumLabelContainerView setHidden:true];
    }
}

- (IBAction)closeMinimumLabel:(id)sender {
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionReveal;
    animation.subtype = kCATransitionFromTop;
    animation.duration = 0.4;
    [_minimumLabelContainerView.layer addAnimation:animation forKey:nil];
    [_minimumLabelContainerView setHidden:TRUE];
}

- (void)trackDeliveryIcon {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Ícono de información en delivery por valor de S/.%@", [AppDelegate sharedAppDelegate].deliveryFee]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)trackAlcohollessOrder {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Se intentó hacer un pedido sin alcohol"]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (IBAction)didTapDeliveryInfo:(id)sender {
    if (nil == self.currentPopTipViewTarget) {
        NSString *message = [_deliveryOptions valueForKey:[AppDelegate sharedAppDelegate].deliveryMessageOption];
        self.currentPopTipViewTarget = [[CMPopTipView alloc] initWithTitle:@"¿Cómo se calcula esta tarifa?" message:message];
        self.currentPopTipViewTarget.titleFont = [UIFont fontWithName:@"Aller-Bold" size:13.0];
        self.currentPopTipViewTarget.titleAlignment = NSTextAlignmentLeft;
        self.currentPopTipViewTarget.textFont = [UIFont fontWithName:@"Aller" size:13.0];
        self.currentPopTipViewTarget.textAlignment = NSTextAlignmentLeft;
        self.currentPopTipViewTarget.delegate = self;
        self.currentPopTipViewTarget.backgroundColor = [UIColorHelper practicalColorWithRed:76.0 green:124.0 blue:255.0 alpha:1.0];
        self.currentPopTipViewTarget.textColor = [UIColor whiteColor];
        self.currentPopTipViewTarget.titleColor = [UIColor whiteColor];
        self.currentPopTipViewTarget.borderColor = [UIColorHelper practicalColorWithRed:76.0 green:124.0 blue:255.0 alpha:1.0];
        self.currentPopTipViewTarget.borderWidth = 0.0;
        self.currentPopTipViewTarget.cornerRadius = 5.0;
        self.currentPopTipViewTarget.shouldEnforceCustomViewPadding = YES;
        self.currentPopTipViewTarget.dismissTapAnywhere = YES;
        self.currentPopTipViewTarget.has3DStyle = NO;
        self.currentPopTipViewTarget.hasShadow = NO;
        self.currentPopTipViewTarget.bubblePaddingX = 16;
        self.currentPopTipViewTarget.bubblePaddingY = 10;
        
        UIButton *button = (UIButton *)sender;
        [self.currentPopTipViewTarget presentPointingAtView:button inView:self.view animated:YES];
        [self trackDeliveryIcon];
    }
    else {
        // Dismiss
        [self.currentPopTipViewTarget dismissAnimated:YES];
        self.currentPopTipViewTarget = nil;
    }
}

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    self.currentPopTipViewTarget = nil;
}


@end
