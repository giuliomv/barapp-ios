//
//  SignUpViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 29/11/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthDashboardViewController.h"

@interface SignUpViewController : UIViewController

@property (strong, nonatomic) NSObject *userBasicInfo;
@property (strong, nonatomic) UIViewController *parentVC;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *telephoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *confirmSignUpButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *helloLabel;
@property (weak, nonatomic) IBOutlet UIView *fullNameErrorView;
@property (weak, nonatomic) IBOutlet UIView *emailErrorView;
@property (weak, nonatomic) IBOutlet UIView *telephoneErrorView;
@property (strong, nonatomic) IBOutlet UIDatePicker *birthdayPicker;
@property (weak, nonatomic) IBOutlet UISwitch *checkAdultSwitch;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UIView *birthdayErrorView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UIToolbar *birthdayToolbar;
- (IBAction)confirmSignUp:(id)sender;
- (IBAction)cancelSignUp:(id)sender;
- (IBAction)dateChanged:(id)sender;
- (IBAction)closeBirthdayPicker:(id)sender;
- (IBAction)adultSwitchChanged:(id)sender;


@end
