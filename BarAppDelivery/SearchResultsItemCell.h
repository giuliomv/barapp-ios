//
//  SearchResultsItemCell.h
//  BarAppDelivery
//
//  Created by Home on 6/8/20.
//  Copyright © 2020 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchResultsItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *searchResultsItemName;
@property (weak, nonatomic) IBOutlet UILabel *searchResultsItemQty;
@property (weak, nonatomic) IBOutlet UILabel *searchResultsItemPrice;
@property (weak, nonatomic) IBOutlet UIButton *searchResultsAddButton;
@property (weak, nonatomic) IBOutlet UIButton *searchResultsSubstractButton;
@property (weak, nonatomic) IBOutlet UIImageView *searchResultsThumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *searchResultsItemDescription;

@end

NS_ASSUME_NONNULL_END
