//
//  Product.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BAUser : NSManagedObject

+(BOOL)isUserLoggedIn;
+(BAUser*)getLoggedInUser;
+(void)logOutCurrentUser;

@end

NS_ASSUME_NONNULL_END

#import "BAUser+CoreDataProperties.h"
