//
//  AppDelegate.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ShoppingCart.h"
#import "MSDynamicsDrawerViewController.h"
#import "SlideDrawerViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSString *selectedLocationAddress;
@property (nonatomic, assign) CLLocationCoordinate2D selectedLocationCoordinates;
@property (nonatomic, assign) NSNumber *closestCompany;
@property (strong, nonatomic) MSDynamicsDrawerViewController *dynamicsDrawerViewController;
@property (strong, nonatomic) SlideDrawerViewController *menuViewController;
@property (strong, nonatomic) NSDictionary *couponApplied;
@property (strong, nonatomic) NSNumber *deliveryFee;
@property (strong, nonatomic) NSNumber *minimumAmount;
@property (strong, nonatomic) NSString *deliveryMessageOption;
@property (nonatomic) BOOL reachableCompany;
@property (nonatomic) BOOL internetConnection;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)reachabilityUnreachable:(UIViewController *)presenter;
- (void)registerForPushNotifications:(UIApplication *)application;

+ (AppDelegate *)sharedAppDelegate;
+ (ShoppingCart *)sharedShoppingCart;

@end

