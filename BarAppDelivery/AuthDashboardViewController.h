//
//  AuthDashboardViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 28/11/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthDashboardViewController : UIViewController
@property (strong, nonatomic) NSString *originVc;
@property (weak, nonatomic) IBOutlet UIImageView *authImageView;
@property (weak, nonatomic) IBOutlet UILabel *authExplanationLabel;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)fbLoginButtonClicked:(id)sender;
- (IBAction)goBackToHome;
- (void)closeAuthDashboard;
- (void)showNotificationDefaultAlert;
- (IBAction)showTermsConditions:(id)sender;

@end
