//
//  CustomCategoryCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCategoryCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UIView *labelFilterView;
@property (weak, nonatomic) IBOutlet UIView *extraFilterView;

@end
