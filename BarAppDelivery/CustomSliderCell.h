//
//  CustomSliderCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 22/09/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomSliderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *optionNameLabel;



@end
