//
//  AuthDashboardViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 28/11/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "AuthDashboardViewController.h"
#import "NotificationPermissionRequestViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "CustomLoaderView.h"
#import "RequestBuilder.h"
#import "SignUpViewController.h"
#import "AppDelegate.h"
#import "CategoriesViewController.h"
#import "ShoppingCartViewController.h"
#import "UIColorHelper.h"
#import "Constants.h"
#import "PDKeychainBindings.h"
#import "PDKeychainBindingsController.h"
#import "BAUser.h"
#import <Google/Analytics.h>
#import "ContentViewController.h"
#import "TermsConditionModalViewController.h"
#import "CardItem+CoreDataClass.h"

@interface AuthDashboardViewController (){
    CustomLoaderView *activityLoader;
}

@end

@implementation AuthDashboardViewController

@synthesize authImageView = _authImageView;
@synthesize authExplanationLabel = _authExplanationLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneDragRevealEnabled:NO forDirection:MSDynamicsDrawerDirectionLeft];
}

-(void)viewWillAppear:(BOOL)animated{
    [self setupNavigationBar];
    [self trackPageOriginWithGA];
}

-(void)trackPageOriginWithGA{
    NSString *origin = @"Dashboard de autenticación desde Carrito de Compras";
    if ([_originVc isEqual:auth_dashboard_origin_slider]) {
        origin = @"Dashboard de autenticación desde menú";
    }
    [self trackPageWithGA:origin];
}

-(void)trackPageWithGA:(NSString *)page{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:page];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)setupNavigationBar{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    UIImage *image = [UIImage imageNamed:@"nav-logo"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) showMarkerLoader{
    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando los tragos"];
    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
}

- (void)openSignUp:(NSObject *)jsonData {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignUpViewController* svc = [storyboard instantiateViewControllerWithIdentifier:@"signUpViewControllerId"];
    ((SignUpViewController*)svc).userBasicInfo = [jsonData valueForKey:@"basic_info"];
    ((SignUpViewController*)svc).parentVC = self;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:svc];
    [self presentViewController:navigationController animated:true completion:nil];
}

-(void)requestSession:(NSString *)fb_token{
    [self showMarkerLoader];
    RequestBuilder *r = [[RequestBuilder alloc] init];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"session": @{@"fb_auth":fb_token}}];
    [r check_user_session:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                // Create Entity
                NSObject *jsonObject = (NSObject *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if ([jsonObject valueForKey:@"success"]) {
                        NSObject *jsonData = [jsonObject valueForKey:@"data"];
                        if ([[jsonData valueForKey:@"new_user"] boolValue]) {
                            [activityLoader stopAndRemoveLoader];
                            [self openSignUp:jsonData];
                        }else{
                            NSObject *userData = [jsonData valueForKey:@"user"];
                            [activityLoader stopAndRemoveLoader];
                            PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
                            [bindings setObject:[userData valueForKey:@"authentication_token"] forKey:@"authToken"];
                            BAUser *record = nil;
                            NSError *error = nil;
                            NSEntityDescription *entity = [NSEntityDescription entityForName:@"BAUser" inManagedObjectContext:self.managedObjectContext];
                            record = [[BAUser alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
                            record.fullName = [userData valueForKey:@"full_name"];
                            record.phoneNumber = [userData valueForKey:@"phone_number"];
                            record.fidelityPoints = [userData valueForKey:@"total_fidelity_points"];
                            NSString *dateString = [userData valueForKey:@"fb_birthday"];
                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss z"];
                            NSDate *dateFromString = [[NSDate alloc] init];
                            dateFromString = [dateFormatter dateFromString:dateString];
                            record.birthday = dateFromString;
                            record.firstName = [userData valueForKey:@"first_name"];
                            record.avatarUrl = [userData valueForKey:@"fb_picture"];
                            record.userEmail = [userData valueForKey:@"email"];
                            if ([self.managedObjectContext save:&error]) {
                                [self requestUserCards];
                            } else {
                                if (error) {
                                    NSLog(@"Unable to save record.");
                                    NSLog(@"%@, %@", error, error.localizedDescription);
                                }
                            }
                        }
                    }
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
                [activityLoader stopAndRemoveLoader];
            }
        });
        
    }];
}

-(void)requestUserCards{
    __block NSString *preferred_card_method;
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_user_cards:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    NSError *error = nil;
                    NSObject *jsonObj = (NSObject *) responseObject;
                    NSArray *jsonArray = (NSArray*)[jsonObj valueForKey:@"cards"];
                    for (int i=0; i<jsonArray.count; i++) {
                        CardItem *record = [[CardItem alloc] initWithEntity:[NSEntityDescription entityForName:@"CardItem" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
                        record.itemId = [[jsonArray objectAtIndex:i] valueForKey:@"id"];
                        record.lastFour = [[jsonArray objectAtIndex:i] valueForKey:@"last_four"];
                        record.cardBrand = [[jsonArray objectAtIndex:i] valueForKey:@"card_brand"];;
                        
                        if ([self.managedObjectContext save:&error]) {
                        }else{
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                    [activityLoader stopAndRemoveLoader];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"user_state_changed" object:nil userInfo:nil];
                    [self showNotificationPermissionRequestView];
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

- (void)showNotificationPermissionRequestView {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationPermissionRequestViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"notificationPermissionRequestViewControllerId"];
    ((NotificationPermissionRequestViewController*)ivc).parentVC = self;
    ivc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:ivc animated:true completion:nil];
}

-(void)showNotificationDefaultAlert{
    [self closeAuthDashboard];
    [[AppDelegate sharedAppDelegate] registerForPushNotifications:[UIApplication sharedApplication]];
}

- (IBAction)showTermsConditions:(id)sender {
    [self trackPageWithGA:@"Revisar los términos y condiciones desde pantalla de registro"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TermsConditionModalViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"termsConditionModalViewControllerId"];
    ((TermsConditionModalViewController*)ivc).parentVC = self;
    ivc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:ivc animated:true completion:nil];
    
}

-(void)closeAuthDashboard{
    if ([_originVc isEqual:auth_dashboard_origin_slider]) {
        [self goBackToHome];
    } else {
        [self dismissAuthDashboard];
    }
}

-(void)goBackToHome{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CategoriesViewController *cvc = (CategoriesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"categoriesViewControllerId"];
    UINavigationController *paneNavigationViewController = [[UINavigationController alloc] initWithRootViewController:cvc];
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneViewController:paneNavigationViewController animated:NO completion:nil];
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneDragRevealEnabled:YES forDirection:MSDynamicsDrawerDirectionLeft];
}

- (void)dismissAuthDashboard{
    NSLog(@"dismissAuthDashboard");
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneDragRevealEnabled:YES forDirection:MSDynamicsDrawerDirectionLeft];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"AuthDashboardDismissed"
     object:nil userInfo:nil];
}

- (IBAction)fbLoginButtonClicked:(id)sender {
    if ([AppDelegate sharedAppDelegate].internetConnection) {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login
         logInWithReadPermissions: @[@"public_profile", @"email", @"user_birthday", @"user_likes"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error) {
                 NSLog(@"Process error: %@", error.localizedDescription);
             } else if (result.isCancelled) {
                 NSLog(@"Cancelled");
             } else {
                 [self requestSession:[FBSDKAccessToken currentAccessToken].tokenString];
             }
         }];
    }else{
        [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
    }
}
@end
