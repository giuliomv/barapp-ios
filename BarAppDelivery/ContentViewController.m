//
//  ContentViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 4/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import "ContentViewController.h"
#import "UIColorHelper.h"
#import "AppDelegate.h"
#import <Google/Analytics.h>
#import "Constants.h"

@interface ContentViewController ()

@end

@implementation ContentViewController

@synthesize informatioTextView = _informatioTextView;
@synthesize rowSelected = _rowSelected;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_rowSelected.intValue == 0) {
        _informatioTextView.text = barapp_terms_conditions_text;
    }else{
        _informatioTextView.text = barapp_about_us_text;
    }

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"Gobold" size:14]}];
    self.navigationController.navigationBar.translucent = YES;
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    UIImage *image = [UIImage imageNamed:@"nav-logo"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
    
    [self trackPageWithGA];
}


-(void)trackPageWithGA{
    NSString *pageTitle = (_rowSelected.intValue == 0 ? @"Términos y Condiciones" : @"Sobre Bar App");
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value: [NSString stringWithFormat: @"Página informativa: %@", pageTitle]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidLayoutSubviews{
    [_informatioTextView setContentOffset:CGPointMake(0.0, 0.0) animated:NO];
    [_informatioTextView scrollRangeToVisible:NSMakeRange(0, 0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openSlider:(id)sender {
    [[AppDelegate sharedAppDelegate].dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:YES completion:^{
    }];
}
@end
