//
//  FirstViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSCoachMarksView.h"
#import "MSDynamicsDrawerViewController.h"
#import "BBBadgeBarButtonItem.h"

@interface CategoriesViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate>


@property (weak, nonatomic) IBOutlet UIView *activityIndicatorContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (strong, nonatomic) IBOutlet UIButton *shoppingCartButton;
@property (strong, nonatomic) IBOutlet UIButton *shoppingCartZeroButton;
@property (weak, nonatomic) IBOutlet UIPageControl *combosScrollPageControl;
@property (weak, nonatomic) IBOutlet UIView *comboControllerContainerView;
@property (weak, nonatomic) IBOutlet UICollectionView *categoriesCollectionView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
-(void)showConfirmedOrderWithName:(NSNotification *)aNotification;
- (IBAction)selectNewLocation:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *deliveryAddressContainer;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
- (IBAction)goBackToSelectLocation:(id)sender;
- (IBAction)openSlider:(id)sender;
- (void)showOrdersList;

@property (strong, nonatomic) NSNumber *companyId;

@end

