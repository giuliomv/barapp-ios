//
//  ShoppingCart.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 12/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"
#import "Combo.h"

@interface ShoppingCart : NSObject

@property (assign, nonatomic) float total;
@property (strong, nonatomic) NSMutableArray *shoppingCartCombosItems;
@property (strong, nonatomic) NSMutableArray *shoppingCartProductsItems;

-(void) addProductToShoppingCart:(Product *) p
                        quantity:(NSNumber *) qty;
-(void) addComboToShoppingCart:(Combo *) c
                      quantity:(NSNumber *) qty;
-(void) removeProductFromShoppingCart:(Product *) p;
-(void) removeComboFromShoppingCart:(Combo *) c;
-(long)amountOfProductsByQty;
-(long)amountOfCombosByQty;

@end
