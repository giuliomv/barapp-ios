//
//  CustomProductCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productUnitCostLabel;
@property (weak, nonatomic) IBOutlet UIStepper *productStepper;
@property (weak, nonatomic) IBOutlet UILabel *productQtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;

@end
