//
//  BrandBanner+CoreDataClass.h
//  
//
//  Created by Giulio Mondoñedo on 28/06/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BrandBanner : NSManagedObject

+(void)deleteAllBrandBanners:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "BrandBanner+CoreDataProperties.h"
