//
//  ProductsTableViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ProductsTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *productsActivityIndicator;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSNumber *categoryId;
@property (strong, nonatomic) IBOutlet UIButton *shoppingCartButton;
@property (strong, nonatomic) IBOutlet UIButton *shoppingCartZeroButton;
@property (strong, nonatomic) NSNumber *companyId;
@property (strong, nonatomic) NSString *productImgCode;
@property (strong, nonatomic) NSString *categoryName;
@property (strong, nonatomic) IBOutlet UITableView *productsTableView;
- (IBAction)qtyChanged:(UIButton *)sender value:(double)val;
- (IBAction)goBack:(id)sender;
- (IBAction)goToShoppingCart:(id)sender;
- (IBAction)plusSelected:(id)sender;
- (IBAction)minusSelected:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *brandsScrollView;

@end
