//
//  ConfirmedOrderViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 30/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmedOrderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *confirmationTextView;
@property (weak, nonatomic) IBOutlet UITextView *deliveryDetailsTextView;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *referenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *obtainedPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *accumulatedPointsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pickingBottlesImageAnimation;
@property (weak, nonatomic) IBOutlet UIImageView *cardSelectedImageView;

@property (strong, nonatomic) NSString *contactName;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSNumber *total;
@property (strong, nonatomic) NSString *addressReference;
@property (strong, nonatomic) NSNumber *deliveryFee;
@property (strong, nonatomic) NSString *paymentMethod;
@property (strong, nonatomic) NSNumber *cashAmount;
@property (strong, nonatomic) NSString *cardSelected;
@property (strong, nonatomic) UIViewController *parentVC;

@property (strong, nonatomic) NSNumber *orderPoints;
- (IBAction)goBackToCategories:(id)sender;
@end
