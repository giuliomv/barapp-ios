//
//  UIColorHelper.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 4/03/16.
//  Copyright (c) 2016 Giulio Mondoñedo. All rights reserved.
//

#import "UIColorHelper.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIColorHelper

+(UIColor*)getColorByCategoryCode:(NSString*) code{
    return [[self getConstDictionary] objectForKey:code];
}

+(NSDictionary*)getConstDictionary {
    static NSDictionary *inst = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        inst = @{
                 @"ba_cerveza" : [self practicalColorWithRed:231.0 green:76.0 blue:60.0 alpha:1.0],
                 @"ba_whiskey" : [self practicalColorWithRed:61.0 green:162.0 blue:0.0 alpha:1.0],
                 @"ba_ron" : [self practicalColorWithRed:255.0 green:153.0 blue:0.0 alpha:1.0],
                 @"ba_vodka" : [self practicalColorWithRed:231.0 green:76.0 blue:60.0 alpha:1.0],
                 @"ba_preparado" :[self practicalColorWithRed:242.0 green:108.0 blue:50.0 alpha:1.0],
                 @"ba_salvavida" :[self practicalColorWithRed:154.0 green:214.0 blue:229.0 alpha:1.0],
                 @"ba_piqueo" :[self practicalColorWithRed:255.0 green:153.0 blue:0.0 alpha:1.0],
                 @"ba_energizante" : [self practicalColorWithRed:61.0 green:162.0 blue:0.0 alpha:1.0],
                 @"ba_cigarro" : [self practicalColorWithRed:231.0 green:76.0 blue:60.0 alpha:1.0],
                 @"ba_pisco" : [self practicalColorWithRed:154.0 green:214.0 blue:225.0 alpha:1.0],
                 @"ba_gaseosa_jugo" : [self practicalColorWithRed:116.0 green:32.0 blue:44.0 alpha:1.0]
                 
                 };
    });
    return inst;
}

+(UIColor*)practicalColorWithRed:(CGFloat) r
                 green:(CGFloat) g
                  blue:(CGFloat) b
                 alpha:(CGFloat) a{
    return [super colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:a];
}

+(CAGradientLayer*)gradientColorWithColors:(NSArray*)colors
                                startPoint:(CGPoint)startPoint
                                  endPoint:(CGPoint)endPoint
                                    frame:(CGRect)bounds{
    CAGradientLayer *ca = [[CAGradientLayer alloc] init];
    ca.frame = bounds;
    ca.colors = colors;
    ca.startPoint = startPoint;
    ca.endPoint = endPoint;
    return ca;
}


+ (UIImage *)imageFromLayer:(CALayer *)layer
{
    CGRect bounds = [layer bounds];
    UIGraphicsBeginImageContext(bounds.size);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}

+ (UIImage *)imageWithColor:(UIColor *)color andBounds:(CGRect)imgBounds {
    UIGraphicsBeginImageContextWithOptions(imgBounds.size, NO, 0);
    [color setFill];
    UIRectFill(imgBounds);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

@end
