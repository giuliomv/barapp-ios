//
//  ProductImageViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 19/05/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "ProductImageViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <Google/Analytics.h>
#import "AppDelegate.h"
#import "Constants.h"

@interface ProductImageViewController ()

@end

@implementation ProductImageViewController

@synthesize productImgUrl = _productImgUrl;
@synthesize productImageView = _productImageView;
@synthesize titleNavigationItem = _titleNavigationItem;
@synthesize productImgName = _productImgName;
@synthesize productNameLabel = _productNameLabel;
@synthesize productDescriptionLabel = _productDescriptionLabel;
@synthesize productUnitCostLabel = _productUnitCostLabel;
@synthesize product = _product;
@synthesize backgroundOverlayView = _backgroundOverlayView;
@synthesize containerView = _containerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setModalPresentationStyle:UIModalPresentationCustom];
    if (_productImgUrl) {
        [_productImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",server_base_url,_productImgUrl]]];
    } else {
        [_productImageView setImage:[UIImage imageNamed:_productImgName]];
    }
    _productNameLabel.text = _product.name;
    _productUnitCostLabel.text = [NSString stringWithFormat:@"S/. %.02f",[_product.unitPrice floatValue]];
    _productDescriptionLabel.text = _product.productDescription;
    _productQtyLabel.text = [NSString stringWithFormat:@"%@",_product.qty];
    UITapGestureRecognizer *viewTapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapParentView:)];
    [_backgroundOverlayView addGestureRecognizer:viewTapRecognizer];
    UITapGestureRecognizer *containerTapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapContainerView:)];
    [_containerView addGestureRecognizer:containerTapRecognizer];
    
    // Do any additional setup after loading the view.
}

-(void)tapParentView:(UIView*)view{
    [self goBack:view];
}

-(void)tapContainerView:(UIView*)view{
    
}

-(void) qtyChanged:(UIButton *)sender value:(double)val{
    double newQty = _product.qty.doubleValue + val;
    if (newQty>0) {
        [[AppDelegate sharedShoppingCart] addProductToShoppingCart:_product quantity:[NSNumber numberWithDouble:newQty]];
        _productQtyLabel.text = [NSString stringWithFormat:@"%@",_product.qty];
    }else if (newQty==0){
        [[AppDelegate sharedShoppingCart] removeProductFromShoppingCart:_product];
        _productQtyLabel.text = [NSString stringWithFormat:@"%@",_product.qty];
    }
}

- (IBAction)minusSelected:(UIButton *)sender {
    [self qtyChanged:sender value:-1];
}

- (IBAction)plusSelected:(UIButton *)sender {
    [self qtyChanged:sender value:1];
}

-(void)viewWillAppear:(BOOL)animated{
    [self trackPageWithGA];
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Página de detalle de producto: %@", _product.name]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goBack:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

@end
