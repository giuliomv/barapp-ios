//
//  SelectLocationViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 27/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MSDynamicsDrawerViewController.h"

@interface SelectLocationViewController : UIViewController <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *locationMapView;
@property (weak, nonatomic) IBOutlet UIToolbar *currentLocationToolbar;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIImageView *buttonMarkerPin;
@property (weak, nonatomic) IBOutlet UIButton *buttonMarkerButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageMarker;
@property (strong, nonatomic) NSString *selectedLocationAddress;
@property (nonatomic, assign) CLLocationCoordinate2D selectedLocationCoordinates;
@property (strong, nonatomic) UIViewController *parentVC;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)goBack:(id)sender;
- (IBAction)defineLocation:(id)sender;

@end
