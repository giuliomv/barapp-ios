//
//  LocationPermissionRequestViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 7/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationPermissionRequestViewController : UIViewController

@property (strong, nonatomic) UIViewController *parentVC;
- (IBAction)acceptAction:(id)sender;
- (IBAction)cancelAction:(id)sender;

@end
