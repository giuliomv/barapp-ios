//
//  OrderLocationAnnotation.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 3/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface OrderLocationAnnotation : NSObject <MKAnnotation> {
    
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
    
}

@property(nonatomic, assign) CLLocationCoordinate2D coordinate;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *subtitle;

@end
