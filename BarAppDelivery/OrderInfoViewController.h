//
//  OrderInfoViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 1/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface OrderInfoViewController : UIViewController<MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet MKMapView *orderLocationMap;
@property (weak, nonatomic) IBOutlet UILabel *orderAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderStateLabel;
@property (weak, nonatomic) IBOutlet UITableView *orderDetailsTable;
@property (weak, nonatomic) IBOutlet UILabel *deliveryCostLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointsWonLabel;
@property (strong, nonatomic) NSObject *orderObj;
@property (strong, nonatomic) NSNumber *orderLat;
@property (strong, nonatomic) NSNumber *orderLng;
@property (weak, nonatomic) IBOutlet UILabel *previousCashLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountPayedLabel;
@property (weak, nonatomic) IBOutlet UILabel *activePaysWithLabel;
@property (weak, nonatomic) IBOutlet UIImageView *previousAppliedCardImage;
@property (weak, nonatomic) IBOutlet UILabel *previousAppliedLastFourCardLabel;
@property (weak, nonatomic) IBOutlet UIView *previousAppliedCardContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *activeAppliedCardImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)goBack:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ordersTableViewHeight;

@end
