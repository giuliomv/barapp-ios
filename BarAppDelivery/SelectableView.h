//
//  SelectableView.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 8/06/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectableView : UIView

@end
