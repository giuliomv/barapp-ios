//
//  RequestBuilder.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "RequestBuilder.h"
#import "Request.h"
#import "Constants.h"
#import "PDKeychainBindings.h"
#import "BAUser.h"

@implementation RequestBuilder

@synthesize request = _request;

-(id)init{
    self = [super init];
    if (self) {
        _request = [[Request alloc] initWithBaseURL:server_base_url];
    }
    return self;
}

-(NSURLSessionDataTask *)get_categories_with_parameters:(NSString*) parameters
                                                success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                                failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request get: [NSString stringWithFormat:@"%@/categories",api_namespace] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)get_products_by_category:(NSNumber*) categoryId
                                        companyId:(NSNumber*) companyId
                                       parameters:(NSString*) parameters
                                          success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                          failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request get:[NSString stringWithFormat:@"%@/companies/%@/products?category_id=%@",api_namespace, companyId, categoryId] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)get_combos_by_company: (NSNumber*) companyId
                                    parameters: (NSString*) parameters
                                       success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                       failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request get:[NSString stringWithFormat:@"%@/companies/%@/combos", api_namespace, companyId] parameters:parameters success:success failure:failure];
}

-(NSURLSessionTask *)get_banners_by_company:(NSNumber*)companyId
                                 parameters:(NSString*)parameters
                                    success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                    failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request get:[NSString stringWithFormat:@"%@/companies/%@/company_banners", api_namespace, companyId] parameters:parameters success:success failure:failure];
    
}

-(NSURLSessionDataTask *)get_schedules_by_company: (NSNumber*) companyId
                                       parameters:(NSString*) parameters
                                          success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                          failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request get:[NSString stringWithFormat:@"%@/active_schedules?company_id=%@",api_namespace, companyId] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)create_order_with_company:(NSNumber *) companyId
                                        parameters:(NSDictionary *) parameters
                                           success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                           failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    if ([BAUser isUserLoggedIn]) {
        PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
        [_request.manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[bindings objectForKey:@"authToken"]] forHTTPHeaderField:@"Authorization"];
    }
    return [_request post:[NSString stringWithFormat:@"%@/companies/%@/orders",api_namespace, companyId] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)send_token_for_push_with_parameters:(NSDictionary *) parameters
                                                     success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                                     failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    if ([BAUser isUserLoggedIn]) {
        PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
        [_request.manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[bindings objectForKey:@"authToken"]] forHTTPHeaderField:@"Authorization"];
    }
    return [_request post:[NSString stringWithFormat:@"%@/device_tokens",api_namespace] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)get_closest_company_with_latitude: (double) latitude
                                                 longitude: (double) longitude
                                                   success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                                   failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request get:[NSString stringWithFormat:@"%@/closest_company?lat=%f&lng=%f",api_namespace, latitude, longitude] parameters:nil success:success failure:failure];

}

-(NSURLSessionDataTask *)validate_coupon: (NSString *) input_code
                                                   success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                                   failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request get:[NSString stringWithFormat:@"%@/valid_coupon?input_code=%@",api_namespace, input_code] parameters:nil success:success failure:failure];
    
}

-(NSURLSessionDataTask *)check_user_session:(NSDictionary *) parameters
                                                     success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                                     failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request post:[NSString stringWithFormat:@"%@/sessions",api_namespace] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)register_new_user:(NSDictionary *) parameters
                                                     success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                                     failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request post:[NSString stringWithFormat:@"%@/registrations",api_namespace] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)get_user_fidelity_points:(NSDictionary *) parameters
                                 success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                 failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
        [_request.manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[bindings objectForKey:@"authToken"]] forHTTPHeaderField:@"Authorization"];
    
    return [_request get:[NSString stringWithFormat:@"%@/fidelity_points",api_namespace] parameters:nil success:success failure:failure];
    
}

-(NSURLSessionDataTask *)submit_order_rating:(NSDictionary *) parameters
                                    success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                    failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request post:[NSString stringWithFormat:@"%@/net_promoting_scores",api_namespace] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)get_user_cards:(NSDictionary *) parameters
                                          success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                          failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
    [_request.manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[bindings objectForKey:@"authToken"]] forHTTPHeaderField:@"Authorization"];
    
    return [_request get:[NSString stringWithFormat:@"%@/credit_cards",api_namespace] parameters:nil success:success failure:failure];
    
}

-(NSURLSessionDataTask *)create_user_card:(NSDictionary *) parameters
                                           success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                           failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    if ([BAUser isUserLoggedIn]) {
        PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
        [_request.manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[bindings objectForKey:@"authToken"]] forHTTPHeaderField:@"Authorization"];
    }
    return [_request post:[NSString stringWithFormat:@"%@/payments/register_card",api_namespace] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)delete_user_card:(NSDictionary *) parameters
                                  success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                  failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    if ([BAUser isUserLoggedIn]) {
        PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
        [_request.manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[bindings objectForKey:@"authToken"]] forHTTPHeaderField:@"Authorization"];
    }
    return [_request delete:[NSString stringWithFormat:@"%@/credit_cards",api_namespace] parameters:parameters success:success failure:failure];
}

-(NSURLSessionDataTask *)get_user_orders:(NSDictionary *) parameters
                                          success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                          failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
    [_request.manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[bindings objectForKey:@"authToken"]] forHTTPHeaderField:@"Authorization"];
    
    return [_request get:[NSString stringWithFormat:@"%@/my_orders",api_namespace] parameters:nil success:success failure:failure];
    
}

-(NSURLSessionDataTask *)get_items_by_query:(NSString*) query
                                        companyId:(NSNumber*) companyId
                                       parameters:(NSString*) parameters
                                          success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                          failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    return [_request get:[NSString stringWithFormat:@"%@/companies/%@/find_items?query=%@",api_namespace, companyId, query] parameters:parameters success:success failure:failure];
}

@end
