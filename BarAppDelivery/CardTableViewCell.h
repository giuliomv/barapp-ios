//
//  CardTableViewCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 15/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lastFourLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardBrandImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@end
