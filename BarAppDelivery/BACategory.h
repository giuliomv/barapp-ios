//
//  Category.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BACategory : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+(void)deleteAllCategories:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Category+CoreDataProperties.h"
