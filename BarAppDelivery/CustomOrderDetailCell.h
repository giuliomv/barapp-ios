//
//  CustomOrderDetailCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 29/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomOrderDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *orderDetailTextField;

@end
