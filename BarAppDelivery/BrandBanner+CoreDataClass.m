//
//  BrandBanner+CoreDataClass.m
//  
//
//  Created by Giulio Mondoñedo on 28/06/17.
//
//

#import "BrandBanner+CoreDataClass.h"

@implementation BrandBanner

+(void)deleteAllBrandBanners:(NSManagedObjectContext *)context{
    NSFetchRequest *allBrandBanners = [[NSFetchRequest alloc] init];
    [allBrandBanners setEntity:[NSEntityDescription entityForName:@"BrandBanner" inManagedObjectContext:context]];
    [allBrandBanners setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *brandBanners = [context executeFetchRequest:allBrandBanners error:&error];
    //error handling goes here
    for (NSManagedObject *brandBanner in brandBanners) {
        [context deleteObject:brandBanner];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

@end
