//
//  ShoppingCartItem.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 13/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoppingCartItem : NSObject

@property (nullable, nonatomic, retain) NSNumber *itemId;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSNumber *qty;
@property (nullable, nonatomic, retain) NSString *itemImage;
@property (nullable, nonatomic, retain) NSString *itemDescription;
@property (nonatomic, assign) bool isCombo;
@property (nonatomic, assign) bool isAlcoholic;

-(id)initWithItemId:(NSNumber *) itemId
               name:(NSString *) name
              price:(NSNumber *) price
                qty:(NSNumber *) qty
            isCombo:(bool) isCombo
          itemImage:(NSString*) itemImage
    itemDescription:(NSString*) itemDescription
        isAlcoholic:(bool) isAlcoholic;

@end
