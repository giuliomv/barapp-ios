//
//  TermsConditionModalViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 11/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import "TermsConditionModalViewController.h"
#import "AuthDashboardViewController.h"
#import "Constants.h"

@interface TermsConditionModalViewController ()

@end

@implementation TermsConditionModalViewController

@synthesize termsConditionsTextView = _termsConditionsTextView;

- (void)viewDidLoad {
    [super viewDidLoad];
    _termsConditionsTextView.text = barapp_terms_conditions_text;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    [_termsConditionsTextView setContentOffset:CGPointMake(0.0, 0.0) animated:NO];
    [_termsConditionsTextView scrollRangeToVisible:NSMakeRange(0, 0)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)goBack {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelAction:(id)sender {
    [self goBack];
}

@end
