//
//  Expiry.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 23/05/18.
//  Copyright © 2018 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Expiry.h"

@implementation Expiry

@synthesize month = _month;
@synthesize year = _year;
@synthesize rawValue = _rawValue;

-(id)initWithString:(NSString*) str{
    
    // Make sure that there is only one non-numeric separation character in the entire string
    
    if (([str stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]]).length > 1) {
        return nil;
    }
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^(\\d{1,2})[/|-](\\d{1,4})"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSTextCheckingResult* match = nil;
    NSString* monthStr = @"";
    NSString* yearStr = @"";
    
    if (error==nil) {
        match = [regex firstMatchInString:str options:NSMatchingProgress range:NSMakeRange(0, str.length)];
    }else{
        return nil;
    }
    
    NSRange monthRange = [match rangeAtIndex:1];
    monthStr = [str substringWithRange:monthRange];
    if (!(monthRange.length > 0 && ![monthStr isEqual:@""])) {
        return nil;
    }
    
    NSRange yearRange = [match rangeAtIndex:2];
    yearStr = [str substringWithRange:yearRange];
    if (!(yearRange.length > 0 && ![yearStr isEqual:@""])) {
        return nil;
    }
    
    return [self expiryByMonth:monthStr andYear:yearStr];
    
}

-(id) expiryByMonth: (NSString*)month andYear: (NSString*) year{
    int monthVal = [month intValue];
    int yearVal = [year intValue];
    if (monthVal!=0 && yearVal!=0 && year.length>=2) {
        return [self expiryByMonthInt: monthVal andYear: yearVal];
    }else {
        return nil;
    }
}

-(id)expiryByMonthInt:(int)month andYear:(int)year{
    int numberPrefix = 2000;
    int minYear = 2000;
    int yearValue = 0;
    
    if (year < 100) {
        yearValue = year + numberPrefix;
    }else{
        yearValue = year;
    }
    
    if (month>12 || month<1 || yearValue < minYear) {
        return nil;
    }
    
    NSDate* date = [self dateWithMonth: month andYear: yearValue];
    if (date==nil) {
        return nil;
    }
    return [self initDate:date];
}

-(id)initDate:(NSDate*)date {
        self = [super init];
        if (self) {
            _rawValue = date;
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents* components = [gregorianCalendar components:NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
            _month = (int)[components month];
            _year = (int)[components year];
            
        }
        return self;
}

-(NSDate*)dateWithMonth: (int)month andYear:(int) year{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:1];
    [dateComponents setMonth:month];
    [dateComponents setYear:year];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *newDate = [gregorianCalendar dateFromComponents:dateComponents];
    
    if (newDate!=nil) {
        NSRange monthRange = [gregorianCalendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:newDate];
        [dateComponents setDay:monthRange.length];
        [dateComponents setHour:23];
        [dateComponents setMinute:59];
        
        return [gregorianCalendar dateFromComponents:dateComponents];
    }
    return nil;
}
@end
