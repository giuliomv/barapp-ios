//
//  UIPlaceHolderTextView.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 30/09/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
IB_DESIGNABLE
@interface UIPlaceHolderTextView : UITextView

@property (nonatomic, retain) IBInspectable NSString *placeholder;
@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@end
