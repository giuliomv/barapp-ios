//
//  SelectLocationViewController.m
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 27/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import "SelectLocationViewController.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>
#import <Google/Analytics.h>
#import "MSDynamicsDrawerViewController.h"
#import "OrderDetailsViewController.h"
#import "AppDelegate.h"
#import "RequestBuilder.h"
#import "CategoriesViewController.h"
#import "SlideDrawerViewController.h"
#import "UIColorHelper.h"
#import "CustomLoaderView.h"
#import "Combo.h"
#import "Product.h"
#import "BAConfiguration.h"
#import "Schedule.h"
#import "ShoppingCartItem.h"
#import "WSCoachMarksView.h"
#import "Reachability.h"
#import "BAUser.h"
#import "Constants.h"
#import "CardItem+CoreDataClass.h"
#import "OrderRatingViewController.h"

@interface SelectLocationViewController () <CLLocationManagerDelegate, MKMapViewDelegate>{
    CustomLoaderView *activityLoader;
}
    @property UIAlertController *reachabilityAlert;
@end

@implementation SelectLocationViewController

@synthesize locationManager = _locationManager;
@synthesize locationMapView = _locationMapView;
@synthesize selectedLocationAddress = _selectedLocationAddress;
@synthesize selectedLocationCoordinates = _selectedLocationCoordinates;
@synthesize addressLabel = _addressLabel;
@synthesize imageMarker = _imageMarker;
@synthesize buttonMarkerPin = _buttonMarkerPin;
@synthesize buttonMarkerButton = _buttonMarkerButton;
@synthesize currentLocationToolbar = _currentLocationToolbar;

- (void)setupNavigationBar {
    UIColor *initialColor = [UIColorHelper practicalColorWithRed:155.0 green:63.0 blue:114.0 alpha:1.0];
    UIColor *endColor = [UIColorHelper practicalColorWithRed:216.0 green:50.0 blue:62.0 alpha:1.0];
    CGRect frame = self.navigationController.navigationBar.bounds;
    frame.size.height +=20;
    CAGradientLayer *ca = [UIColorHelper gradientColorWithColors:@[(__bridge id)initialColor.CGColor, (__bridge id)endColor.CGColor] startPoint:CGPointMake(0.0, 0.5) endPoint:CGPointMake(1.0, 0.5) frame:frame];
    UIImage *background = [UIColorHelper imageFromLayer:ca];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    self.navigationController.navigationBar.layer.shadowRadius = 4.0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Gobold" size:14],
                                 NSKernAttributeName: @1};
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Marca tu ubicación".uppercaseString attributes:attributes];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
}

- (void)setupMapAndLocationServices {
    [_locationMapView setDelegate:self];
    _locationManager = [[CLLocationManager alloc] init];
    
    
    [_locationManager setDelegate:self];
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
            [_locationMapView setShowsUserLocation:YES];
            [_locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
            [_locationManager startUpdatingLocation];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
}

-(void)showCoachMarks{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    UIImage * handImage = [UIImage imageNamed:@"move_map_image"];
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{10.0f,(screenHeight/2) - handImage.size.height - 100.0f},{300.0f,0.0f}}],
                                @"caption": @"Mueve el mapa y marca tu ubicación",
                                @"image": handImage,
                                @"shape": @"square"
                                }
                            ];
    WSCoachMarksView *coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    coachMarksView.animationDuration = 0.5f;
    coachMarksView.enableSkipButton = NO;
    [self.navigationController.view addSubview:coachMarksView];
    [coachMarksView start];
}

#define kPckAnimationSequenceCount 17

-(void) showMarkerLoader{
    activityLoader = [[CustomLoaderView alloc]initWithTitle:@"Cargando los tragos"];
    [activityLoader setupLoader:[UIApplication sharedApplication].keyWindow];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"Gobold" size:14]}];
    [self setupNavigationBar];
    MKUserTrackingBarButtonItem *currentLocationbuttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:_locationMapView];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                   target:nil
                                                                                   action:nil];
    NSMutableArray *buttonsArray = [[NSMutableArray alloc] init];
    [buttonsArray addObject:flexibleSpace];
    [buttonsArray addObject:currentLocationbuttonItem];
    [_currentLocationToolbar setItems:buttonsArray];
    [_currentLocationToolbar setBackgroundImage:[UIImage new]
                             forToolbarPosition:UIBarPositionAny
                                     barMetrics:UIBarMetricsDefault];
    [_currentLocationToolbar setShadowImage:[UIImage new]
                         forToolbarPosition:UIBarPositionAny];

    [self setupMapAndLocationServices];
    [self trackPageWithGA];
    if (![AppDelegate sharedAppDelegate].internetConnection) {
        [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"map_coach_mark"]) {
        [self showCoachMarks];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"map_coach_mark"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRatingView:) name:@"order_rating_arrived" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"order_rating_arrived"
                                                  object:nil];
}

-(void)trackPageWithGA{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Página de selección de ubicación"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)trackSelectionLocationEvent{
    if (CLLocationCoordinate2DIsValid(_selectedLocationCoordinates)) {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Click"
                                                              action:[NSString stringWithFormat: @"%f,%f",_selectedLocationCoordinates.latitude, _selectedLocationCoordinates.longitude]
                                                               label:@"Ubicación seleccionada"
                                                               value:nil] build]];
    }
}

 #pragma mark MKMapViewDelegate

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    CLLocationCoordinate2D coordinates = [mapView centerCoordinate];
    [self geocodeLocation:coordinates];
    
    [self showButtonMarker];
}

-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    [self showImageMarker];
}

-(void)showButtonMarker{
    [self marker_moving:FALSE];
}

- (void)marker_moving:(BOOL)moving {
    CATransition *animation = [CATransition animation];
    animation.type = @"rippleEffect";
    animation.duration = 0.4;
    [_buttonMarkerButton.layer addAnimation:animation forKey:nil];
    [_buttonMarkerPin.layer addAnimation:animation forKey:nil];
    [_imageMarker.layer addAnimation:animation forKey:nil];
    [_buttonMarkerButton setHidden:moving];
    [_buttonMarkerPin setHidden:moving];
    [_imageMarker setHidden:!moving];
}

-(void)showImageMarker{
    [self marker_moving:TRUE];
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations.lastObject;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000);
    [_locationMapView setRegion:viewRegion animated:YES];
    MKPointAnnotation *pin = [[MKPointAnnotation alloc]init];
    pin.coordinate = location.coordinate;
    [self geocodeLocation:location.coordinate];
    [_locationManager stopUpdatingLocation];
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation{
    MKAnnotationView *pinView = nil;
    return pinView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addAnnotationToMap:(id)sender {
//    CGPoint location = [sender locationInView:_locationMapView];
//    CLLocationCoordinate2D coordinates = [_locationMapView convertPoint:location toCoordinateFromView:_locationMapView];
//    MKPointAnnotation *pin = [[MKPointAnnotation alloc]init];
//    pin.coordinate = coordinates;
//    [self geocodeLocation:coordinates annotation:pin]; //[NSString stringWithFormat:@"Lat: %f, Long: %f", coordinates.latitude, coordinates.longitude];
//    [_locationMapView removeAnnotations:_locationMapView.annotations];
//    [_locationMapView addAnnotation:pin];
}

- (void)geocodeLocation:(CLLocationCoordinate2D)coordinates
{
    CLLocation *locObj = [[CLLocation alloc] initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:locObj completionHandler:^(NSArray *placemarks, NSError *error) {
        if(error){
            //do something
        }
        if(placemarks && placemarks.count){
            CLPlacemark *placemark2 = placemarks[0];
            NSDictionary *addressDictionary = placemark2.addressDictionary;
            NSString *street = [addressDictionary objectForKey:(NSString*)kABPersonAddressStreetKey];
            NSString *state = [addressDictionary objectForKey:(NSString*)kABPersonAddressStateKey];
            _addressLabel.text = [NSString stringWithFormat:@"%@ %@",street,state];
            _selectedLocationCoordinates = coordinates;
            _selectedLocationAddress = [NSString stringWithFormat:@"%@ %@",street,state];
        }
    }];
}

-(void)requestClosestLocation{
    [self showMarkerLoader];
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_closest_company_with_latitude: _selectedLocationCoordinates.latitude longitude:_selectedLocationCoordinates.longitude success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                // Create Entity
                NSObject *jsonObject = (NSObject *) responseObject;;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    NSNumber *companyId = [jsonObject valueForKey:@"id"];
                    BOOL isValidCompany = [[jsonObject valueForKey:@"is_valid_company"] boolValue];
                    NSArray *deliveryTiers = [jsonObject valueForKey: @"delivery_tiers"];
                    NSNumber *highestDeliveryFee = [jsonObject valueForKey:@"highest_delivery_fee"];
                    NSNumber *deliverySelected = [jsonObject valueForKey:@"delivery_fee"];
                    [self storeClosestCompany:companyId];
                    [self storeDeliveryFee:deliverySelected];
                    [self storeMinimumAmount:[jsonObject valueForKey:@"minimum_amount"]];
                    [self storeDeliveryLocation];
                    [self storeReachableCompany:isValidCompany];
                    [self defineDeliveryMessageWithTiers:deliveryTiers highestDeliveryFee:highestDeliveryFee selectedDelivery:deliverySelected];
                    [self resetInventory];
                    if ([BAUser isUserLoggedIn]) {
                        [self requestUserInfo];
                    }else{
                        [activityLoader stopAndRemoveLoader];
                        [self goToCategories];
                    }
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

-(void)defineDeliveryMessageWithTiers:(NSArray*)tiers highestDeliveryFee:(NSNumber*)highest selectedDelivery:(NSNumber*)selected{
    NSArray* totalTiers = [tiers arrayByAddingObject:@{@"delivery_value":highest}];
    NSNumber *lowerValue = [((NSObject *)totalTiers[(totalTiers.count / 3) - 1]) valueForKey:@"delivery_value"];
    NSNumber *upperValue = [((NSObject *)totalTiers[(totalTiers.count * 2 / 3)]) valueForKey:@"delivery_value"];
    if ([selected doubleValue] <= [lowerValue doubleValue]) {
        [self storeDeliveryMessageOption:lowest_delivery_value];
    }else if (selected >= upperValue){
        [self storeDeliveryMessageOption:highest_delivery_value];
    }else{
        [self storeDeliveryMessageOption:average_delivery_value];
    }
}

-(void)requestUserInfo{
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_user_fidelity_points:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                // Create Entity
                NSObject *jsonObject = (NSObject *) responseObject;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if ([[jsonObject valueForKey:@"success"] boolValue]) {
                        NSError *error = nil;
                        NSNumber *fidelityPoints = [jsonObject valueForKey:@"fidelity_points"];
                        NSString *avatarUrl = [jsonObject valueForKey:@"avatar_url"];
                        NSString *userEmail = [jsonObject valueForKey:@"user_email"];
                        BAUser* user = [BAUser getLoggedInUser];
                        user.fidelityPoints = fidelityPoints;
                        user.avatarUrl = avatarUrl;
                        user.userEmail = userEmail;
                        if ([self.managedObjectContext save:&error]) {
                            [self validateSentDeviceToken];
                            [self requestUserCards];
                        } else {
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                });
            }
        });
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
    }];
}


-(void)requestUserCards{
    __block NSString *preferred_card_method;
    RequestBuilder *r = [[RequestBuilder alloc] init];
    [r get_user_cards:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    NSError *error = nil;
                    NSObject *jsonObj = (NSObject *) responseObject;
                    NSArray *jsonArray = (NSArray*)[jsonObj valueForKey:@"cards"];
                    NSLog(@"preferred_card_method: %@", preferred_card_method);
                    for (int i=0; i<jsonArray.count; i++) {
                        CardItem *record = [[CardItem alloc] initWithEntity:[NSEntityDescription entityForName:@"CardItem" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
                        record.itemId = [[jsonArray objectAtIndex:i] valueForKey:@"id"];
                        record.lastFour = [[jsonArray objectAtIndex:i] valueForKey:@"last_four"];
                        record.cardBrand = [[jsonArray objectAtIndex:i] valueForKey:@"card_brand"];;
                        
                        if ([self.managedObjectContext save:&error]) {
                        }else{
                            if (error) {
                                NSLog(@"Unable to save record.");
                                NSLog(@"%@, %@", error, error.localizedDescription);
                            }
                        }
                    }
                    [activityLoader stopAndRemoveLoader];
                    [self goToCategories];
                });
            }
        });
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // (3)
            @autoreleasepool {
                NSLog(@"FAIL: %@", responseObject);
            }
        });
        
    }];
}

-(void)validateSentDeviceToken{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"device_token_sent"]) {
        [[AppDelegate sharedAppDelegate] registerForPushNotifications:[UIApplication sharedApplication]];
    }
}

-(void)resetInventory{
    NSManagedObjectContext *managedObjectContext = [[AppDelegate sharedAppDelegate] managedObjectContext];
    [self clearShoppingCart];
    [Combo deleteAllCombos:managedObjectContext];
    [Product deleteAllProducts:managedObjectContext];
    [BAConfiguration deleteAllBAConfiguration:managedObjectContext];
    [Schedule deleteAllSchedules:managedObjectContext];
    [CardItem deleteAllCards:self.managedObjectContext];
    
}

-(void)clearShoppingCart{
    NSMutableArray * shoppingCartItems = [NSMutableArray array];
    [shoppingCartItems addObjectsFromArray:([[AppDelegate sharedShoppingCart] shoppingCartCombosItems])];
    [shoppingCartItems addObjectsFromArray:([[AppDelegate sharedShoppingCart] shoppingCartProductsItems])];
    for (int i = 0; i < shoppingCartItems.count ; i++) {
        ShoppingCartItem *record = [shoppingCartItems objectAtIndex:i];
        if (record.isCombo) {
            [[AppDelegate sharedShoppingCart] removeComboFromShoppingCart:([Combo getComboById:record.itemId bannerType:@"Combo" context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
        }else{
            [[AppDelegate sharedShoppingCart] removeProductFromShoppingCart:([Product getProductById:record.itemId context:[[AppDelegate sharedAppDelegate] managedObjectContext]])];
        }
    }
    [shoppingCartItems removeAllObjects];
}

-(void) storeDeliveryFee:(NSNumber*)delivery_fee{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:delivery_fee forKey:@"deliveryFee"];
    [defaults synchronize];
    [AppDelegate sharedAppDelegate].deliveryFee = delivery_fee;
}

-(void) storeMinimumAmount:(NSNumber*)minimum_amount{
    [AppDelegate sharedAppDelegate].minimumAmount = minimum_amount;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:minimum_amount forKey:@"minimumAmount"];
    [defaults synchronize];
}

-(void) storeReachableCompany:(BOOL)reachable_company{
    [AppDelegate sharedAppDelegate].reachableCompany = reachable_company;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:reachable_company forKey:@"reachableCompany"];
    [defaults synchronize];
}

-(void) storeDeliveryMessageOption:(NSString*)option{
    [AppDelegate sharedAppDelegate].deliveryMessageOption = option;
}

-(void) storeClosestCompany:(NSNumber*)companyId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:companyId forKey:@"closestCompany"];
    [defaults synchronize];
    [AppDelegate sharedAppDelegate].closestCompany = companyId;
}

- (void) storeDeliveryLocation{
    [AppDelegate sharedAppDelegate].selectedLocationAddress = _selectedLocationAddress;
    [AppDelegate sharedAppDelegate].selectedLocationCoordinates =  _selectedLocationCoordinates;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_selectedLocationAddress forKey:@"selectedLocationAddress"];
    [defaults setDouble:_selectedLocationCoordinates.latitude forKey:@"selectedLocationCoordinatesLatitude"];
    [defaults setDouble:_selectedLocationCoordinates.longitude forKey:@"selectedLocationCoordinatesLongitude"];
    [defaults synchronize];
}
#pragma mark - Navigation
/*


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)goToCategories{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CategoriesViewController* vc = (CategoriesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"categoriesViewControllerId"];
    SlideDrawerViewController *menuViewController = (SlideDrawerViewController *)[storyboard instantiateViewControllerWithIdentifier:@"slideDrawerViewControllerId"];
    menuViewController.paneViewControllerType = @"CATEGORIES";
    MSDynamicsDrawerViewController *dynamicsViewController = (MSDynamicsDrawerViewController *)[storyboard instantiateViewControllerWithIdentifier:@"msDynamicsDrawerViewControllerId"];
    [AppDelegate sharedAppDelegate].dynamicsDrawerViewController = dynamicsViewController;
    [AppDelegate sharedAppDelegate].menuViewController = menuViewController;
    menuViewController.dynamicsDrawerViewController = dynamicsViewController;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    dynamicsViewController.paneViewController = navController;
    [dynamicsViewController addStylersFromArray:@[[MSDynamicsDrawerResizeStyler styler],[MSDynamicsDrawerParallaxStyler styler]] forDirection:MSDynamicsDrawerDirectionLeft];
    [dynamicsViewController setDrawerViewController:menuViewController forDirection:MSDynamicsDrawerDirectionLeft];
    [self.navigationController presentViewController:dynamicsViewController animated:true completion:nil];
}

- (IBAction)goBack:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)defineLocation:(id)sender {
    if ([AppDelegate sharedAppDelegate].internetConnection) {
        [self trackSelectionLocationEvent];
        [self requestClosestLocation];
    }else{
        [[AppDelegate sharedAppDelegate] reachabilityUnreachable:self];
    }
    
}

- (void)showRatingView:(NSNotification*)notification {
    NSDictionary* userInfo = notification.userInfo;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"orderRatingViewControllerId"];
    ((OrderRatingViewController*)ivc).orderId = (NSNumber*)userInfo[@"order_id"];
    ivc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self.navigationController presentViewController:ivc animated:true completion:nil];
}

@end
