//
//  Product.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Product : NSManagedObject

+(Product*)getProductById:(NSNumber *)productId
                  context:(NSManagedObjectContext *)context;

+(void)deleteAllProducts:(NSManagedObjectContext *)context;

-(bool)isAlcoholicProduct:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Product+CoreDataProperties.h"
