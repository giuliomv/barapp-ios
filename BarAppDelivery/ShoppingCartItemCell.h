//
//  ShoppingCartItemCell.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 17/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingCartItemCell : UITableViewCell<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *shoppingCartItemName;
@property (weak, nonatomic) IBOutlet UILabel *shoppingCartItemQty;
@property (weak, nonatomic) IBOutlet UILabel *shoppingCartItemPrice;
@property (weak, nonatomic) IBOutlet UIButton *shoppingCartAddButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ShoppingCartSubstractButton;
@property (weak, nonatomic) IBOutlet UIImageView *shoppingCartThumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *shoppingCartItemDescription;

@end
