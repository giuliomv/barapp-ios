//
//  CombosViewController.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 10/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CombosViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *combosScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *combosScrollPageControl;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (strong, nonatomic) NSNumber *companyId;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *combosArray;
- (IBAction)addComboAction:(id)sender;


@end
