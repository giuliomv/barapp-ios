//
//  RequestBuilder.h
//  BarAppDelivery
//
//  Created by Giulio Mondoñedo on 5/03/16.
//  Copyright © 2016 Giulio Mondoñedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Request.h"

@interface RequestBuilder : NSObject

@property (nonatomic, copy) Request *request;

-(NSURLSessionDataTask *)get_categories_with_parameters:(NSString*) parameters
                                success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)get_products_by_category:(NSNumber*)categoryId
                                        companyId:(NSNumber*)companyId
                                       parameters:(NSString*) parameters
                                          success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                          failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)get_combos_by_company: (NSNumber*) companyId
                                    parameters: (NSString*) parameters
                                            success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                            failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;
-(NSURLSessionDataTask *)get_schedules_by_company:(NSNumber*) companyId
                                       parameters:(NSString*) parameters
                                          success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                          failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;
-(NSURLSessionDataTask *)create_order_with_company:(NSNumber *) companyId
                                        parameters:(NSDictionary *) parameters
                                           success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                           failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;
-(NSURLSessionDataTask *)send_token_for_push_with_parameters:(NSDictionary*) parameters
                                                     success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                                     failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)get_closest_company_with_latitude: (double) latitude
                                                 longitude: (double) longitude
                                                   success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                                   failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;
-(NSURLSessionDataTask *)validate_coupon: (NSString *) input_code
                                 success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                 failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)check_user_session:(NSDictionary *) parameters
                                    success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                    failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)register_new_user:(NSDictionary *) parameters
                                   success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                   failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)get_user_fidelity_points:(NSDictionary *) parameters
                                          success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                          failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)submit_order_rating:(NSDictionary *) parameters
                                     success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                     failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionTask *_Nonnull)get_banners_by_company: (NSNumber*_Nonnull) companyId
                                    parameters: (NSString*_Nullable) parameters
                                       success:(void (^_Nullable)(NSURLSessionDataTask * _Nullable, id _Nullable))success
                                       failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable, NSError * _Nullable))failure;

-(NSURLSessionDataTask *)get_user_cards:(NSDictionary *) parameters
                                success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)create_user_card:(NSDictionary *) parameters
                                  success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                  failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)delete_user_card:(NSDictionary *) parameters
                                  success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                  failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)get_user_orders:(NSDictionary *) parameters
                                 success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                 failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

-(NSURLSessionDataTask *)get_items_by_query:(NSString*)query
                                  companyId:(NSNumber*)companyId
                                 parameters:(NSString*) parameters
                                    success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success
                                    failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;

@end
