//
//  BrandBanner+CoreDataProperties.m
//  
//
//  Created by Giulio Mondoñedo on 28/06/17.
//
//

#import "BrandBanner+CoreDataProperties.h"

@implementation BrandBanner (CoreDataProperties)

+ (NSFetchRequest<BrandBanner *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BrandBanner"];
}

@dynamic name;
@dynamic bannerId;
@dynamic categoryId;
@dynamic imgUrl;

@end
